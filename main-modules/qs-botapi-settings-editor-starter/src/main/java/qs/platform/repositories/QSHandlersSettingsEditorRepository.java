package qs.platform.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import qs.platform.beans.QSHandlerSettingsEditorEntity;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface QSHandlersSettingsEditorRepository extends JpaRepository<QSHandlerSettingsEditorEntity, String> {
  Boolean existsByCode(String cod);

  Optional<QSHandlerSettingsEditorEntity> findByCode(String code);

  Collection<QSHandlerSettingsEditorEntity> findByDeleted(boolean deleted);

  @Transactional
  @Modifying
  @Query(nativeQuery = true, value = """
    UPDATE qs_handlers_settings_editor SET
    deleted = :deleted,
    version = :version + 1,
    change_date = now()
    WHERE code = :code
    AND version = :version""")
  Integer markAsDeleted(@Param("code") String code, @Param("deleted") Boolean deleted, @Param("version") Integer version);
}