package qs.platform;

import qs.platform.botapi.QSHandlerSettings;

import java.util.Collection;

public interface QSHandlersSettingsEditorService {
  /**
   * Save {@link QSHandlerSettingsEditor handler's settings}
   *
   * @param handlerSettings handler's settings to save
   * @return {@link QSHandlerSettingsEditor saved handler's settings}
   */
  QSHandlerSettingsEditor save(QSHandlerSettingsEditor handlerSettings);

  /**
   * @return collection of actual {@link QSHandlerSettings handlers' settings}
   */
  Collection<QSHandlerSettingsEditor> getActual();

  /**
   * Returns {@link QSHandlerSettings handler's settings} by {@link QSHandlerSettings#getCode()  code}
   *
   * @param code code of {@link QSHandlerSettings handler}
   * @return {@link QSHandlerSettings handler's settings}
   */
  QSHandlerSettingsEditor getByCode(String code);

  /**
   * Returns actual handlers tree
   *
   * @return tree of actual {@link QSHandlerSettingsTree handlers}
   */
  Collection<QSHandlerSettingsEditorTree> getTree();

  /**
   * Update {@link QSHandlerSettingsEditor handler's settings} by {@link QSHandlerSettingsEditor#getCode() code}
   *
   * @param settings handler's settings to update
   * @return {@link QSHandlerSettingsEditor updated handler's settings}
   */
  QSHandlerSettingsEditor update(QSHandlerSettingsEditor settings);

  /**
   * Performs logical deletion of {@link QSHandlerSettingsEditor handler's settings}
   *
   * @param code    {@link QSHandlerSettingsEditor#getCode() handler's code}
   * @param version {@link QSHandlerSettingsEditor#getVersion() actual handler's settings version}
   */
  void logicalDelete(String code, Integer version);

  /**
   * Performs logical recovery of {@link QSHandlerSettingsEditor handler's settings}
   *
   * @param code    {@link QSHandlerSettingsEditor#getCode() handler's code}
   * @param version {@link QSHandlerSettingsEditor#getVersion() actual handler's settings version}
   */
  void logicalRecovery(String code, Integer version);

  /**
   * Actualize list of {@link QSHandlerSettings handlers' settings}
   *
   * @param settings {@link QSHandlerSettings handlers' settings} for actualization
   */
  void actualize(Collection<QSHandlerCodeVersion> settings);
}
