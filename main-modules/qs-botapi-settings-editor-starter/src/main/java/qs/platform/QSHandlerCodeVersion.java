package qs.platform;
/**
 * Handler's code and version bean
 */
public interface QSHandlerCodeVersion {
  /**
   * Handler's code
   */
  public String getCode();

  /**
   * Handler's settings version
   */
  Integer getVersion();
}
