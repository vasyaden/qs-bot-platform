package qs.platform;

import qs.platform.botapi.QSHandlerSettings;

import java.time.LocalDateTime;

/**
 * Handler's settings editor bean
 */
public interface QSHandlerSettingsEditor extends QSHandlerSettings {
  /**
   * Handler settings activity/inactivity flag
   */
  Boolean getDeleted();

  /**
   * Handler's settings version
   */
  Integer getVersion();

  /**
   * The date on which the handler settings were changed
   */
  LocalDateTime getChangeDate();

}
