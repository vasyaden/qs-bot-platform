package qs.platform.beans;

import org.springframework.util.CollectionUtils;
import qs.platform.QSHandlerSettingsEditor;
import qs.platform.QSHandlerSettingsEditorTree;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

public class QSHandlerSettingsEditorTreeBean implements QSHandlerSettingsEditorTree {
  private final QSHandlerSettingsEditor handlerSettings;
  private final Collection<QSHandlerSettingsEditorTree> children;

  private QSHandlerSettingsEditorTreeBean(QSHandlerSettingsEditor handlerSettings,
                                          Collection<QSHandlerSettingsEditorTree> children) {
    this.handlerSettings = handlerSettings;
    this.children = CollectionUtils.isEmpty(children) ?
      Collections.emptyList() : Collections.unmodifiableCollection(new ArrayList<>(children));
  }

  @Override
  public String getCode() {
    return handlerSettings.getCode();
  }

  @Override
  public String getParentCode() {
    return handlerSettings.getParentCode();
  }

  @Override
  public String getTitle() {
    return handlerSettings.getTitle();
  }

  @Override
  public Integer getOrder() {
    return handlerSettings.getOrder();
  }

  @Override
  public boolean isUnique() {
    return handlerSettings.isUnique();
  }

  @Override
  public String getDescription() {
    return handlerSettings.getDescription();
  }

  @Override
  public Collection<String> getFilters() {
    return handlerSettings.getFilters();
  }

  @Override
  public String getDataQuery() {
    return handlerSettings.getDataQuery();
  }

  @Override
  public String getScenarioCode() {
    return handlerSettings.getScenarioCode();
  }

  @Override
  public Map<String, Object> getScenarioParams() {
    return handlerSettings.getScenarioParams();
  }

  @Override
  public LocalDateTime getCreationDate() {
    return handlerSettings.getCreationDate();
  }

  @Override
  public Boolean getDeleted() {
    return handlerSettings.getDeleted();
  }

  @Override
  public Integer getVersion() {
    return handlerSettings.getVersion();
  }

  @Override
  public LocalDateTime getChangeDate() {
    return handlerSettings.getChangeDate();
  }

  @Override
  public Collection<QSHandlerSettingsEditorTree> getChildren() {
    return children;
  }

  public static Builder builder(QSHandlerSettingsEditor settings) {
    return new Builder(settings);
  }


  public static final class Builder {
    private final QSHandlerSettingsEditor handlerSettings;
    private final Collection<QSHandlerSettingsEditorTreeBean.Builder> children = new ArrayList<>();

    private Builder(QSHandlerSettingsEditor handlerSettings) {
      this.handlerSettings = handlerSettings;
    }

    public String getCode() {
      return handlerSettings.getCode();
    }

    public String getParentCode() {
      return handlerSettings.getParentCode();
    }

    public String getDataQuery() {
      return handlerSettings.getDataQuery();
    }

    public Builder addChild(QSHandlerSettingsEditorTreeBean.Builder child) {
      children.add(child);
      return this;
    }

    public QSHandlerSettingsEditorTree build() {
      Collection<QSHandlerSettingsEditorTree> child = children.stream().map(Builder::build).toList();
      return new QSHandlerSettingsEditorTreeBean(handlerSettings, child);
    }
  }
}
