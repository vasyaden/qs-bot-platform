package qs.platform.beans;

import qs.platform.QSHandlerCodeVersion;

public class QSHandlerCodeVersionBean implements QSHandlerCodeVersion {
  private String code;
  private Integer version;

  private QSHandlerCodeVersionBean(String code, Integer version) {
    this.code = code;
    this.version = version;
  }

  @Override
  public String getCode() {
    return code;
  }

  @Override
  public Integer getVersion() {
    return version;
  }

  public static Builder builder() {
    return new Builder();
  }

  public static final class Builder {
    private String code;
    private Integer version;

    public Builder code(String code) {
      this.code = code;
      return this;
    }

    public Builder version(Integer version) {
      this.version = version;
      return this;
    }

    public QSHandlerCodeVersionBean build() {
      return new QSHandlerCodeVersionBean(code, version);
    }
  }
}
