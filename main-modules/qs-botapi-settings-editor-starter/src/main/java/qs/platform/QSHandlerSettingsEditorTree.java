package qs.platform;

import java.util.Collection;

/**
 * Hierarchical tree of {@link QSHandlerSettingsEditorTree handlers' settings}
 */
public interface QSHandlerSettingsEditorTree extends QSHandlerSettingsEditor {
  /**
   * {@link QSHandlerSettingsEditorTree Child handlers settings}
   */
  Collection<QSHandlerSettingsEditorTree> getChildren();
}
