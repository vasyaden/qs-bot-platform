package qs.platform.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import qs.platform.*;
import qs.platform.beans.QSHandlerSettingsEditorEntity;
import qs.platform.beans.QSHandlerSettingsEditorTreeBean;
import qs.platform.botapi.QSHandlerCache;
import qs.platform.exceptions.QSAppRuntimeException;
import qs.platform.exceptions.QSInvalidVersionException;
import qs.platform.exceptions.QSNotFoundException;
import qs.platform.repositories.QSHandlersSettingsEditorRepository;
import qs.platform.settings.QSHandlersSettingsService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class QSHandlersSettingsEditorBaseService implements QSHandlersSettingsEditorService {
  private QSHandlersSettingsEditorRepository repository;

  private QSEntityBeanConverter<QSHandlerSettingsEditorEntity, QSHandlerSettingsEditor> converter;
  private QSHandlersSettingsService service;
  private QSHandlerCache handlersCache;

  @Autowired
  private void setRepository(QSHandlersSettingsEditorRepository repository) {
    this.repository = repository;
  }

  @Autowired
  private void setConverter(QSEntityBeanConverter<QSHandlerSettingsEditorEntity, QSHandlerSettingsEditor> converter) {
    this.converter = converter;
  }

  @Autowired
  private void setService(QSHandlersSettingsService service) {
    this.service = service;
  }

  @Autowired
  private void setHandlersCache(QSHandlerCache handlersCache) {
    this.handlersCache = handlersCache;
  }

  public QSHandlerSettingsEditor save(QSHandlerSettingsEditor handlerSettings) {
    if (!repository.existsByCode(handlerSettings.getCode())) {
      LocalDateTime now = LocalDateTime.now();
      QSHandlerSettingsEditorEntity entity = converter.toEntity(handlerSettings);
      entity.setDeleted(false);
      entity.setCreationDate(now);
      entity.setChangeDate(now);
      return converter.toBean(repository.save(entity));
    } else {
      throw new QSAppRuntimeException(QSHandlerSettingsEditor.class.getSimpleName() + "already exists",
        "Handler's settings with code = " + handlerSettings.getCode() + "and version = " + handlerSettings.getVersion()
          + "already exists;");
    }
  }

  @Override
  public Collection<QSHandlerSettingsEditor> getActual() {
    return repository.findByDeleted(false).stream().map(converter::toBean).toList();
  }

  @Override
  public QSHandlerSettingsEditor getByCode(String code) {
    return converter.toBean(repository.findByCode(code)
      .orElseThrow(() -> new QSNotFoundException("handlersSettingsEditor.notFound",
        "HandlersSettingsEditor with code = " + code + " not found")));
  }

  private void markAsDeleted(String code, Boolean deleted, Integer version) {
    if (repository.markAsDeleted(code, deleted, version) < 1) {
      if (repository.existsByCode(code)) {
        throw new QSInvalidVersionException(QSHandlerSettingsEditorEntity.class.getSimpleName() + ".invalidVersion",
          "Handler's settings already changed");
      } else {
        throw new QSNotFoundException(QSHandlerSettingsEditorEntity.class.getSimpleName() +
          ".notFound", "Handler's settings with code = " + code + "not found");
      }
    }
  }

  @Override
  public void logicalDelete(String code, Integer version) {
    markAsDeleted(code, true, version);
  }

  @Override
  public void logicalRecovery(String code, Integer version) {
    markAsDeleted(code, false, version);
  }

  @Override
  public QSHandlerSettingsEditor update(QSHandlerSettingsEditor settings) {
    if (repository.existsByCode(settings.getCode())) {
      QSHandlerSettingsEditorEntity entity = converter.toEntity(settings);
      entity.setChangeDate(LocalDateTime.now());
      try {
        return converter.toBean(repository.save(entity));
      } catch (OptimisticLockingFailureException ex) {
        throw new QSInvalidVersionException(QSHandlerSettingsEditorEntity.class.getSimpleName() + ".invalidVersion",
          "Handler's settings already changed");
      }
    } else {
      throw new QSNotFoundException("handlersSettingsEditor.notFound",
        "HandlerSettingsEditor with code = " + settings.getCode() + " not found");
    }
  }

  @Override
  @Transactional
  public void actualize(Collection<QSHandlerCodeVersion> settings) {
    Collection<QSHandlerSettingsEditor> actual = getActual();

    if (settings.size() == actual.size() && checkVersions(settings, actual)) {
      service.deleteAll();
      service.saveAll(actual);
      handlersCache.refresh();
    } else {
      throw new QSInvalidVersionException(QSHandlerSettingsEditorEntity.class.getSimpleName() + ".invalidVersion",
        "Handler's settings already changed");
    }
  }

  private boolean checkVersions(Collection<QSHandlerCodeVersion> settings, Collection<QSHandlerSettingsEditor> actualHandlersSettings) {
    Map<String, Integer> settingsMap = settings.stream().collect(Collectors.toMap(QSHandlerCodeVersion::getCode,
      QSHandlerCodeVersion::getVersion, (oldVersion, newVersion) -> oldVersion));

    for (QSHandlerSettingsEditor actual : actualHandlersSettings) {
      if (!actual.getVersion().equals(settingsMap.get(actual.getCode()))) {
        return false;
      }
    }
    return true;
  }

  @Override
  public Collection<QSHandlerSettingsEditorTree> getTree() {
    Map<String, QSHandlerSettingsEditorTreeBean.Builder> initialCollection = getActual().stream()
      .map(QSHandlerSettingsEditorTreeBean::builder)
      .collect(Collectors.toMap(QSHandlerSettingsEditorTreeBean.Builder::getCode, Function.identity()));
    Collection<QSHandlerSettingsEditorTreeBean.Builder> finalCollection = new ArrayList<>();
    Collection<String> included = new HashSet<>();
    for (QSHandlerSettingsEditorTreeBean.Builder current : initialCollection.values()) {
      String parentCode = current.getParentCode();
      QSHandlerSettingsEditorTreeBean.Builder parent = initialCollection.get(parentCode);

      if (parent == null) {
        finalCollection.add(current);
      } else {
        parent.addChild(current);
        included.add(current.getCode());
      }

      QSHandlerSettingsEditorTreeBean.Builder child = initialCollection.get(current.getDataQuery());
      if (child != null) {
        current.addChild(child);
        included.add(child.getCode());
      }
    }
    return finalCollection.stream()
      .filter(x -> !included.contains(x.getCode()))
      .map(QSHandlerSettingsEditorTreeBean.Builder::build)
      .collect(Collectors.toList());
  }
}
