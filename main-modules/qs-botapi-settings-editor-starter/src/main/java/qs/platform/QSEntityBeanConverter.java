package qs.platform;

/**
 * Converter from {@link E Entity} to {@link B Bean} and from {@link B Bean} to {@link E Entity}
 *
 * @param <E> Entity class
 * @param <B> Bean class
 */
public interface QSEntityBeanConverter<E, B> {
  /**
   * Converts {@link E entity} to {@link B bean}
   *
   * @return {@link B bean}
   */
  B toBean(E entity);

  /**
   * Converts {@link B bean} to {@link E entity}
   *
   * @return {@link E entity}
   */
  E toEntity(B bean);
}
