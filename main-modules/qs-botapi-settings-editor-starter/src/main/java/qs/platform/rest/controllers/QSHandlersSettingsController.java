package qs.platform.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import qs.platform.converters.QSConverter;
import qs.platform.rest.headers.QSHttpHeaders;
import qs.platform.rest.json.beans.QSJsonHandlerSettings;
import qs.platform.rest.json.beans.QSJsonHandlersSettingsTree;
import qs.platform.settings.QSHandlerSettingsTree;
import qs.platform.settings.QSHandlersSettingsService;

import java.util.Collection;

@RestController
@RequestMapping("/bot/v1/${qs.bot.routing.service-name}/handlers/actual")
public class QSHandlersSettingsController {
  private QSHandlersSettingsService settingsService;
  private QSConverter<QSHandlerSettingsTree, QSJsonHandlerSettings> converter;
  @Value("${qs.bot.secret}")
  private String secret;

  @Autowired
  public void setConverter(QSConverter<QSHandlerSettingsTree, QSJsonHandlerSettings> converter) {
    this.converter = converter;
  }

  @Autowired
  public void setSettingsService(QSHandlersSettingsService settingsService) {
    this.settingsService = settingsService;
  }

  @GetMapping(value = "/tree", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<QSJsonHandlersSettingsTree> getHandlersSettingsTree(@RequestHeader(value = QSHttpHeaders.X_QS_SERVICE_SECRET) String secret) {
    if (!this.secret.equals(secret)) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
    Collection<QSJsonHandlerSettings> tree = converter.convert(settingsService.getTree());
    return new ResponseEntity<>(QSJsonHandlersSettingsTree.from(tree), HttpStatus.OK);
  }
}