package qs.platform.rest.json.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import qs.platform.QSHandlerSettingsEditor;
import qs.platform.botapi.QSHandlerSettings;

/**
 * Handler's code and version json bean
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QSJsonHandlerCodeVersion {
  /**
   * @see QSHandlerSettings#getCode()
   */
  private String code;

  /**
   * @see QSHandlerSettingsEditor#getVersion()
   */
  private Integer version;

  public QSJsonHandlerCodeVersion(String code, Integer version) {
    this.code = code;
    this.version = version;
  }

  public QSJsonHandlerCodeVersion() {
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }


  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public static Builder builder() {
    return new Builder();
  }

  public static final class Builder {
    private String code;
    private Integer version;

    public Builder code(String code) {
      this.code = code;
      return this;
    }

    public Builder version(Integer version) {
      this.version = version;
      return this;
    }

    public QSJsonHandlerCodeVersion build() {
      return new QSJsonHandlerCodeVersion(code, version);
    }
  }
}
