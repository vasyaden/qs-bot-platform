package qs.platform.rest.json.converters;

import org.springframework.stereotype.Service;
import qs.platform.QSHandlerSettingsEditor;
import qs.platform.beans.QSHandlerSettingsEditorBean;
import qs.platform.converters.QSConverter;
import qs.platform.rest.json.beans.QSJsonHandlerSettingsEditor;

@Service
public class QSJsonToBeanHandlersSettingsEditorConverter implements QSConverter<QSJsonHandlerSettingsEditor, QSHandlerSettingsEditor> {
  @Override
  public QSHandlerSettingsEditor convert(QSJsonHandlerSettingsEditor json) {
    return QSHandlerSettingsEditorBean.builder()
            .code(json.getCode())
            .title(json.getTitle())
            .parentCode(json.getParentCode())
            .description(json.getDescription())
            .filters(json.getFilters())
            .dataQuery(json.getDataQuery())
            .unique(json.isUnique())
            .scenarioCode(json.getScenarioCode())
            .scenarioParams(json.getScenarioParams())
            .order(json.getOrder())
            .creationDate(json.getCreationDate())
            .changeDate(json.getChangeDate())
            .version(json.getVersion())
            .deleted(json.getDeleted())
            .build();
  }

  @Override
  public Class<QSJsonHandlerSettingsEditor> getInputType() {
    return QSJsonHandlerSettingsEditor.class;
  }

  @Override
  public Class<QSHandlerSettingsEditor> getOutputType() {
    return QSHandlerSettingsEditor.class;
  }

}
