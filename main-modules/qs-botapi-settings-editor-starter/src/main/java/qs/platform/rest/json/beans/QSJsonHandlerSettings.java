package qs.platform.rest.json.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QSJsonHandlerSettings {
  private String code;
  private String title;
  private String parentCode;
  private String description;
  private Collection<String> filters;
  private String dataQuery;
  private boolean unique;
  private Boolean deleted;
  private String scenarioCode;
  private Map<String, Object> scenarioParams;
  private Integer order;
  private LocalDateTime creationDate;
  private Collection<QSJsonHandlerSettings> children = new ArrayList<>();

  public QSJsonHandlerSettings() {
  }

  private QSJsonHandlerSettings(String code, String title, String parentCode, String description, Collection<String> filters, String dataQuery, boolean unique, Boolean deleted, String scenarioCode, Map<String, Object> scenarioParams, Integer order, LocalDateTime creationDate) {
    this.code = code;
    this.title = title;
    this.parentCode = parentCode;
    this.description = description;
    this.filters = filters;
    this.dataQuery = dataQuery;
    this.unique = unique;
    this.deleted = deleted;
    this.scenarioCode = scenarioCode;
    this.scenarioParams = scenarioParams;
    this.order = order;
    this.creationDate = creationDate;
  }

  public static Builder builder() {
    return new Builder();
  }

  public String getCode() {
    return code;
  }

  public String getTitle() {
    return title;
  }

  public String getDescription() {
    return description;
  }

  public String getParentCode() {
    return parentCode;
  }

  public Boolean getDeleted() {
    return deleted;
  }

  public Integer getOrder() {
    return order;
  }

  public boolean isUnique() {
    return unique;
  }

  public Collection<String> getFilters() {
    return filters;
  }

  public String getDataQuery() {
    return dataQuery;
  }

  public String getScenarioCode() {
    return scenarioCode;
  }

  public Map<String, Object> getScenarioParams() {
    return scenarioParams;
  }

  public LocalDateTime getCreationDate() {
    return creationDate;
  }

  public Collection<QSJsonHandlerSettings> getChildren() {
    return children;
  }

  public void addChild(QSJsonHandlerSettings child) {
    this.children.add(child);
  }

  public void setCode(String code) {
    this.code = code;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setParentCode(String parentCode) {
    this.parentCode = parentCode;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setFilters(Collection<String> filters) {
    this.filters = filters;
  }

  public void setDataQuery(String dataQuery) {
    this.dataQuery = dataQuery;
  }

  public void setUnique(boolean unique) {
    this.unique = unique;
  }

  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }

  public void setScenarioCode(String scenarioCode) {
    this.scenarioCode = scenarioCode;
  }

  public void setScenarioParams(Map<String, Object> scenarioParams) {
    this.scenarioParams = scenarioParams;
  }

  public void setOrder(Integer order) {
    this.order = order;
  }

  public void setCreationDate(LocalDateTime creationDate) {
    this.creationDate = creationDate;
  }

  public void setChildren(Collection<QSJsonHandlerSettings> children) {
    this.children = children;
  }

  public static final class Builder {
    private String code;
    private String title;
    private String parentCode;
    private String description;
    private Collection<String> filters;
    private String dataQuery;
    private boolean unique;
    private Boolean deleted;
    private String scenarioCode;
    private Map<String, Object> scenarioParams;
    private Integer order;
    private LocalDateTime creationDate;
    private Collection<QSJsonHandlerSettings> children = new ArrayList<>();

    private Builder() {
    }

    public Builder code(String code) {
      this.code = code;
      return this;
    }

    public Builder title(String title) {
      this.title = title;
      return this;
    }

    public Builder description(String description) {
      this.description = description;
      return this;
    }

    public Builder parentCode(String parentCode) {
      this.parentCode = parentCode;
      return this;
    }

    public Builder deleted(boolean deleted) {
      this.deleted = deleted;
      return this;
    }

    public Builder order(Integer order) {
      this.order = order;
      return this;
    }

    public Builder filters(Collection<String> filters) {
      this.filters = filters;
      return this;
    }

    public Builder unique(boolean unique) {
      this.unique = unique;
      return this;
    }


    public Builder dataQuery(String dataQuery) {
      this.dataQuery = dataQuery;
      return this;
    }

    public Builder scenarioCode(String scenarioCode) {
      this.scenarioCode = scenarioCode;
      return this;
    }

    public Builder scenarioParams(Map<String, Object> scenarioParams) {
      this.scenarioParams = scenarioParams;
      return this;
    }

    public Builder creationDate(LocalDateTime creationDate) {
      this.creationDate = creationDate;
      return this;
    }

    public Builder children(Collection<QSJsonHandlerSettings> children) {
      this.children = children;
      return this;
    }

    public QSJsonHandlerSettings build() {
      QSJsonHandlerSettings settings = new QSJsonHandlerSettings(code, title, parentCode, description, filters, dataQuery, unique, deleted, scenarioCode, scenarioParams, order, creationDate);
      settings.setChildren(children);
      return settings;
    }
  }
}
