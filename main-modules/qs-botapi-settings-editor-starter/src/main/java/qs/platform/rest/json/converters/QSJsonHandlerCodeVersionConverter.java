package qs.platform.rest.json.converters;

import org.springframework.stereotype.Component;
import qs.platform.QSHandlerCodeVersion;
import qs.platform.beans.QSHandlerCodeVersionBean;
import qs.platform.converters.QSConverter;
import qs.platform.rest.json.beans.QSJsonHandlerCodeVersion;

@Component
public class QSJsonHandlerCodeVersionConverter implements QSConverter<QSJsonHandlerCodeVersion, QSHandlerCodeVersion> {

  @Override
  public QSHandlerCodeVersion convert(QSJsonHandlerCodeVersion value) {
    return QSHandlerCodeVersionBean.builder()
      .code(value.getCode())
      .version(value.getVersion())
      .build();
  }

  @Override
  public Class<QSJsonHandlerCodeVersion> getInputType() {
    return QSJsonHandlerCodeVersion.class;
  }

  @Override
  public Class<QSHandlerCodeVersion> getOutputType() {
    return QSHandlerCodeVersion.class;
  }
}
