package qs.platform.rest.json.converters;

import org.springframework.stereotype.Service;
import qs.platform.QSHandlerSettingsEditor;
import qs.platform.converters.QSConverter;
import qs.platform.rest.json.beans.QSJsonHandlerSettingsEditor;

@Service
public class QSBeanToJsonHandlersSettingsEditorConverter implements QSConverter<QSHandlerSettingsEditor, QSJsonHandlerSettingsEditor> {

  @Override
  public QSJsonHandlerSettingsEditor convert(QSHandlerSettingsEditor bean) {
    return QSJsonHandlerSettingsEditor.builder()
            .code(bean.getCode())
            .title(bean.getTitle())
            .parentCode(bean.getParentCode())
            .description(bean.getDescription())
            .filters(bean.getFilters())
            .dataQuery(bean.getDataQuery())
            .unique(bean.isUnique())
            .scenarioCode(bean.getScenarioCode())
            .scenarioParams(bean.getScenarioParams())
            .order(bean.getOrder())
            .creationDate(bean.getCreationDate())
            .changeDate(bean.getChangeDate())
            .version(bean.getVersion())
            .deleted(bean.getDeleted())
            .build();
  }

  @Override
  public Class<QSHandlerSettingsEditor> getInputType() {
    return QSHandlerSettingsEditor.class;
  }

  @Override
  public Class<QSJsonHandlerSettingsEditor> getOutputType() {
    return QSJsonHandlerSettingsEditor.class;
  }
}
