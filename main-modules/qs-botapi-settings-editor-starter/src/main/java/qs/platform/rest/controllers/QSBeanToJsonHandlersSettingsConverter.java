package qs.platform.rest.controllers;

import org.springframework.stereotype.Service;
import qs.platform.converters.QSConverter;
import qs.platform.rest.json.beans.QSJsonHandlerSettings;
import qs.platform.settings.QSHandlerSettingsTree;

@Service
public class QSBeanToJsonHandlersSettingsConverter implements QSConverter<QSHandlerSettingsTree, QSJsonHandlerSettings> {

  @Override
  public QSJsonHandlerSettings convert(QSHandlerSettingsTree bean) {
    return QSJsonHandlerSettings.builder()
            .code(bean.getCode())
            .title(bean.getTitle())
            .parentCode(bean.getParentCode())
            .description(bean.getDescription())
            .filters(bean.getFilters())
            .dataQuery(bean.getDataQuery())
            .unique(bean.isUnique())
            .scenarioCode(bean.getScenarioCode())
            .scenarioParams(bean.getScenarioParams())
            .children(convert(bean.getChildren()))
            .order(bean.getOrder())
            .creationDate(bean.getCreationDate())
            .build();
  }

  @Override
  public Class<QSHandlerSettingsTree> getInputType() {
    return QSHandlerSettingsTree.class;
  }

  @Override
  public Class<QSJsonHandlerSettings> getOutputType() {
    return QSJsonHandlerSettings.class;
  }
}
