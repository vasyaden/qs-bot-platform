package qs.platform.rest.json.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Collection;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QSJsonHandlersSettingsTree {
  private final Collection<QSJsonHandlerSettings> handlersSettings;

  private QSJsonHandlersSettingsTree(Collection<QSJsonHandlerSettings> handlersSettings) {
    this.handlersSettings = handlersSettings;
  }

  public Collection<QSJsonHandlerSettings> getHandlers() {
    return handlersSettings;
  }

  public static QSJsonHandlersSettingsTree from(Collection<QSJsonHandlerSettings> handlers) {
    return new QSJsonHandlersSettingsTree(handlers);
  }

}
