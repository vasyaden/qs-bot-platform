package qs.platform.rest.json.converters;

import org.springframework.stereotype.Service;
import qs.platform.QSHandlerSettingsEditorTree;
import qs.platform.converters.QSConverter;
import qs.platform.rest.json.beans.QSJsonHandlerSettingsEditorTree;

@Service
public class QSBeanToJsonHandlersSettingsEditorTreeConverter implements QSConverter<QSHandlerSettingsEditorTree, QSJsonHandlerSettingsEditorTree> {

  @Override
  public QSJsonHandlerSettingsEditorTree convert(QSHandlerSettingsEditorTree bean) {
    return QSJsonHandlerSettingsEditorTree.builder()
      .code(bean.getCode())
      .title(bean.getTitle())
      .parentCode(bean.getParentCode())
      .description(bean.getDescription())
      .filters(bean.getFilters())
      .dataQuery(bean.getDataQuery())
      .unique(bean.isUnique())
      .scenarioCode(bean.getScenarioCode())
      .scenarioParams(bean.getScenarioParams())
      .children(convert(bean.getChildren()))
      .order(bean.getOrder())
      .creationDate(bean.getCreationDate())
      .changeDate(bean.getChangeDate())
      .version(bean.getVersion())
      .deleted(bean.getDeleted())
      .build();
  }

  @Override
  public Class<QSHandlerSettingsEditorTree> getInputType() {
    return QSHandlerSettingsEditorTree.class;
  }

  @Override
  public Class<QSJsonHandlerSettingsEditorTree> getOutputType() {
    return QSJsonHandlerSettingsEditorTree.class;
  }
}