package qs.platform.rest.json.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Collection;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QSJsonHandlersSettingsEditorTree {
  private final Collection<QSJsonHandlerSettingsEditorTree> handlersSettings;

  private QSJsonHandlersSettingsEditorTree(Collection<QSJsonHandlerSettingsEditorTree> handlersSettings) {
    this.handlersSettings = handlersSettings;
  }

  public Collection<QSJsonHandlerSettingsEditorTree> getHandlers() {
    return handlersSettings;
  }

  public static QSJsonHandlersSettingsEditorTree from(Collection<QSJsonHandlerSettingsEditorTree> handlers) {
    return new QSJsonHandlersSettingsEditorTree(handlers);
  }
}
