package qs.platform.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import qs.platform.QSHandlerCodeVersion;
import qs.platform.QSHandlerSettingsEditor;
import qs.platform.QSHandlerSettingsEditorTree;
import qs.platform.QSHandlersSettingsEditorService;
import qs.platform.converters.QSConverter;
import qs.platform.converters.QSConverterService;
import qs.platform.exceptions.QSAppRuntimeException;
import qs.platform.rest.headers.QSHttpHeaders;
import qs.platform.rest.json.beans.*;
import qs.platform.rest.json.validation.QSCreate;
import qs.platform.rest.json.validation.QSUpdate;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.util.Collection;

@RestController
@RequestMapping("/bot/v1/${qs.bot.routing.service-name}/handlers/editor")
public class QSHandlersSettingsEditorController {
  private QSHandlersSettingsEditorService settingsService;
  private QSConverter<QSHandlerSettingsEditorTree, QSJsonHandlerSettingsEditorTree> treeConverter;
  private QSConverter<QSHandlerSettingsEditor, QSJsonHandlerSettingsEditor> settingsBeanConverter;
  private QSConverterService converterService;
  @Value("${qs.bot.secret}")
  private String secret;

  @Autowired
  private void setTreeConverter(QSConverter<QSHandlerSettingsEditorTree, QSJsonHandlerSettingsEditorTree> treeConverter) {
    this.treeConverter = treeConverter;
  }

  @Autowired
  private void setSettingsBeanConverter(QSConverter<QSHandlerSettingsEditor, QSJsonHandlerSettingsEditor> settingsBeanConverter) {
    this.settingsBeanConverter = settingsBeanConverter;
  }

  @Autowired
  private void setSettingsService(QSHandlersSettingsEditorService settingsService) {
    this.settingsService = settingsService;
  }

  @Autowired
  private void setConverterService(QSConverterService converterService) {
    this.converterService = converterService;
  }

  @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseEntity<QSJsonHandlerSettingsEditor> save(@Validated(QSCreate.class) @RequestBody QSJsonHandlerSettingsEditor handlerSettings,
                                                          @RequestHeader(value = QSHttpHeaders.X_QS_SERVICE_SECRET) String secret) {
    if (!this.secret.equals(secret)) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
    QSJsonHandlerSettingsEditor saved = settingsBeanConverter.convert(
      settingsService.save(converterService.convertTo(QSHandlerSettingsEditor.class, handlerSettings))
    );
    return new ResponseEntity<>(saved, HttpStatus.CREATED);
  }

  @GetMapping(value = "/tree", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<QSJsonHandlersSettingsEditorTree> getTree(@RequestHeader(value = QSHttpHeaders.X_QS_SERVICE_SECRET) String secret) {
    if (!this.secret.equals(secret)) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
    Collection<QSJsonHandlerSettingsEditorTree> tree = treeConverter.convert(settingsService.getTree());
    return new ResponseEntity<>(QSJsonHandlersSettingsEditorTree.from(tree), HttpStatus.OK);
  }

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<QSJsonHandlerSettingsEditor> getByCode(@RequestParam("code") String code,
                                                               @RequestHeader(value =
                                                                 QSHttpHeaders.X_QS_SERVICE_SECRET) String secret) {
    if (!this.secret.equals(secret)) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
    QSJsonHandlerSettingsEditor handlerSettings = settingsBeanConverter.convert(settingsService.getByCode(code));
    return new ResponseEntity<>(handlerSettings, HttpStatus.OK);
  }

  @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<QSJsonHandlerSettingsEditor> update(@Validated(QSUpdate.class)
                                                            @RequestBody QSJsonHandlerSettingsEditor bean,
                                                            @RequestHeader(value =
                                                              QSHttpHeaders.X_QS_SERVICE_SECRET) String secret) {
    if (!this.secret.equals(secret)) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    QSHandlerSettingsEditor updated = settingsService.update(converterService.convertTo(QSHandlerSettingsEditor.class, bean));
    return new ResponseEntity<>(settingsBeanConverter.convert(updated), HttpStatus.OK);
  }

  @DeleteMapping
  public ResponseEntity<Void> logicalDelete(@RequestParam @NotNull String code,
                                            @RequestParam @NotNull @Min(0) @Max(Integer.MAX_VALUE) Integer version,
                                            @RequestHeader(value =
                                              QSHttpHeaders.X_QS_SERVICE_SECRET) String secret) {
    if (!this.secret.equals(secret)) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    settingsService.logicalDelete(code, version);

    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PutMapping("/recover")
  public ResponseEntity<Void> logicalRecovery(@RequestParam @NotNull String code,
                                              @RequestParam @NotNull @Min(0) @Max(Integer.MAX_VALUE) Integer version,
                                              @RequestHeader(value =
                                                QSHttpHeaders.X_QS_SERVICE_SECRET) String secret) {
    if (!this.secret.equals(secret)) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    settingsService.logicalRecovery(code, version);

    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PostMapping("/actualize")
  public ResponseEntity<Void> actualize(@RequestBody QSJsonHandlersActualizeRequest handlers, @RequestHeader(value =
    QSHttpHeaders.X_QS_SERVICE_SECRET) String secret) {
    if (!this.secret.equals(secret)) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    Collection<QSJsonHandlerCodeVersion> handlersCodeVersion = handlers.getHandlers();

    if (CollectionUtils.isEmpty(handlersCodeVersion)) {
      throw new QSAppRuntimeException("handlers.toActualize.isBlank",
        "List of handlers' settings to actualize can not be empty or null");
    }

    settingsService.actualize(
      handlers.getHandlers().stream()
        .map(handler ->
          converterService.convertTo(QSHandlerCodeVersion.class, handler)
        ).toList()
    );

    return new ResponseEntity<>(HttpStatus.OK);
  }
}
