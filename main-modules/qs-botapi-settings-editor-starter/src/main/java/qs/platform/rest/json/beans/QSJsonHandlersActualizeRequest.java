package qs.platform.rest.json.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Collection;

/**
 * Request to actualize handlers' settings
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QSJsonHandlersActualizeRequest {
  /**
   * collection of {@link QSJsonHandlerCodeVersion handlers' codes and versions}
   */
  private Collection<QSJsonHandlerCodeVersion> handlers;

  public QSJsonHandlersActualizeRequest() {
  }

  public QSJsonHandlersActualizeRequest(Collection<QSJsonHandlerCodeVersion> handlers) {
    this.handlers = handlers;
  }

  public Collection<QSJsonHandlerCodeVersion> getHandlers() {
    return handlers;
  }

  public QSJsonHandlersActualizeRequest setHandlers(Collection<QSJsonHandlerCodeVersion> handlers) {
    this.handlers = handlers;
    return this;
  }
}
