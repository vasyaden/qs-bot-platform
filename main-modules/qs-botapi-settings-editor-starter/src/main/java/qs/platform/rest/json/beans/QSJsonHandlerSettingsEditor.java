package qs.platform.rest.json.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import qs.platform.rest.json.validation.QSCreate;
import qs.platform.rest.json.validation.QSUpdate;

import jakarta.validation.constraints.AssertFalse;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Null;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QSJsonHandlerSettingsEditor {
  @NotNull(groups = {QSCreate.class, QSUpdate.class})
  private String code;
  @NotNull(groups = QSCreate.class)
  private String title;
  private String parentCode;
  private String description;
  private Collection<String> filters;
  private String dataQuery;
  private Boolean unique;
  @Null(groups = QSCreate.class)
  @NotNull(groups = QSUpdate.class)
  @AssertFalse(groups = QSUpdate.class)
  private Boolean deleted;
  @NotNull(groups = {QSCreate.class, QSUpdate.class})
  private String scenarioCode;
  @NotNull(groups = {QSCreate.class, QSUpdate.class})
  private Map<String, Object> scenarioParams;
  @NotNull(groups = QSCreate.class)
  private Integer order;
  @Null(groups = QSCreate.class)
  private LocalDateTime creationDate;
  @Null(groups = QSCreate.class)
  private LocalDateTime changeDate;
  @NotNull(groups = QSUpdate.class)
  @Null(groups = QSCreate.class)
  private Integer version;

  public QSJsonHandlerSettingsEditor() {
  }

  private QSJsonHandlerSettingsEditor(String code, String title, String parentCode, String description,
                                      Collection<String> filters, String dataQuery, Boolean unique, Boolean deleted,
                                      String scenarioCode, Map<String, Object> scenarioParams, Integer order,
                                      LocalDateTime creationDate, LocalDateTime changeDate, Integer version) {
    this.code = code;
    this.title = title;
    this.parentCode = parentCode;
    this.description = description;
    this.filters = filters;
    this.dataQuery = dataQuery;
    this.unique = unique;
    this.deleted = deleted;
    this.scenarioCode = scenarioCode;
    this.scenarioParams = scenarioParams;
    this.order = order;
    this.creationDate = creationDate;
    this.changeDate = changeDate;
    this.version = version;
  }

  public static Builder builder() {
    return new Builder();
  }

  public String getCode() {
    return code;
  }

  public String getTitle() {
    return title;
  }

  public String getDescription() {
    return description;
  }

  public String getParentCode() {
    return parentCode;
  }

  public Boolean getDeleted() {
    return deleted;
  }

  public Integer getOrder() {
    return order;
  }

  public boolean isUnique() {
    return unique;
  }

  public Collection<String> getFilters() {
    return filters;
  }

  public String getDataQuery() {
    return dataQuery;
  }

  public String getScenarioCode() {
    return scenarioCode;
  }

  public Map<String, Object> getScenarioParams() {
    return scenarioParams;
  }

  public LocalDateTime getCreationDate() {
    return creationDate;
  }

  public Boolean getUnique() {
    return unique;
  }

  public LocalDateTime getChangeDate() {
    return changeDate;
  }

  public Integer getVersion() {
    return version;
  }

  public static final class Builder {
    private String code;
    private String title;
    private String parentCode;
    private String description;
    private Collection<String> filters;
    private String dataQuery;
    private boolean unique;
    private Boolean deleted;
    private String scenarioCode;
    private Map<String, Object> scenarioParams;
    private Integer order;
    private LocalDateTime creationDate;
    private LocalDateTime changeDate;
    private Integer version;

    private Builder() {
    }

    public Builder code(String code) {
      this.code = code;
      return this;
    }

    public Builder title(String title) {
      this.title = title;
      return this;
    }

    public Builder description(String description) {
      this.description = description;
      return this;
    }

    public Builder parentCode(String parentCode) {
      this.parentCode = parentCode;
      return this;
    }

    public Builder deleted(boolean deleted) {
      this.deleted = deleted;
      return this;
    }

    public Builder order(Integer order) {
      this.order = order;
      return this;
    }

    public Builder filters(Collection<String> filters) {
      this.filters = filters;
      return this;
    }

    public Builder unique(boolean unique) {
      this.unique = unique;
      return this;
    }


    public Builder dataQuery(String dataQuery) {
      this.dataQuery = dataQuery;
      return this;
    }

    public Builder scenarioCode(String scenarioCode) {
      this.scenarioCode = scenarioCode;
      return this;
    }

    public Builder scenarioParams(Map<String, Object> scenarioParams) {
      this.scenarioParams = scenarioParams;
      return this;
    }

    public Builder creationDate(LocalDateTime creationDate) {
      this.creationDate = creationDate;
      return this;
    }

    public Builder changeDate(LocalDateTime changeDate) {
      this.changeDate = changeDate;
      return this;
    }

    public Builder version(Integer version) {
      this.version = version;
      return this;
    }

    public QSJsonHandlerSettingsEditor build() {
      return new QSJsonHandlerSettingsEditor(code, title, parentCode, description, filters, dataQuery, unique, deleted, scenarioCode, scenarioParams, order, creationDate, changeDate, version);
    }
  }
}
