package qs.platform.converters;

import org.springframework.stereotype.Service;
import qs.platform.QSEntityBeanConverter;
import qs.platform.QSHandlerSettingsEditor;
import qs.platform.beans.QSHandlerSettingsEditorBean;
import qs.platform.beans.QSHandlerSettingsEditorEntity;

@Service
public class QSHandlersSettingsEditorConverter implements QSEntityBeanConverter<QSHandlerSettingsEditorEntity, QSHandlerSettingsEditor> {
  @Override
  public QSHandlerSettingsEditor toBean(QSHandlerSettingsEditorEntity entity) {
    return QSHandlerSettingsEditorBean.builder()
      .code(entity.getCode())
      .title(entity.getTitle())
      .parentCode(entity.getParentCode())
      .description(entity.getDescription())
      .filters(entity.getFilters())
      .dataQuery(entity.getDataQuery())
      .unique(entity.isUnique())
      .scenarioCode(entity.getScenarioCode())
      .scenarioParams(entity.getScenarioParams())
      .order(entity.getOrder())
      .creationDate(entity.getCreationDate())
      .changeDate(entity.getChangeDate())
      .deleted(entity.getDeleted())
      .version(entity.getVersion())
      .build();
  }

  @Override
  public QSHandlerSettingsEditorEntity toEntity(QSHandlerSettingsEditor bean) {
    return QSHandlerSettingsEditorEntity.builder()
      .code(bean.getCode())
      .title(bean.getTitle())
      .parentCode(bean.getParentCode())
      .description(bean.getDescription())
      .filters(bean.getFilters())
      .dataQuery(bean.getDataQuery())
      .unique(bean.isUnique())
      .scenarioCode(bean.getScenarioCode())
      .scenarioParams(bean.getScenarioParams())
      .order(bean.getOrder())
      .creationDate(bean.getCreationDate())
      .changeDate(bean.getChangeDate())
      .deleted(bean.getDeleted())
      .version(bean.getVersion())
      .build();
  }
}
