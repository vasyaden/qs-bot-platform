package qs.platform.converters;

import org.apache.commons.collections4.CollectionUtils;
import qs.platform.exceptions.QSConverterException;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Type converter
 *
 * @param <I> input type
 * @param <O> output type
 */
public interface QSConverter<I, O> {
  /**
   * Converts input object to output type
   *
   * @param value input object
   * @return object converted to output type
   */
  @SuppressWarnings("unchecked")
  default O convertObject(Object value) {
    if (value == null) {
      return null;
    }
    if (getInputType().isInstance(value)) {
      return convert((I) value);
    } else {
      throw new QSConverterException(
        "qs.invalid.converter",
        "Invalid type passed: expected " + getInputType().getTypeName() +
          " but passed " + value.getClass().getTypeName()
      );
    }
  }

  /**
   * Converts input value to output type
   *
   * @param value input value
   * @return object converted to output type
   */
  O convert(I value);

  /**
   * Converts input values to output type
   *
   * @param values input values
   * @return objects converted to output type
   */
  default Collection<O> convert(Collection<I> values) {
    return CollectionUtils.isEmpty(values) ?
      Collections.emptyList() : values.stream()
      .filter(Objects::nonNull)
      .map(this::convert)
      .collect(Collectors.toList());
  }

  /**
   * Input type
   *
   * @return Input type
   */
  Class<I> getInputType();

  /**
   * Output type
   *
   * @return Output type
   */
  Class<O> getOutputType();
}
