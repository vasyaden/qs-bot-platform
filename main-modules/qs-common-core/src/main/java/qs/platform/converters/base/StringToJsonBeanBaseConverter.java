package qs.platform.converters.base;

import org.springframework.beans.factory.annotation.Autowired;
import qs.platform.converters.QSConverter;
import qs.platform.json.QSJsonMapper;

/**
 * Base class for {@link String} to json-bean  conversion
 *
 * @param <J> type of json-bean
 */
public abstract class StringToJsonBeanBaseConverter<J> implements QSConverter<String, J> {
  private QSJsonMapper jsonMapper;

  @Autowired
  private void setJsonMapper(QSJsonMapper jsonMapper) {
    this.jsonMapper = jsonMapper;
  }

  @Override
  public J convert(String value) {
    return jsonMapper.toObject(getOutputType(), value);
  }

  @Override
  public Class<String> getInputType() {
    return String.class;
  }

}
