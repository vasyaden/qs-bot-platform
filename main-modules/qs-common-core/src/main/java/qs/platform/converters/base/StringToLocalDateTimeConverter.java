package qs.platform.converters.base;

import org.springframework.stereotype.Service;
import qs.platform.converters.QSConverter;

import java.time.LocalDateTime;

/**
 * {@link String} to {@link LocalDateTime} converter
 */
@Service
public class StringToLocalDateTimeConverter implements QSConverter<String, LocalDateTime> {
  /**
   * Converts {@link String} value to {@link LocalDateTime}
   *
   * @param value input value
   * @return {@link LocalDateTime converted value}
   */
  @Override
  public LocalDateTime convert(String value) {
    return value == null ? null : LocalDateTime.parse(value.trim());
  }

  @Override
  public Class<String> getInputType() {
    return String.class;
  }

  @Override
  public Class<LocalDateTime> getOutputType() {
    return LocalDateTime.class;
  }
}
