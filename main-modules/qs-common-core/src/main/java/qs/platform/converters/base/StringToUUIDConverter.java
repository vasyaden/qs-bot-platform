package qs.platform.converters.base;

import org.springframework.stereotype.Service;
import qs.platform.converters.QSConverter;

import java.util.UUID;

/**
 * {@link String} to {@link UUID} converter
 */
@Service
public class StringToUUIDConverter implements QSConverter<String, UUID> {
  /**
   * Converts {@link String} value to {@link UUID}
   *
   * @param value input value
   * @return {@link UUID converted value}
   */
  public UUID convert(String value) {
    if (value == null) {
      return null;
    }
    return UUID.fromString(value);
  }

  public Class<String> getInputType() {
    return String.class;
  }

  public Class<UUID> getOutputType() {
    return UUID.class;
  }
}
