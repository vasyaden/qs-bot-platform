package qs.platform.converters.base;

import org.springframework.stereotype.Service;
import qs.platform.converters.QSConverter;

/**
 * {@link String} to {@link Boolean} converter
 */
@Service
public class StringToBooleanConverter implements QSConverter<String, Boolean> {
  /**
   * Converts {@link String} value to {@link Boolean}
   *
   * @param value input value
   * @return {@link Boolean converted value}
   */
  @Override
  public Boolean convert(String value) {
    return value == null ? null : Boolean.parseBoolean(value.trim());
  }

  @Override
  public Class<String> getInputType() {
    return String.class;
  }

  @Override
  public Class<Boolean> getOutputType() {
    return Boolean.class;
  }
}
