package qs.platform.converters.base;

import org.springframework.stereotype.Service;
import qs.platform.converters.QSConverter;

/**
 * {@link Integer} to {@link Long} converter
 */
@Service
public class IntegerToLongConverter implements QSConverter<Integer, Long> {
  /**
   * Converts {@link Integer} value to {@link Long}
   *
   * @param value input value
   * @return {@link Long converted value}
   */
  @Override
  public Long convert(Integer value) {
    return value == null ? null : value.longValue();
  }

  @Override
  public Class<Integer> getInputType() {
    return Integer.class;
  }

  @Override
  public Class<Long> getOutputType() {
    return Long.class;
  }
}
