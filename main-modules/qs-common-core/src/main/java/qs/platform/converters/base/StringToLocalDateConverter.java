package qs.platform.converters.base;

import org.springframework.stereotype.Service;
import qs.platform.converters.QSConverter;

import java.time.LocalDate;

/**
 * {@link String} to {@link LocalDate} converter
 */
@Service
public class StringToLocalDateConverter implements QSConverter<String, LocalDate> {
  /**
   * Converts {@link String} value to {@link LocalDate}
   *
   * @param value input value
   * @return {@link LocalDate converted value}
   */
  @Override
  public LocalDate convert(String value) {
    if (value == null) {
      return null;
    }
    String trimmedValue = value.trim();
    if (trimmedValue.length() > 10) {
      return LocalDate.parse(trimmedValue.substring(0, 10));
    }
    return LocalDate.parse(trimmedValue);
  }

  @Override
  public Class<String> getInputType() {
    return String.class;
  }

  @Override
  public Class<LocalDate> getOutputType() {
    return LocalDate.class;
  }
}
