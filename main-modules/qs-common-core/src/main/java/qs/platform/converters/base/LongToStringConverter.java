package qs.platform.converters.base;

import org.springframework.stereotype.Service;
import qs.platform.converters.QSConverter;

/**
 * {@link Long} to {@link String} converter
 */
@Service
public class LongToStringConverter implements QSConverter<Long, String> {
  /**
   * Converts {@link Long} value to {@link String}
   *
   * @param value input value
   * @return {@link String converted value}
   */
  @Override
  public String convert(Long value) {
    return value == null ? null : value.toString();
  }

  @Override
  public Class<Long> getInputType() {
    return Long.class;
  }

  @Override
  public Class<String> getOutputType() {
    return String.class;
  }
}
