package qs.platform.converters.base;

import org.springframework.stereotype.Service;
import qs.platform.converters.QSConverter;

/**
 * {@link String} to {@link Long} converter
 */
@Service
public class StringToLongConverter implements QSConverter<String, Long> {
  /**
   * Converts {@link String} value to {@link Long}
   *
   * @param value input value
   * @return {@link Long converted value}
   */
  @Override
  public Long convert(String value) {
    return value == null ? null : Long.parseLong(value.trim());
  }

  @Override
  public Class<String> getInputType() {
    return String.class;
  }

  @Override
  public Class<Long> getOutputType() {
    return Long.class;
  }
}
