package qs.platform.converters.base;

import org.springframework.stereotype.Service;
import qs.platform.converters.QSConverter;

/**
 * {@link String} to {@link Integer} converter
 */
@Service
public class StringToIntegerConverter implements QSConverter<String, Integer> {
  /**
   * Converts {@link String} value to {@link Integer}
   *
   * @param value input value
   * @return {@link Integer converted value}
   */
  @Override
  public Integer convert(String value) {
    return value == null ? null : Integer.parseInt(value.trim());
  }

  @Override
  public Class<String> getInputType() {
    return String.class;
  }

  @Override
  public Class<Integer> getOutputType() {
    return Integer.class;
  }
}
