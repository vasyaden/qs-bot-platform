package qs.platform.converters.base;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import qs.platform.converters.QSConverter;
import qs.platform.json.QSJsonMapper;

/**
 * Base class for {@link JsonNode} to json-bean  conversion
 *
 * @param <J> type of json-bean
 */
public abstract class JsonNodeToJsonBeanBaseConverter<J> implements QSConverter<JsonNode, J> {
  private QSJsonMapper jsonMapper;

  @Autowired
  private void setJsonMapper(QSJsonMapper jsonMapper) {
    this.jsonMapper = jsonMapper;
  }

  @Override
  public J convert(JsonNode value) {
    return jsonMapper.toObject(getOutputType(), value);
  }

  @Override
  public Class<JsonNode> getInputType() {
    return JsonNode.class;
  }
}
