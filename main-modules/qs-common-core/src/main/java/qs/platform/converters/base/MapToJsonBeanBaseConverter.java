package qs.platform.converters.base;

import org.springframework.beans.factory.annotation.Autowired;
import qs.platform.converters.QSConverter;
import qs.platform.json.QSJsonMapper;

import java.util.Map;

/**
 * Base class for {@link Map} to json-bean  conversion
 *
 * @param <J> type of json-bean
 */
public abstract class MapToJsonBeanBaseConverter<J> implements QSConverter<Map, J> {
  private QSJsonMapper jsonMapper;

  @Autowired
  private void setJsonMapper(QSJsonMapper jsonMapper) {
    this.jsonMapper = jsonMapper;
  }

  @Override
  public J convert(Map value) {
    return jsonMapper.toObject(getOutputType(), jsonMapper.toJsonString(value));
  }

  @Override
  public Class<Map> getInputType() {
    return Map.class;
  }

}
