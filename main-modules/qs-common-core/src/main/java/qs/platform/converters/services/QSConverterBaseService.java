package qs.platform.converters.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import qs.platform.converters.QSConverter;
import qs.platform.converters.QSConverterService;
import qs.platform.exceptions.QSConverterException;
import qs.platform.exceptions.QSConverterNotFoundException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Base implementation of the {@link QSConverterService}
 */
@Service
public class QSConverterBaseService implements QSConverterService {
  /**
   * All converters
   */
  private final Map<Class<?>, Map<Class<?>, QSConverter<?, ?>>> converters = new HashMap<>();

  @Autowired(required = false)
  private void setConverters(Collection<QSConverter<?, ?>> converters) {
    if (converters == null) {
      return;
    }
    for (QSConverter<?, ?> converter : converters) {
      this.converters.computeIfAbsent(converter.getInputType(), x -> new HashMap<>()).put(converter.getOutputType(), converter);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T> T convertTo(Class<T> to, Object value) {
    if (value == null) {
      return null;
    }
    if (to == null) {
      throw new IllegalArgumentException("Unset output type <to>");
    }
    if (to.isInstance(value)) {
      if (to == String.class) {
        String s = ((String) value).trim();
        value = s.isEmpty() ? null : s;
      }
      return (T) value;
    }
    Map<Class<?>, QSConverter<?, ?>> inputTypeConverters = converters.get(value.getClass());
    if (inputTypeConverters == null && value instanceof Collection) {
      inputTypeConverters = converters.get(Collection.class);
    }
    if (inputTypeConverters == null) {
      throw new QSConverterNotFoundException(String.format("Not found converter from {%s} to {%s} class", value.getClass(), to));
    }
    QSConverter<?, ?> converter = inputTypeConverters.get(to);
    if (converter == null) {
      throw new QSConverterNotFoundException(String.format("Not found converter from {%s} to {%s} class", value.getClass(), to));
    }
    try {
      return (T) converter.convertObject(value);
    } catch (Exception e) {
      throw new QSConverterException("qs.converter.service", e.getMessage());
    }
  }
}
