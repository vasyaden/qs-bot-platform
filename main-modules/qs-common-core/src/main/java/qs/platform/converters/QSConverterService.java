package qs.platform.converters;

/**
 * Converter service
 */
public interface QSConverterService {
  /**
   * Converts the value to the specified type
   *
   * @param to    Output type
   * @param value value
   * @param <T>   output type
   * @return value converted to the output type
   */
  <T> T convertTo(Class<T> to, Object value);
}
