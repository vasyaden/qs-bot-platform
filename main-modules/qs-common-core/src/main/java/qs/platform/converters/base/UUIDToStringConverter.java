package qs.platform.converters.base;

import org.springframework.stereotype.Service;
import qs.platform.converters.QSConverter;

import java.util.UUID;

/**
 * {@link UUID} to {@link String} converter
 */
@Service
public class UUIDToStringConverter implements QSConverter<UUID, String> {
  /**
   * Converts {@link UUID} value to {@link String}
   *
   * @param value input value
   * @return {@link String converted value}
   */
  @Override
  public String convert(UUID value) {
    return value == null ? null : value.toString();
  }

  @Override
  public Class<UUID> getInputType() {
    return UUID.class;
  }

  @Override
  public Class<String> getOutputType() {
    return String.class;
  }
}
