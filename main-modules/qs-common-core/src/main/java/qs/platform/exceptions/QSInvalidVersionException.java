package qs.platform.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class QSInvalidVersionException extends QSAppRuntimeException {

  private static final String CODE_SUFFIX = ".invalidVersion";

  public QSInvalidVersionException(String code, String message) {
    super(code + CODE_SUFFIX, message);
  }
}
