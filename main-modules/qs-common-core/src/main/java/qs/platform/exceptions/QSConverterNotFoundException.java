package qs.platform.exceptions;

/**
 * Exception when converter not found for given types
 */
public class QSConverterNotFoundException extends QSRuntimeException {

  public QSConverterNotFoundException(String message) {
    super(message);
  }
}
