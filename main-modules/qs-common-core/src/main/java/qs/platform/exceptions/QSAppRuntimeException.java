package qs.platform.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception to be sent to the user
 */

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class QSAppRuntimeException extends QSRuntimeException {

  public QSAppRuntimeException() {
  }

  public QSAppRuntimeException(String code, String message) {
    super(code, message);
  }

  public QSAppRuntimeException(String code, String message, Throwable cause) {
    super(code, message, cause);
  }

  /**
   * Constructs a new runtime exception with the specified detail
   * message, cause, suppression enabled or disabled, and writable
   * stack trace enabled or disabled.
   *
   * @param code               code of the error
   * @param message            the detail message.
   * @param cause              the cause.  (A {@code null} value is permitted,
   *                           and indicates that the cause is nonexistent or unknown.)
   * @param enableSuppression  whether suppression is enabled
   *                           or disabled
   * @param writableStackTrace whether the stack trace should
   *                           be writable
   * @since 1.7
   */
  public QSAppRuntimeException(String code, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(code, message, cause, enableSuppression, writableStackTrace);
  }

}
