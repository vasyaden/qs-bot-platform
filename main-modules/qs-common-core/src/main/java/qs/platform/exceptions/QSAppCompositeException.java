package qs.platform.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

/**
 * Composite exception for {@link QSAppRuntimeException}
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class QSAppCompositeException extends QSAppRuntimeException implements QSCompositeException<QSAppRuntimeException> {
  private final Collection<QSAppRuntimeException> data = new ArrayList<>();
  private final Collection<QSAppRuntimeException> umData = Collections.unmodifiableCollection(data);

  public QSAppCompositeException() {
  }

  public QSAppCompositeException(Collection<QSAppRuntimeException> data) {
    if (data != null) {
      this.data.addAll(data);
    }
  }

  /**
   * Add exception to collection
   *
   * @param e exception
   * @return {@link QSAppCompositeException this}
   */
  public QSAppCompositeException addException(QSAppRuntimeException e) {
    if (e != null) {
      data.add(e);
    }
    return this;
  }

  /**
   * Add exceptions to collection
   *
   * @param exceptions exceptions
   * @return {@link QSAppCompositeException this}
   */
  public QSAppCompositeException addExceptions(Collection<QSAppRuntimeException> exceptions) {
    if (exceptions != null) {
      data.addAll(exceptions);
    }
    return this;
  }

  /**
   * Get collected exceptions
   *
   * @return collected {@link QSAppRuntimeException exceptions}
   */
  public Collection<QSAppRuntimeException> getExceptions() {
    return umData;
  }

  /**
   * Get composite message of all collected exceptions separated by comma and space: {@code ", "}
   *
   * @return composite message of all collected exceptions
   */
  @Override
  public String getMessage() {
    return data.stream().map(QSAppRuntimeException::getMessage).collect(Collectors.joining(", "));
  }

  /**
   * Fill passed list of exceptions by collected ones
   *
   * @param list list of exceptions for filling
   */
  public void fillData(Collection<QSAppRuntimeException> list) {
    list.addAll(data);
  }

  /**
   * Checks is list of collected exceptions empty
   */
  public boolean isEmpty() {
    return CollectionUtils.isEmpty(umData);
  }

  /**
   * Checks is list of collected exceptions not empty
   */
  public boolean isNotEmpty() {
    return !isEmpty();
  }
}
