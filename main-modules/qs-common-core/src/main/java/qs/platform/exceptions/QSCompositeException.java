package qs.platform.exceptions;

import java.util.Collection;

/**
 * Composite exception
 *
 * @param <T> Exception type
 */
public interface QSCompositeException<T extends Exception> {
  /**
   * Get collected exceptions
   */
  Collection<T> getExceptions();
}
