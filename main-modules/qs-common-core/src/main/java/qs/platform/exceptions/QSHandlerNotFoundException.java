package qs.platform.exceptions;

/**
 * Exception to be thrown when handler was not found or not resolved
 */
public class QSHandlerNotFoundException extends QSAppRuntimeException {

  public QSHandlerNotFoundException(String code, String message) {
    super(code, message);
  }
}
