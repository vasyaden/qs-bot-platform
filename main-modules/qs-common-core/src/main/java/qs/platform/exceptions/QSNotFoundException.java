package qs.platform.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * QSNotFoundException
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class QSNotFoundException extends QSAppRuntimeException {

  private static final String CODE_SUFFIX = ".notFound";

  public QSNotFoundException(String code, String message) {
    super(code + CODE_SUFFIX, message);
  }

  public QSNotFoundException(String code, String message, Throwable cause) {
    super(code, message, cause);
  }

  /**
   * Constructs a new runtime exception with the specified detail
   * message, cause, suppression enabled or disabled, and writable
   * stack trace enabled or disabled.
   *
   * @param code               code of the error
   * @param message            the detail message.
   * @param cause              the cause.  (A {@code null} value is permitted,
   *                           and indicates that the cause is nonexistent or unknown.)
   * @param enableSuppression  whether suppression is enabled
   *                           or disabled
   * @param writableStackTrace whether the stack trace should
   *                           be writable
   * @since 1.7
   */
  public QSNotFoundException(String code, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(code + CODE_SUFFIX, message, cause, enableSuppression, writableStackTrace);
  }
}
