package qs.platform.exceptions;

/**
 * Exception to be thrown when user has no permissions for use the invoked handler
 */
public class QSHandlerForbiddenException extends QSAppRuntimeException {

  private static final String CODE = "qs.handler.forbidden";
  private static final String MESSAGE = "У вас нет доступа к этому действию";

  public QSHandlerForbiddenException() {
    super(CODE, MESSAGE);
  }
}
