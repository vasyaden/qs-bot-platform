package qs.platform.exceptions;

/**
 * Conversion exception
 */
public class QSConverterException extends QSAppRuntimeException {
  /**
   * Conversion exception
   *
   * @param messageCode error code
   * @param message     description
   */
  public QSConverterException(String messageCode, String message) {
    super(messageCode, message);
  }
}
