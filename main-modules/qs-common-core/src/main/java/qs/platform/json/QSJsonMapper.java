package qs.platform.json;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.json.JsonMapper;

/**
 * Service for JSON serialization/deserialization
 */
public interface QSJsonMapper {
  /**
   * Get {@link JsonMapper} instance
   */
  JsonMapper getJsonMapper();

  /**
   * Deserialize JSON-string to passed class
   *
   * @param clazz target class
   * @param json  json-string
   * @return object of passed class
   */
  <T> T toObject(Class<T> clazz, String json);

  /**
   * Deserialize {@link JsonNode} to passed class
   *
   * @param clazz target class
   * @param json  {@link JsonNode json}
   * @return object of passed class
   */
  <T> T toObject(Class<T> clazz, JsonNode json);

  /**
   * Deserialize JSON-string to passed type reference
   *
   * @param typeReference target type reference
   * @param json          json-string
   * @return object of given type
   */
  <T> T toObject(TypeReference<T> typeReference, String json);

  /**
   * Serialize object to JSON-string
   *
   * @param jsonObject object
   * @return JSON-string
   */
  String toJsonString(Object jsonObject);

}
