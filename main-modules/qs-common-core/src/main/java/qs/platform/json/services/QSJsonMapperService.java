package qs.platform.json.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import qs.platform.exceptions.QSRuntimeException;
import qs.platform.json.QSJsonMapper;

@Service
public class QSJsonMapperService implements QSJsonMapper {

  private static final JsonMapper JSON_MAPPER = JsonMapper.builder()
    .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
    .addModule(new JavaTimeModule())
    .build();

  @Override
  public JsonMapper getJsonMapper() {
    return JSON_MAPPER;
  }

  @Override
  public <T> T toObject(Class<T> clazz, String json) {
    if (clazz == null) {
      throw new QSRuntimeException("Deserialization class should not be null");
    }
    if (StringUtils.isBlank(json)) {
      return null;
    }
    try {
      return JSON_MAPPER.readValue(json, clazz);
    } catch (JsonProcessingException e) {
      throw new QSRuntimeException(
        "Can not deserialize json: " + json + " to " + clazz.getSimpleName() + ", cause " + e.getMessage(),
        e
      );
    }
  }

  @Override
  public <T> T toObject(Class<T> clazz, JsonNode json) {
    if (clazz == null) {
      throw new QSRuntimeException("Deserialization class should not be null");
    }
    if (json == null || json instanceof NullNode) {
      return null;
    }
    try {
      return JSON_MAPPER.treeToValue(json, clazz);
    } catch (JsonProcessingException e) {
      throw new QSRuntimeException(
        "Can not deserialize json: " + json + " to " + clazz.getSimpleName() + ", cause " + e.getMessage(),
        e
      );
    }
  }

  @Override
  public <T> T toObject(TypeReference<T> typeReference, String json) {
    if (typeReference == null) {
      throw new QSRuntimeException("Deserialization class should not be null");
    }
    if (StringUtils.isBlank(json)) {
      return null;
    }
    try {
      return JSON_MAPPER.readValue(json, typeReference);
    } catch (JsonProcessingException e) {
      throw new QSRuntimeException(
        "Can not deserialize json: " + json + " to " + typeReference.getType().getTypeName() +
          ", cause " + e.getMessage(),
        e
      );
    }
  }

  @Override
  public String toJsonString(Object jsonObject) {
    if (jsonObject == null) {
      return null;
    }
    try {
      return JSON_MAPPER.writeValueAsString(jsonObject);
    } catch (JsonProcessingException e) {
      throw new QSRuntimeException("Can not serialize object: " + jsonObject + ", cause " + e.getMessage(), e);
    }
  }
}
