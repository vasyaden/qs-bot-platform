package qs.platform.rest.headers;

import org.springframework.http.HttpHeaders;

public class QSHttpHeaders extends HttpHeaders {
  public static final String X_QS_SERVICE_SECRET = "X-QS-Service-Secret";
}
