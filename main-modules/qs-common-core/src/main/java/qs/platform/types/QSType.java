package qs.platform.types;

/**
 * Enum with types
 */
public enum QSType {
  INTEGER("int", Integer.class),
  STRING("string", String.class),
  BOOLEAN("boolean", Boolean.class);


  private final String type;
  private final Class<?> clazz;

  QSType(String type, Class<?> clazz) {
    this.type = type;
    this.clazz = clazz;
  }

  /**
   * Code of the type
   */
  public String getType() {
    return type;
  }

  /**
   * Java class of the type
   */
  public Class<?> getClazz() {
    return clazz;
  }
}
