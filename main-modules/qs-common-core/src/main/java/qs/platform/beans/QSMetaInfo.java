package qs.platform.beans;

import java.time.LocalDateTime;

public interface QSMetaInfo {

  Long getUserCreator();

  LocalDateTime getCreationDate();

  Long getUserEditor();

  LocalDateTime getChangeDate();

}
