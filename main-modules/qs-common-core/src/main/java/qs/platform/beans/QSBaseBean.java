package qs.platform.beans;

/**
 * BaseBean
 */

public interface QSBaseBean<I extends Comparable<I>> {

  I getId();

  Integer getVersion();

  Boolean getDeleted();

  QSMetaInfo getMetaInfo();

}
