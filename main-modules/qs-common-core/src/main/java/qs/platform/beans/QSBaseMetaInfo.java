package qs.platform.beans;

import java.time.LocalDateTime;

/**
 * QSBaseMetaInfo
 */

public class QSBaseMetaInfo implements QSMetaInfo {

  private final Long userCreator;

  private final LocalDateTime creationDate;

  private final Long userEditor;

  private final LocalDateTime changeDate;

  private QSBaseMetaInfo(Long userCreator, LocalDateTime creationDate, Long userEditor, LocalDateTime changeDate) {
    this.userCreator = userCreator;
    this.creationDate = creationDate;
    this.userEditor = userEditor;
    this.changeDate = changeDate;
  }

  @Override
  public Long getUserCreator() {
    return userCreator;
  }

  @Override
  public LocalDateTime getCreationDate() {
    return creationDate;
  }

  @Override
  public Long getUserEditor() {
    return userEditor;
  }

  @Override
  public LocalDateTime getChangeDate() {
    return changeDate;
  }

  public static Builder builder() {
    return new Builder();
  }

  public static final class Builder {
    private Long userCreator;
    private LocalDateTime creationDate;
    private Long userEditor;
    private LocalDateTime changeDate;

    private Builder() {
    }

    public Builder userCreator(Long userCreator) {
      this.userCreator = userCreator;
      return this;
    }

    public Builder creationDate(LocalDateTime creationDate) {
      this.creationDate = creationDate;
      return this;
    }

    public Builder userEditor(Long userEditor) {
      this.userEditor = userEditor;
      return this;
    }

    public Builder changeDate(LocalDateTime changeDate) {
      this.changeDate = changeDate;
      return this;
    }

    public QSBaseMetaInfo build() {
      return new QSBaseMetaInfo(userCreator, creationDate, userEditor, changeDate);
    }
  }
}
