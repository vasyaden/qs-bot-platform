package qs.platform.botapi.events;


import org.springframework.context.ApplicationEvent;

/**
 * this event indicates that the QSHandlerCache has been refreshed
 */
public class QSHandlersCacheRefreshedEvent extends ApplicationEvent {

  private QSHandlersCacheRefreshedEvent(Object source) {
    super(source);
  }

  public static Builder builder() {
    return new Builder();
  }

  public static final class Builder {
    private Object source;

    public Builder source(Object source) {
      this.source = source;
      return this;
    }

    public QSHandlersCacheRefreshedEvent build() {
      return new QSHandlersCacheRefreshedEvent(source);
    }
  }
}
