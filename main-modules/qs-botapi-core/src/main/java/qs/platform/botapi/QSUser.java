package qs.platform.botapi;

/**
 * User who initiated the event
 */
public interface QSUser {
  /**
   * ID of the user
   */
  Long getId();

  /**
   * First name of the user
   */
  String getFirstName();

  /**
   * Last name of the user
   */
  String getLastName();

  /**
   * Username of the user
   */
  String getUserName();

}
