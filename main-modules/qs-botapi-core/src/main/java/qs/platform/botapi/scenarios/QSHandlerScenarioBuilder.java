package qs.platform.botapi.scenarios;

import qs.platform.botapi.QSHandlerSettings;
import qs.platform.botapi.scenarios.annotations.QSHandlerScenarioSetting;

import java.util.Map;

/**
 * Builder of the {@link QSHandlerScenario} parametrized with {@link QSHandlerSettings#getScenarioParams() handler's settings}
 */
public interface QSHandlerScenarioBuilder {
  /**
   * @see QSHandlerScenarioSetting#code()
   */
  default String getCode() {
    QSHandlerScenarioSetting anno = this.getClass()
      .getAnnotation(QSHandlerScenarioSetting.class);
    return anno == null ? null : anno.code();
  }

  /**
   * @see QSHandlerScenarioSetting#title()
   */
  default String getTitle() {
    QSHandlerScenarioSetting anno = this.getClass()
      .getAnnotation(QSHandlerScenarioSetting.class);
    return anno == null ? null : anno.title();
  }

  /**
   * @see QSHandlerScenarioSetting#description()
   */
  default String getDescription() {
    QSHandlerScenarioSetting anno = this.getClass()
      .getAnnotation(QSHandlerScenarioSetting.class);
    return anno == null ? null : anno.description();
  }

  /**
   * Builds the parametrized {@link  QSHandlerScenario scenario}
   *
   * @param params parameters
   * @return {@link  QSHandlerScenario scenario}
   */
  QSHandlerScenario build(Map<String, Object> params);
}
