package qs.platform.botapi;

import org.telegram.telegrambots.meta.api.objects.Contact;
import org.telegram.telegrambots.meta.api.objects.Update;

/**
 * Event from telegram API
 */
public interface QSEvent {
  /**
   * Chat id, where the event came from
   */
  Long getChatId();

  /**
   * Chat id, where the event came from (in string format)
   */
  String getChatIdString();

  /**
   * Type of the event
   */
  QSEventType getType();

  /**
   * Indicates that the event contains a message
   */
  boolean hasMessage();

  /**
   * Indicates that the event contains a callback query
   */
  boolean hasCallbackQuery();

  /**
   * Indicates that the event is of passed type
   *
   * @param type event type for check
   */
  boolean isType(QSEventType type);

  /**
   * Text of the message
   */
  String getMessageText();

  /**
   * Callback data
   */
  String getCallbackData();

  /**
   * Contained contact
   */
  Contact getContact();

  /**
   * ID of the message
   */
  Integer getMessageId();

  /**
   * Text of the message to which the event is a reply
   */
  String getReplyToMessageText();

  /**
   * User who initiated the event
   */
  QSUser getUser();

  /**
   * Telegram update object
   *
   * @deprecated Use this method only when there are no possibilities to deal with others
   */
  @Deprecated
  Update getUpdate();

}
