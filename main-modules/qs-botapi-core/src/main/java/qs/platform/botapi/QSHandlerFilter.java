package qs.platform.botapi;

public interface QSHandlerFilter {

  /**
   * Returns the code by which the {@link QSHandlerFilter filter} can usually be found.
   *
   * @return string code of the filter.
   */
  String getCode();

  /**
   * Return the title (name) of the filter. You can find it hardcoded in each filter. <br>
   *
   * @return string title (name) of the filter.
   */
  String getTitle();

  /**
   * Return the description of the filter. You can find it hardcoded in each filter. <br>
   *
   * @return string description of the filter.
   */
  String getDescription();

  boolean check(QSJsonData jsonData);

}
