package qs.platform.botapi;

import org.telegram.telegrambots.meta.api.objects.Update;
import qs.platform.exceptions.QSRuntimeException;

import java.util.Arrays;
import java.util.function.Predicate;

/**
 * Type of the telegram event
 */
public enum QSEventType {
  /**
   * Message
   */
  MESSAGE(Update::hasMessage),
  /**
   * Callback query
   */
  CALLBACK(Update::hasCallbackQuery);

  private final Predicate<Update> typeChecker;

  QSEventType(Predicate<Update> typeChecker) {
    this.typeChecker = typeChecker;
  }

  private boolean checkType(Update update) {
    return typeChecker.test(update);
  }

  public static QSEventType from(Update update) {
    return Arrays.stream(QSEventType.values())
      .filter(type -> type.checkType(update))
      .findFirst()
      .orElseThrow(() -> new QSRuntimeException("Unknown event type: " + update));
  }
}
