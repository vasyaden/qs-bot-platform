package qs.platform.botapi.scenarios;

import qs.platform.types.QSType;

/**
 * Parameters of the {@link QSHandlerScenarioSetting}
 */
public interface QSScenarioParam {
  /**
   * @see qs.platform.botapi.scenarios.annotations.QSHandlerScenarioParam#code()
   */
  String getCode();

  /**
   * @see qs.platform.botapi.scenarios.annotations.QSHandlerScenarioParam#code()
   */
  QSType getType();

  /**
   * @see qs.platform.botapi.scenarios.annotations.QSHandlerScenarioParam#code()
   */
  String getTitle();

  /**
   * @see qs.platform.botapi.scenarios.annotations.QSHandlerScenarioParam#code()
   */
  default String getDescription() {
    return "";
  }

  /**
   * @see qs.platform.botapi.scenarios.annotations.QSHandlerScenarioParam#required()
   */
  default boolean isRequired() {
    return true;
  }

  /**
   * @see qs.platform.botapi.scenarios.annotations.QSHandlerScenarioParam#multiple()
   */
  default boolean isMultiple() {
    return false;
  }

}
