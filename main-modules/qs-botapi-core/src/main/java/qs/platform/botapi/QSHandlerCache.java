package qs.platform.botapi;

import qs.platform.botapi.handlers.QSHandler;

import java.util.Collection;

/**
 * Cache of the {@link QSHandler handlers}
 * <p>
 * Should be used everywhere to find handler or to get all actual handlers instead of the <s>{@link QSHandlersFactory}</s>
 * to avoid multi-configure of the handlers
 */
public interface QSHandlerCache {
  /**
   * Refreshes the cache
   */
  void refresh();

  /**
   * Get {@code nullable} handler by code
   *
   * @param code code of the handler
   * @return {@link QSHandler}
   */
  QSHandler getHandler(String code);

  /**
   * Get all actual handlers
   *
   * @return {@link QSHandler actual handlers}
   */
  Collection<QSHandler> getHandlers();
}
