package qs.platform.botapi.scenarios.annotations;

import qs.platform.botapi.scenarios.QSHandlerScenario;
import qs.platform.types.QSType;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Describes the parameter of the {@link QSHandlerScenario}
 */
@Target(ElementType.ANNOTATION_TYPE)
@Retention(RUNTIME)
@Documented
public @interface QSHandlerScenarioParam {
  /**
   * Code of the parameter
   */
  String code();

  /**
   * Type of the parameter
   */
  QSType type();

  /**
   * Title of the parameter
   */
  String title();

  /**
   * Description of the parameter
   */
  String description() default "";

  /**
   * Determines whether the parameter is required. By default, is true.
   */
  boolean required() default true;

  /**
   * Determines whether the parameter is a collection. By default, is false.
   */
  boolean multiple() default false;

}

