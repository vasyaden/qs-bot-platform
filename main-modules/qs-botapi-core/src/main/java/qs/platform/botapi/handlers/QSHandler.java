package qs.platform.botapi.handlers;

import org.telegram.telegrambots.meta.api.methods.PartialBotApiMethod;
import qs.platform.botapi.QSEvent;
import qs.platform.botapi.QSJsonData;

import java.util.Collection;
import java.util.Optional;

/**
 * Handler of incoming {@link QSEvent event}
 */
public interface QSHandler extends Comparable<QSHandler> {
  /**
   * Code of the start handler
   */
  String START_CODE = "/start";

  /**
   * Checks if handler may be invoked
   *
   * @param jsonData data
   * @return true/false
   */
  default boolean check(QSJsonData jsonData) {
    return true;
  }

  /**
   * Handle incoming {@link QSEvent event} parametrized with {@link QSJsonData data} and return collection of the
   * {@link PartialBotApiMethod response events}
   *
   * @param event    incoming {@link QSEvent event}
   * @param jsonData {@link QSJsonData data}
   * @return collection of the {@link PartialBotApiMethod response events}
   */
  Collection<PartialBotApiMethod<?>> handle(QSEvent event, QSJsonData jsonData);

  /**
   * Add the handler to children of current one
   *
   * @param handler {@link QSHandler child handler}
   */
  void addChild(QSHandler handler);

  /**
   * Clears children of the handler
   */
  void clearChildren();

  /**
   * Get collection of the {@link QSHandler handler's children}
   *
   * @return collection of the {@link QSHandler handler's children}
   * @apiNote collection is unmodifiable
   */
  Collection<QSHandler> getChildren();

  /**
   * Code of the {@link QSHandler handler}
   */
  String getCode();

  /**
   * Title of the {@link QSHandler handler}, that should be placed on the button
   */
  String getTitle();

  /**
   * Query to prepare parameters of the {@link QSHandler handler}
   */
  default Optional<String> getDataQuery() {
    return Optional.empty();//TODO: make it object with params to get data from different sources and add pagination?
  }

  /**
   * Code of the parent {@link QSHandler handler}
   */
  String getParentCode();

  /**
   * Order number of the handler in menu
   */
  Integer getOrder();

  /**
   * Flag that determines whether the handler's data should be unique or not
   */
  default boolean isUnique() {
    return false;
  }

}
