package qs.platform.botapi;

import qs.platform.botapi.handlers.QSHandler;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Map;

/**
 * Handler's settings bean
 */
public interface QSHandlerSettings {
  /**
   * Handler's code
   */
  String getCode();

  /**
   * The code of the parent handler
   */
  String getParentCode();

  /**
   * The title of the button calling handler
   */
  String getTitle();

  /**
   * The handler's order in menu
   */
  Integer getOrder();

  /**
   * boolean flag that determines whether the handler is unique or not
   */
  boolean isUnique();

  /**
   * Handler's description
   */
  String getDescription();

  /**
   * {@link QSHandlerFilter Filters'} codes
   */
  Collection<String> getFilters();

  /**
   * Query to prepare parameters of the {@link QSHandler handler}
   */
  String getDataQuery();

  /**
   * Code of the scenario
   */
  String getScenarioCode();

  /**
   * Params of the scenario
   */
  Map<String, Object> getScenarioParams();

  /**
   * The date of handler's creation
   */
  LocalDateTime getCreationDate();
}
