package qs.platform.botapi.scenarios;

import java.util.Collection;
import java.util.Collections;

/**
 * Settings for the {@link QSHandlerScenario}
 */
public interface QSHandlerScenarioSetting {
  /**
   * @see qs.platform.botapi.scenarios.annotations.QSHandlerScenarioSetting#code()
   */
  String getCode();

  /**
   * @see qs.platform.botapi.scenarios.annotations.QSHandlerScenarioSetting#title() ()
   */
  String getTitle();

  /**
   * @see qs.platform.botapi.scenarios.annotations.QSHandlerScenarioSetting#description()
   */
  String getDescription();

  /**
   * @see qs.platform.botapi.scenarios.annotations.QSHandlerScenarioSetting#params()
   */
  default Collection<QSScenarioParam> getParams() {
    return Collections.emptyList();
  }
}
