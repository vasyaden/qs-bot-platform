package qs.platform.botapi.scenarios;

import org.telegram.telegrambots.meta.api.methods.PartialBotApiMethod;
import qs.platform.botapi.QSEvent;
import qs.platform.botapi.QSJsonData;
import qs.platform.botapi.handlers.QSHandler;

import java.util.Collection;

/**
 * Scenario of the handling the {@link QSEvent incoming event}
 */
public interface QSHandlerScenario {
  /**
   * Handle incoming {@link QSEvent event} parametrized with {@link QSJsonData data} and return collection of the
   * {@link PartialBotApiMethod response events}
   *
   * @param handler  {@link QSHandler}
   * @param event    {@link QSEvent incoming event}
   * @param jsonData {@link QSJsonData parameters}
   * @return collection of the {@link PartialBotApiMethod response events}
   */
  Collection<PartialBotApiMethod<?>> handle(QSHandler handler, QSEvent event, QSJsonData jsonData);

}
