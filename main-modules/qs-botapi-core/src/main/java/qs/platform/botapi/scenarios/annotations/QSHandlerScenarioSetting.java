package qs.platform.botapi.scenarios.annotations;

import org.springframework.stereotype.Component;
import qs.platform.botapi.scenarios.QSHandlerScenario;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Annotation to describe settings of the {@link QSHandlerScenario}
 */
@Target(ElementType.TYPE)
@Retention(RUNTIME)
@Documented
@Component
public @interface QSHandlerScenarioSetting {
  /**
   * Code of the scenario
   */
  String code();

  /**
   * Title of the scenario
   */
  String title();

  /**
   * Description of the scenario
   */
  String description();

  /**
   * Parameters of the scenario
   */
  QSHandlerScenarioParam[] params() default {};

}
