package qs.platform.boapi.dbaccess.services;


import com.fasterxml.uuid.Generators;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.event.EventListener;
import org.springframework.data.util.Pair;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import qs.platform.botapi.QSHandlerCache;
import qs.platform.botapi.QSJsonData;
import qs.platform.botapi.QSRequestDataService;
import qs.platform.botapi.beans.QSJsonDataBean;
import qs.platform.botapi.dbaccess.converters.QSRequestDataConverter;
import qs.platform.botapi.dbaccess.entities.QSRequestDataEntity;
import qs.platform.botapi.dbaccess.repositories.QSRequestDataRepository;
import qs.platform.botapi.events.QSHandlersCacheRefreshedEvent;
import qs.platform.botapi.handlers.QSHandler;
import qs.platform.exceptions.QSRuntimeException;

import jakarta.annotation.PreDestroy;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toCollection;

@Service
public class QSRequestDataCachedService implements QSRequestDataService {
  private static final Logger LOG = LoggerFactory.getLogger(QSRequestDataCachedService.class);
  private final QSRequestDataRepository repository;
  private final QSRequestDataConverter converter;
  private QSHandlerCache handlerCache;
  private final ConcurrentMap<UUID, Pair<LocalDateTime, QSJsonData>> cache = new ConcurrentHashMap<>();
  private Map<String, Collection<QSJsonData>> unique = new HashMap<>();
  @Value("${qs.bot.json-data.life-time}")
  private long lifetime;

  @Autowired
  public QSRequestDataCachedService(QSRequestDataRepository repository, QSRequestDataConverter converter) {
    this.repository = repository;
    this.converter = converter;
  }

  @Autowired
  public void setHandlerCache(@Lazy QSHandlerCache handlerCache) {
    this.handlerCache = handlerCache;
  }

  @Override
  public QSJsonData save(QSJsonData jsonData) {

    if (jsonData.isUnique()) {
      String handlerCode = jsonData.getCode();
      Map<String, Object> data = jsonData.getData();

      QSJsonData uniqueJson = this.unique.computeIfAbsent(handlerCode, x -> new ArrayList<>())
        .stream()
        .filter(uniqueJsonData -> uniqueJsonData.getData().equals(data))
        .findFirst()
        .orElse(null);

      if (uniqueJson == null) {
        QSJsonData jsonDataWithId = addToCache(jsonData);
        this.unique.get(handlerCode).add(jsonDataWithId);
        return jsonDataWithId;
      }

      return QSJsonDataBean.builder()
        .unique(true)
        .data(uniqueJson.getData())
        .code(handlerCode)
        .previousId(jsonData.getPreviousId())
        .userId(jsonData.getUserId())
        .id(uniqueJson.getId())
        .build();
    }
    return addToCache(jsonData);
  }

  @Override
  public QSJsonData getJsonDataByIdAndUserId(UUID id, Long userId) {
    Pair<LocalDateTime, QSJsonData> jsonDataPair = cache.get(id);
    if (jsonDataPair == null) {
      return repository.findById(id)
        .map(requestData -> {
            try {
              QSJsonData jsonData = converter.toQsBean(requestData, userId);
              this.cache.put(jsonData.getId(), Pair.of(LocalDateTime.now(), jsonData));
              return jsonData;
            } catch (Exception e) {
              throw new QSRuntimeException("Can't read json data: " + e.getMessage(), e);
            }
          }
        )
        .orElseThrow(() -> new QSRuntimeException("Not found request data with id=" + id));
    }

    QSJsonData jsonData = jsonDataPair.getSecond();
    if (!jsonData.getUserId().equals(userId)) {
      return QSJsonDataBean.builder()
        .id(jsonData.getId())
        .code(jsonData.getCode())
        .data(jsonData.getData())
        .userId(userId)
        .previousId(jsonData.getPreviousId())
        .build();
    }
    return jsonData;
  }

  @EventListener
  private void fillUnique(QSHandlersCacheRefreshedEvent event) {
    Collection<String> handlerCodes = handlerCache.getHandlers().stream()
      .filter(QSHandler::isUnique)
      .map(QSHandler::getCode)
      .collect(toCollection(HashSet::new));

    this.unique = repository.findAllByUniqueAndHandlerCodeIn(true, handlerCodes)
      .stream()
      .map(converter::toQsBean)
      .collect(groupingBy(QSJsonData::getCode, toCollection(ArrayList::new)));

    LOG.info("QSHandlerCache has been refreshed and the Unique has been updated");
  }

  @PreDestroy
  @Scheduled(cron = "${qs.bot.json-data.schedule-time}")
  private void saveCacheInDB() {
    LOG.info("{} QSJsonData in cache before dropping cache", this.cache.size());
    Collection<Pair<LocalDateTime, QSJsonData>> cache = new ArrayList<>(this.cache.values());
    Collection<QSRequestDataEntity> entitiesToSave = new ArrayList<>();
    LocalDateTime timeToDelete = LocalDateTime.now().minusMinutes(this.lifetime);
    cache.forEach(
      pair -> {
        QSJsonData jsonData = pair.getSecond();
        entitiesToSave.add(
          converter.toEntity(jsonData)
        );
        if (pair.getFirst().isBefore(timeToDelete)) {
          deleteFromCache(jsonData.getId());
        }
      }
    );
    repository.saveWithoutUpdate(entitiesToSave);
    LOG.info("{} QSJsonData in cache after dropping cache", this.cache.size());
  }

  private void deleteFromCache(UUID jsonDataId) {
    this.cache.remove(jsonDataId);
  }

  private QSJsonData addToCache(QSJsonData jsonData) {
    QSJsonData jsonDataWithId = QSJsonDataBean.builder()
      .unique(jsonData.isUnique())
      .data(jsonData.getData())
      .code(jsonData.getCode())
      .previousId(jsonData.getPreviousId())
      .userId(jsonData.getUserId())
      .id(Generators.timeBasedGenerator().generate())
      .build();
    this.cache.put(jsonDataWithId.getId(), Pair.of(LocalDateTime.now(), jsonDataWithId));
    return jsonDataWithId;
  }

}

