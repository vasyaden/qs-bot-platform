package qs.platform.dataaccess.fs;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.mock.env.MockEnvironment;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;

class QSFSFileStorageFactoryTest {
  private final static String title = "some storage description";
  private final static String path = "/some_folder";
  private final static String type = "fs";
  @Mock
  QSContentTypeService contentTypeService;

  @Test
  void shouldGetStorages() {
    MockEnvironment env = new MockEnvironment();
    QSFSFileStorageFactory factory;
    env.setProperty("qs.storage.someStorageName.title", title);
    env.setProperty("qs.storage.someStorageName.type", type);
    env.setProperty("qs.storage.someStorageName.path", path);

    factory = new QSFSFileStorageFactory(env, contentTypeService);

    Collection<QSFSFileStorage> storages = factory.getStorages();
    QSFSFileStorage storage = storages.iterator().next();

    assertEquals(1, storages.size());
    assertEquals("someStorageName", storage.getCode());
    assertEquals(title, storage.getTitle());
  }
}