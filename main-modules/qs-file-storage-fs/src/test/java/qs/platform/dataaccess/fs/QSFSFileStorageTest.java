package qs.platform.dataaccess.fs;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import qs.platform.dataaccess.fs.services.QSContentTypeBaseService;
import qs.platform.exceptions.QSRuntimeException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.MonthDay;
import java.time.Year;

import static org.junit.jupiter.api.Assertions.*;

class QSFSFileStorageTest {
  private final static String rootPath = "src";
  private final static String path = "test/java/qs/platform/dataaccess/fs/resources";
  private final static String fileName = "file.txt";
  private final static String fileNameSource = "fileSource.txt";
  private final static long fileSourceLength = 9L;
  private final static String fileContentType = "text/plain";

  @Test
  void shouldReadSaveAndDelete() {
    QSContentTypeService contentTypeService = new QSContentTypeBaseService();
    QSFSFileStorage fileStorage = new QSFSFileStorage(
      "code",
      "title",
      rootPath,
      contentTypeService);

    fileStorage.save(path, fileName, fileStorage.read(path, fileNameSource));
    QSStorageFileInfo infoBean = fileStorage.getInfo(path, fileName);

    assertEquals(fileName, infoBean.getName());
    assertEquals(fileSourceLength, infoBean.getLength());
    assertEquals(fileContentType, infoBean.getContentType());
    assertEquals(path, infoBean.getPath());

    Path filePath = Paths.get(rootPath, path, fileName);
    assertTrue(Files.exists(filePath));
    fileStorage.delete(path, fileName);
    assertFalse(Files.exists(filePath));
  }

  @Test
  void shouldSaveIfPathIsEmpty() {
    QSContentTypeService contentTypeService = new QSContentTypeBaseService();
    QSFSFileStorage fileStorage = new QSFSFileStorage(
      "code",
      "title",
      rootPath,
      contentTypeService);

    fileStorage.save(fileName, fileStorage.read(path, fileNameSource));

    MonthDay md = MonthDay.now();
    String newPathString = Year.now().getValue() + File.separator + md.getMonthValue() + File.separator + md.getDayOfMonth();
    Path newPath = Paths.get(rootPath, newPathString, fileName);
    assertTrue(Files.exists(newPath));
    try {
      FileUtils.deleteDirectory(new File(rootPath, String.valueOf(Year.now().getValue())));
    } catch (IOException e) {
      throw new QSRuntimeException(e);
    }
  }

}