package qs.platform.dataaccess.fs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.env.PropertySource;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class QSFSFileStorageFactory implements QSFileStorageFactory<QSFSFileStorage> {
  private final Environment env;
  private final QSContentTypeService contentTypeService;
  private final static String TYPE_FS = "fs";
  private final static String TYPE = "type";
  private final static String TITLE = "title";
  private final static String PATH = "path";
  private final static String QS_STORAGE = "qs.storage";

  @Autowired
  public QSFSFileStorageFactory(Environment env, QSContentTypeService contentTypeService) {
    this.env = env;
    this.contentTypeService = contentTypeService;
  }

  @Override
  public Collection<QSFSFileStorage> getStorages() {
    Map<String, String> allProperties = getAllProperties(env);
    Map<String, String> rawStorageProperties = getRawStorageProperties(allProperties);
    Collection<String> keysWithTitle = getKeysWithTitle(rawStorageProperties);
    Collection<String> codes = getCodes(keysWithTitle);
    Map<String, Map<String, String>> storageProperties = getStorageProperties(codes, rawStorageProperties);

    Collection<QSFSFileStorage> storages = new ArrayList<>();
    for (String code : storageProperties.keySet()) {
      QSFSFileStorage storage = new QSFSFileStorage(
        code,
        storageProperties.get(code).get(TITLE),
        storageProperties.get(code).get(PATH),
        contentTypeService);
      storages.add(storage);
    }
    return storages;
  }

  private Map<String, String> getAllProperties(Environment env) {
    Map<String, String> allProperties = new HashMap<>();
    if (env instanceof ConfigurableEnvironment) {
      for (PropertySource<?> propertySource : ((ConfigurableEnvironment) env).getPropertySources()) {
        if (propertySource instanceof EnumerablePropertySource<?>) {
          for (String key : ((EnumerablePropertySource<?>) propertySource).getPropertyNames()) {
            allProperties.put(key, Objects.requireNonNull(env.getProperty(key)));
          }
        }
      }
    }
    return allProperties;
  }

  private Map<String, String> getRawStorageProperties(Map<String, String> allProperties) {
    Map<String, String> storageProperties = new HashMap<>();
    for (String key : allProperties.keySet()) {
      if (key.contains(QS_STORAGE)) {
        storageProperties.put(key, allProperties.get(key));
      }
    }
    return storageProperties;
  }

  private Collection<String> getKeysWithTitle(Map<String, String> storageProperties) {
    Collection<String> keysWithTitle = new ArrayList<>();
    for (String key : storageProperties.keySet()) {
      if (key.endsWith(TITLE)) {
        keysWithTitle.add(key);
      }
    }
    return keysWithTitle;
  }

  private Collection<String> getCodes(Collection<String> keysWithTitle) {
    Collection<String> codes = new ArrayList<>();
    for (String key : keysWithTitle) {
      String[] splitKey = key.split("\\.");
      codes.add(splitKey[2]);
    }
    return codes;
  }

  private Map<String, Map<String, String>> getStorageProperties(Collection<String> codes, Map<String, String> rawStorageProperties) {
    Map<String, Map<String, String>> storageProperties = new HashMap<>();
    for (String code : codes) {
      Map<String, String> properties = new HashMap<>();
      properties.put(TYPE, rawStorageProperties.get(QS_STORAGE + "." + code + "." + TYPE));
      if (!TYPE_FS.equals(properties.get(TYPE))) {
        continue;
      }
      properties.put(PATH, rawStorageProperties.get(QS_STORAGE + "." + code + "." + PATH));
      properties.put(TITLE, rawStorageProperties.get(QS_STORAGE + "." + code + "." + TITLE));
      storageProperties.put(code, properties);
    }
    return storageProperties;
  }

}