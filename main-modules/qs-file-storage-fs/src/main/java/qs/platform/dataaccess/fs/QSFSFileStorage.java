package qs.platform.dataaccess.fs;


import org.springframework.http.HttpStatus;
import qs.platform.dataaccess.fs.beans.QSStorageFileInfoBean;
import qs.platform.exceptions.QSNotFoundException;
import qs.platform.exceptions.QSRuntimeException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.MonthDay;
import java.time.Year;
import java.time.ZoneId;
import java.util.StringJoiner;

public class QSFSFileStorage implements QSFileStorage {

  private final String code;
  private final String title;
  private final String rootPath;
  private final QSContentTypeService contentTypeService;

  public QSFSFileStorage(String code, String title, String rootPath, QSContentTypeService contentTypeService) {
    this.code = code;
    this.title = title;
    this.rootPath = rootPath;
    this.contentTypeService = contentTypeService;
  }

  @Override
  public String getCode() {
    return code;
  }

  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public QSStorageFileInfo getInfo(String path, String fileName) {
    Path filePath = getFilePathOrThrow(path, fileName);
    String contentType = contentTypeService.getContentType(fileName);
    LocalDateTime createdTime = getCreatedTime(filePath);
    long length = getFileLength(filePath);

    return QSStorageFileInfoBean.builder()
      .name(fileName)
      .path(path)
      .contentType(contentType)
      .length(length)
      .createdTime(createdTime)
      .build();
  }

  @Override
  public QSStorageFileInfo save(String fileName, InputStream stream) {
    MonthDay md = MonthDay.now();
    StringJoiner path = new StringJoiner(File.separator);
    path.add(String.valueOf(Year.now().getValue()));
    path.add(String.valueOf(md.getMonthValue()));
    path.add(String.valueOf(md.getDayOfMonth()));
    return save(path.toString(), fileName, stream);
  }

  @Override
  public QSStorageFileInfo save(String path, String fileName, InputStream stream) {
    Path filePath = Paths.get(rootPath, path, fileName);
    try {
      Files.createDirectories(filePath.getParent());
      Files.copy(stream, filePath, StandardCopyOption.REPLACE_EXISTING);
    } catch (IOException e) {
      throw new QSRuntimeException(e);
    }
    return getInfo(path, fileName);
  }

  @Override
  public InputStream read(String path, String fileName) {
    Path filePath = getFilePathOrThrow(path, fileName);
    try {
      return Files.newInputStream(filePath);
    } catch (IOException e) {
      throw new QSRuntimeException(e);
    }
  }

  @Override
  public void delete(String path, String fileName) {
    Path filePath = Paths.get(rootPath, path, fileName);
    try {
      Files.delete(filePath);
    } catch (IOException e) {
      throw new QSRuntimeException(e);
    }
  }

  /**
   * get created or last modified time
   *
   * @param filePath absolute path to the file
   */
  private LocalDateTime getCreatedTime(Path filePath) {
    try {
      return Files.getLastModifiedTime(filePath)
        .toInstant()
        .atZone(ZoneId.systemDefault())
        .toLocalDateTime();
    } catch (IOException e) {
      throw new QSRuntimeException(e);
    }
  }

  /**
   * get file length
   *
   * @param filePath absolute path to the file
   */
  private long getFileLength(Path filePath) {
    try {
      return Files.size(filePath);
    } catch (IOException e) {
      throw new QSRuntimeException(e);
    }
  }

  /**
   * get file Path with absolute path to the file, if file does not exist an exception is thrown
   *
   * @param path     relative path to the file
   * @param fileName name of the file
   */
  private Path getFilePathOrThrow(String path, String fileName) {
    Path filePath = Paths.get(rootPath, path, fileName);
    if (Files.notExists(filePath)) {
      throw new QSNotFoundException(HttpStatus.NOT_FOUND.toString(), String.format("file %s does not exist", filePath));
    }
    return filePath;
  }

}
