package qs.platform.botapi.dbaccess.converters;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qs.platform.botapi.QSJsonData;
import qs.platform.botapi.beans.QSJsonDataBean;
import qs.platform.botapi.dbaccess.entities.QSRequestDataEntity;
import qs.platform.botapi.json.beans.JsonData;
import qs.platform.exceptions.QSRuntimeException;
import qs.platform.json.QSJsonMapper;

@Component
public class QSRequestDataConverter {

  private QSJsonMapper jsonMapper;

  @Autowired
  private void setJsonMapper(QSJsonMapper jsonMapper) {
    this.jsonMapper = jsonMapper;
  }

  public QSRequestDataEntity toEntity(QSJsonData immutable) {
    try {
      JsonData jsonData = JsonData.builder()
        .code(immutable.getCode())
        .data(immutable.getData())
        .build();
      return QSRequestDataEntity.builder()
        .id(immutable.getId())
        .userId(immutable.getUserId())
        .handlerCode(immutable.getCode())
        .data(jsonMapper.getJsonMapper().writeValueAsString(jsonData))
        .previousId(immutable.getPreviousId())
        .unique(immutable.isUnique())
        .build();
    } catch (JsonProcessingException e) {
      throw new QSRuntimeException("cant write json data as string", e);
    }
  }

  public QSJsonData toQsBean(QSRequestDataEntity entity) {
    return toQsBean(entity, entity.getUserId());
  }

  public QSJsonData toQsBean(QSRequestDataEntity entity, Long userId) {
    try {
      JsonData jsonData = jsonMapper.getJsonMapper().readValue(entity.getData(), JsonData.class);
      return QSJsonDataBean.builder()
        .id(entity.getId())
        .userId(userId)
        .code(entity.getHandlerCode())
        .data(jsonData.getData())
        .previousId(entity.getPreviousId())
        .unique(entity.isUnique())
        .build();
    } catch (JsonProcessingException e) {
      throw new QSRuntimeException("cant read json data", e);
    }
  }
}
