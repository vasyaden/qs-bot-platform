package qs.platform.botapi.dbaccess.entities;

import jakarta.persistence.*;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.util.UUID;

@Entity
@Table(name = "request_history")
public class QSRequestDataEntity {

  @Id
  private UUID id;

  @Column(name = "user_id")
  private Long userId;

  @Column(name = "handler_code")
  private String handlerCode;

  @JdbcTypeCode(SqlTypes.JSON)
  @Column(name = "json_data")
  private String data;

  @Column(name = "previous")
  private UUID previousId;

  @Column(name = "unique_request")
  private boolean unique;

  public QSRequestDataEntity() {
  }

  public static Builder builder() {
    return new Builder();
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getHandlerCode() {
    return handlerCode;
  }

  public void setHandlerCode(String handlerCode) {
    this.handlerCode = handlerCode;
  }

  public String getData() {
    return data;
  }

  public void setData(String data) {
    this.data = data;
  }

  public UUID getPreviousId() {
    return previousId;
  }

  public void setPreviousId(UUID previousId) {
    this.previousId = previousId;
  }

  public boolean isUnique() {
    return unique;
  }

  public void setUnique(boolean unique) {
    this.unique = unique;
  }

  public static final class Builder {
    private UUID id;
    private Long userId;
    private String handlerCode;
    private String data;
    private UUID previousId;
    private boolean unique;

    private Builder() {
    }

    public Builder id(UUID id) {
      this.id = id;
      return this;
    }

    public Builder userId(Long userId) {
      this.userId = userId;
      return this;
    }

    public Builder handlerCode(String handlerCode) {
      this.handlerCode = handlerCode;
      return this;
    }

    public Builder data(String data) {
      this.data = data;
      return this;
    }

    public Builder previousId(UUID previousId) {
      this.previousId = previousId;
      return this;
    }

    public Builder unique(boolean unique) {
      this.unique = unique;
      return this;
    }

    public QSRequestDataEntity build() {
      QSRequestDataEntity requestData = new QSRequestDataEntity();
      requestData.setId(id);
      requestData.setUserId(userId);
      requestData.setHandlerCode(handlerCode);
      requestData.setData(data);
      requestData.setPreviousId(previousId);
      requestData.setUnique(unique);
      return requestData;
    }
  }
}
