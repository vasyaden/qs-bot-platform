package qs.platform.botapi.dbaccess.repositories.jdbc;

import qs.platform.botapi.dbaccess.entities.QSRequestDataEntity;

import java.util.Collection;

/**
 * SQL repository for {@link QSRequestDataEntity entity}
 */
public interface QSRequestDataSqlRepository {
  /**
   * save list of entities, if there is the same entity in DB, entity won't save
   *
   * @param entities list of {@link QSRequestDataEntity entity}
   */
  void saveWithoutUpdate(Collection<QSRequestDataEntity> entities);
}
