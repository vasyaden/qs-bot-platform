package qs.platform.botapi.dbaccess.repositories.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import qs.platform.botapi.dbaccess.entities.QSRequestDataEntity;

import java.util.Collection;

@Repository
public class QSRequestDataSqlRepositoryImpl implements QSRequestDataSqlRepository {
  private final JdbcTemplate jdbcTemplate;
  private static final String ADD_ALL_REQUEST =
    "INSERT INTO request_history (id, user_id, handler_code, json_data, previous, unique_request) " +
      "VALUES(?, ?, ?, ?::jsonb, ?, ?) on conflict do nothing";

  @Autowired
  public QSRequestDataSqlRepositoryImpl(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  @Override
  public void saveWithoutUpdate(Collection<QSRequestDataEntity> entities) {

    jdbcTemplate.batchUpdate(ADD_ALL_REQUEST,
      entities,
      entities.size(),
      (ps, entity) -> {
        ps.setObject(1, entity.getId(), java.sql.Types.OTHER);
        ps.setLong(2, entity.getUserId());
        ps.setString(3, entity.getHandlerCode());
        ps.setObject(4, entity.getData());
        ps.setObject(5, entity.getPreviousId(), java.sql.Types.OTHER);
        ps.setBoolean(6, entity.isUnique());
      });
  }
}
