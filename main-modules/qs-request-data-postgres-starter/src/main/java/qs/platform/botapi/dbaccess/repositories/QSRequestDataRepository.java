package qs.platform.botapi.dbaccess.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import qs.platform.botapi.dbaccess.entities.QSRequestDataEntity;
import qs.platform.botapi.dbaccess.repositories.jdbc.QSRequestDataSqlRepository;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

public interface QSRequestDataRepository extends JpaRepository<QSRequestDataEntity, UUID>, QSRequestDataSqlRepository {

  Optional<QSRequestDataEntity> getByIdAndUserId(UUID id, Long userId);

  Collection<QSRequestDataEntity> findAllByUniqueAndHandlerCodeIn(boolean unique, Collection<String>codes);
}
