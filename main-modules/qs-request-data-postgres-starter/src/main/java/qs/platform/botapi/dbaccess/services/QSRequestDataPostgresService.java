package qs.platform.botapi.dbaccess.services;

import com.fasterxml.uuid.Generators;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
import org.springframework.stereotype.Service;
import qs.platform.botapi.QSJsonData;
import qs.platform.botapi.QSRequestDataService;
import qs.platform.botapi.dbaccess.converters.QSRequestDataConverter;
import qs.platform.botapi.dbaccess.entities.QSRequestDataEntity;
import qs.platform.botapi.dbaccess.repositories.QSRequestDataRepository;
import qs.platform.exceptions.QSRuntimeException;

import java.util.UUID;

/**
 * Default implementation of {@link QSRequestDataService} without caching.
 * Is not created when another implementation is presented in context.
 */
@Service
@ConditionalOnSingleCandidate(QSRequestDataService.class)
public class QSRequestDataPostgresService implements QSRequestDataService {

  private QSRequestDataRepository repository;
  private QSRequestDataConverter converter;

  @Autowired
  private void setRepository(QSRequestDataRepository repository) {
    this.repository = repository;
  }

  @Autowired
  private void setConverter(QSRequestDataConverter converter) {
    this.converter = converter;
  }

  @Override
  public QSJsonData save(QSJsonData jsonData) {
    QSRequestDataEntity requestData = converter.toEntity(jsonData);
    requestData.setId(Generators.timeBasedGenerator().generate());
    return converter.toQsBean(repository.save(requestData));
  }

  @Override
  public QSJsonData getJsonDataByIdAndUserId(UUID id, Long userId) {
    return repository
      .getByIdAndUserId(id, userId)
      .map(requestData -> {
          try {
            return converter.toQsBean(requestData);
          } catch (Exception e) {
            throw new QSRuntimeException("Can't read json data: " + e.getMessage(), e);
          }
        }
      )
      .orElseThrow(() -> new QSRuntimeException("Not found request data with id=" + id));
  }

}
