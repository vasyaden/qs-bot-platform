package qs.platform.dataaccess.fs;

import java.io.InputStream;

/**
 * Base files storage
 */
public interface QSFileStorage {
  /**
   * Code of the storage
   */
  String getCode();

  /**
   * Title of the storage
   */
  String getTitle();

  /**
   * Get {@link QSStorageFileInfo file info}
   *
   * @param path     relative path to the file in the storage
   * @param fileName name of the file in the storage
   * @return {@link QSStorageFileInfo file info}
   */
  QSStorageFileInfo getInfo(String path, String fileName);

  /**
   * Save passed stream to the file
   *
   * @param fileName name of the file in the storage
   * @param stream   data
   * @return {@link QSStorageFileInfo file info}
   */
  QSStorageFileInfo save(String fileName, InputStream stream);

  /**
   * Save passed stream to the file
   *
   * @param path     relative path to the file in the storage
   * @param fileName name of the file in the storage
   * @param stream   data
   */
  QSStorageFileInfo save(String path, String fileName, InputStream stream);

  /**
   * Read file data
   *
   * @param path     relative path to the file in the storage
   * @param fileName name of the file in the storage
   * @return {@link InputStream data}
   */
  InputStream read(String path, String fileName);

  /**
   * Delete file
   *
   * @param path     relative path to the file in the storage
   * @param fileName name of the file in the storage
   */
  void delete(String path, String fileName);
}
