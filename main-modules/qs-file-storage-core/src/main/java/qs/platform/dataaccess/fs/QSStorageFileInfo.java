package qs.platform.dataaccess.fs;

import java.time.LocalDateTime;

/**
 * Info about file in the storage
 */
public interface QSStorageFileInfo {
  /**
   * Relative path to the file in the storage
   */
  String getPath();

  /**
   * Name of the file in the storage
   */
  String getName();

  /**
   * File creation time
   */
  LocalDateTime getCreatedTime();

  /**
   * File length
   */
  long getLength();

  /**
   * File content type
   */
  String getContentType();

}
