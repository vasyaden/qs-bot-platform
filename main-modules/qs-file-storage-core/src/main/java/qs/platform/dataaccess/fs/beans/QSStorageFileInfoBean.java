package qs.platform.dataaccess.fs.beans;

import qs.platform.dataaccess.fs.QSStorageFileInfo;

import java.time.LocalDateTime;

public class QSStorageFileInfoBean implements QSStorageFileInfo {
  private final String path;
  private final String name;
  private final LocalDateTime createdTime;
  private final long length;
  private final String contentType;

  private QSStorageFileInfoBean(String path, String name, LocalDateTime createdTime, long length, String contentType) {
    this.path = path;
    this.name = name;
    this.createdTime = createdTime;
    this.length = length;
    this.contentType = contentType;
  }

  @Override
  public String getPath() {
    return path;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public LocalDateTime getCreatedTime() {
    return createdTime;
  }

  @Override
  public long getLength() {
    return length;
  }

  @Override
  public String getContentType() {
    return contentType;
  }

  public static Builder builder() {
    return new Builder();
  }

  public static final class Builder {
    private String path;
    private String name;
    private LocalDateTime createdTime;
    private long length;
    private String contentType;

    private Builder() {
    }

    public Builder path(String path) {
      this.path = path;
      return this;
    }

    public Builder name(String name) {
      this.name = name;
      return this;
    }

    public Builder createdTime(LocalDateTime createdTime) {
      this.createdTime = createdTime;
      return this;
    }

    public Builder length(long length) {
      this.length = length;
      return this;
    }

    public Builder contentType(String contentType) {
      this.contentType = contentType;
      return this;
    }

    public QSStorageFileInfoBean build() {
      return new QSStorageFileInfoBean(path, name, createdTime, length, contentType);
    }
  }
}

