package qs.platform.dataaccess.fs;

/**
 * Service determines the content type of the file
 */
public interface QSContentTypeService {

  /**
   * get name of file content type
   *
   * @param fileName name of the file
   */
  String getContentType(String fileName);
}
