package qs.platform.dataaccess.fs.services;

import org.apache.tika.Tika;
import org.springframework.stereotype.Service;
import qs.platform.dataaccess.fs.QSContentTypeService;

@Service
public class QSContentTypeBaseService implements QSContentTypeService {
  private final Tika tika = new Tika();

  public String getContentType(String fileName) {
    return tika.detect(fileName);
  }

}
