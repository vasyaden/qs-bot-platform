package qs.platform.dataaccess.fs.services;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import qs.platform.dataaccess.fs.QSFileStorage;
import qs.platform.dataaccess.fs.QSFileStorageFactory;
import qs.platform.dataaccess.fs.QSFileStorageService;
import qs.platform.dataaccess.fs.exceptions.QSFileStorageNotFoundException;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class QSFileStorageBaseService implements QSFileStorageService {

  private final Map<String, QSFileStorage> fileStorageMap;

  @Autowired(required = false)
  private QSFileStorageBaseService(Collection<QSFileStorageFactory<? extends QSFileStorage>> fileStorageFactories) {
    fileStorageMap = CollectionUtils.isEmpty(fileStorageFactories) ?
      Collections.emptyMap() : fileStorageFactories.stream()
      .map(QSFileStorageFactory::getStorages)
      .flatMap(Collection::stream)
      .collect(Collectors.toMap(QSFileStorage::getCode, Function.identity()));
  }

  @Override
  public QSFileStorage getStorage(String code) {
    return fileStorageMap.get(code);
  }

  @Override
  public QSFileStorage getStorageOrThrow(String code) {
    QSFileStorage fileStorage = fileStorageMap.get(code);
    if (fileStorage == null) {
      throw new QSFileStorageNotFoundException(code);
    }
    return fileStorage;
  }
}
