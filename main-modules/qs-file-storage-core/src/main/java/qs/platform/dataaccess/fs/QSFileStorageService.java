package qs.platform.dataaccess.fs;

/**
 * Service of the files' storages
 */
public interface QSFileStorageService {
  /**
   * Get file storage by code
   *
   * @param code code of the storage
   * @return {@link QSFileStorage}
   */
  QSFileStorage getStorage(String code);

  /**
   * Get file storage by code or throw exception if it is not found
   *
   * @param code code of the storage
   * @return {@link QSFileStorage}
   */
  QSFileStorage getStorageOrThrow(String code);

}
