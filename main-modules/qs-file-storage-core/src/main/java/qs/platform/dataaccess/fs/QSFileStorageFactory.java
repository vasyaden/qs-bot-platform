package qs.platform.dataaccess.fs;

import java.util.Collection;

/**
 * Factory of the files' storages
 *
 * @param <S> type of the files' storage
 */
public interface QSFileStorageFactory<S extends QSFileStorage> {
  /**
   * Returns storages
   */
  Collection<S> getStorages();

}
