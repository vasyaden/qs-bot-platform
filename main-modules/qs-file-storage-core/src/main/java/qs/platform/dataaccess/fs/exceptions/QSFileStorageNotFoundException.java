package qs.platform.dataaccess.fs.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import qs.platform.dataaccess.fs.QSFileStorage;
import qs.platform.exceptions.QSRuntimeException;

/**
 * Exception to be thrown when {@link  QSFileStorage} was not found
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class QSFileStorageNotFoundException extends QSRuntimeException {

  private static final String EXCEPTION_CODE = "qs.fileStorage.notFound";

  public QSFileStorageNotFoundException(String code) {
    super(EXCEPTION_CODE, "Not found file storage with code='" + code + '\'');
  }
}
