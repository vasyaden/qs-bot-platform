# Platform main modules

| Module                                  | Description                                              |
|-----------------------------------------|----------------------------------------------------------|
| qs-botapi-core                          | Common bot API and interfaces                            |
| qs-botapi-settings-client               | Client of bot settings                                   |
| qs-botapi-settings-editor-starter       | REST API for bot settings editing                        |
| qs-common-core                          | Common API, interfaces and exceptions                    |
| qs-file-storage-core                    | Access to file storage common API                        |
| qs-file-storage-fs                      | File system storage implementation                       |
| qs-request-data-core                    | Core module of bot-platform requests data                |
| qs-request-data-postgres-cached-starter | Cached extension of the JPA requests data implementation |
| qs-request-data-postgres-starter        | Simple JPA implementation of the storing requests data   |
| qs-telegram-botapi-starter              | Main starter of the bot-platform                         |
