package qs.platform.botapi.beans;

import qs.platform.botapi.QSJsonData;
import qs.platform.exceptions.QSRuntimeException;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class QSJsonDataBean implements QSJsonData {

  private final UUID id;

  private final Long userId;

  private final String code;

  private final Map<String, Object> data;

  private final UUID previousId;
  private final boolean unique;

  private QSJsonDataBean(UUID id, Long userId, String code, Map<String, Object> data, UUID previousId, boolean unique) {
    this.id = id;
    this.userId = userId;
    this.code = code;
    this.data = data == null ? Collections.emptyMap() : Collections.unmodifiableMap(new HashMap<>(data));
    this.previousId = previousId;
    this.unique = unique;
  }

  public static Builder builder() {
    return new Builder();
  }

  @Override
  public UUID getId() {
    return id;
  }

  @Override
  public Long getUserId() {
    return userId;
  }

  @Override
  public String getCode() {
    return code;
  }

  @Override
  public Map<String, Object> getData() {
    return data;
  }

  @Override
  public Map<String, Object> getMutableData() {
    return new HashMap<>(data);
  }

  @Override
  public Object getParam(String code) {
    return data.get(code);
  }

  @Override
  public Object getParamOrThrow(String code) {
    Object param = getParam(code);
    if (param == null) {
      throw new QSRuntimeException("Not found param with code '" + code + '\'');
    }
    return param;
  }

  @Override
  public UUID getPreviousId() {
    return previousId;
  }

  @Override
  public boolean isUnique() {
    return unique;
  }

  public static final class Builder {
    private UUID id;
    private Long userId;
    private String code;
    private Map<String, Object> data = new HashMap<>();
    private UUID previousId;
    private boolean unique = false;

    private Builder() {
    }

    public Builder id(UUID id) {
      this.id = id;
      return this;
    }

    public Builder userId(Long userId) {
      this.userId = userId;
      return this;
    }

    public Builder code(String code) {
      this.code = code;
      return this;
    }

    public Builder data(Map<String, Object> data) {
      if (data != null) {
        this.data.putAll(data);
      }
      return this;
    }

    public Builder addParam(String code, Object value) {
      this.data.put(code, value);
      return this;
    }

    public Builder previousId(UUID previousId) {
      this.previousId = previousId;
      return this;
    }

    public Builder unique(boolean unique) {
      this.unique = unique;
      return this;
    }

    public QSJsonData build() {
      return new QSJsonDataBean(id, userId, code, data, previousId, unique);
    }
  }
}
