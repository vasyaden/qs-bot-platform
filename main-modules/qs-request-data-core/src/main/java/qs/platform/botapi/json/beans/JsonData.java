package qs.platform.botapi.json.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonData {

  private String code;

  private Map<String, Object> data;

  public JsonData() {
  }

  private JsonData(String code, Map<String, Object> data) {
    this.code = code;
    this.data = data;
  }

  public static Builder builder() {
    return new Builder();
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Map<String, Object> getData() {
    return data;
  }

  public void setData(Map<String, Object> data) {
    this.data = data;
  }

  public static final class Builder {
    private String code;

    private Map<String, Object> data;

    private Builder() {
    }

    public Builder code(String code) {
      this.code = code;
      return this;
    }

    public Builder data(Map<String, Object> data) {
      this.data = data;
      return this;
    }

    public JsonData build() {
      return new JsonData(code, data);
    }
  }
}
