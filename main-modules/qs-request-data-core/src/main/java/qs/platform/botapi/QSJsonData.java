package qs.platform.botapi;

import java.util.Map;
import java.util.UUID;

/**
 * Request data
 */
public interface QSJsonData {
  /**
   * ID
   */
  UUID getId();

  /**
   * User Telegram ID
   */
  Long getUserId();

  /**
   * Handler code
   */
  String getCode();

  /**
   * Immutable params of the request
   */
  Map<String, Object> getData();

  /**
   * Mutable params of the request
   */
  Map<String, Object> getMutableData();

  /**
   * ID of the previous request
   */
  UUID getPreviousId();

  /**
   * Get param by code
   *
   * @param code code of the requested param
   * @return nullable param value
   */
  Object getParam(String code);

  /**
   * Get param by code or throw exception, if it's null
   *
   * @param code code of the requested param
   * @return param value
   */
  Object getParamOrThrow(String code);

  /**
   * Unique request
   */
  boolean isUnique();
}
