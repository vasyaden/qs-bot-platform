package qs.platform.botapi;

import java.util.UUID;

/**
 * Service for {@link QSJsonData}
 */
public interface QSRequestDataService {
  /**
   * Saving {@link QSJsonData request data}
   *
   * @param jsonData request data
   * @return saved {@link QSJsonData request data}
   */
  QSJsonData save(QSJsonData jsonData);

  /**
   * Get {@link QSJsonData request data} by {@link QSJsonData#getId() ID} and
   * {@link QSJsonData#getUserId() user telegram ID}
   *
   * @param id     {@link QSJsonData#getId() request data ID}
   * @param userId {@link QSJsonData#getUserId() user telegram ID}
   * @return found {@link QSJsonData request data}
   */
  QSJsonData getJsonDataByIdAndUserId(UUID id, Long userId);
}
