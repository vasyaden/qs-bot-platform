package qs.platform.settings;

import qs.platform.botapi.QSHandlerSettings;

import java.util.Collection;

/**
 * Hierarchical tree of {@link QSHandlerSettingsTree handlers' settings}
 */
public interface QSHandlerSettingsTree extends QSHandlerSettings {


  /**
   * {@link QSHandlerSettingsTree Child handlers settings}
   */
  Collection<QSHandlerSettingsTree> getChildren();

}
