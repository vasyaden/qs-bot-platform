package qs.platform.settings;

import qs.platform.botapi.QSHandlerSettings;

import java.util.Collection;

/**
 * Service provided <b>actual</b> {@link QSHandlerSettings handlers' settings} API
 */
public interface QSHandlersSettingsService {
  /**
   * @return collection of actual {@link QSHandlerSettings handlers' settings}
   */
  Collection<QSHandlerSettings> getAll();

  /**
   * Returns {@link QSHandlerSettings handler's settings} by {@link QSHandlerSettings#getCode()  code}
   *
   * @param code code of {@link QSHandlerSettings handler}
   * @return {@link QSHandlerSettings handler's settings}
   */
  QSHandlerSettings getByCode(String code);

  /**
   * Returns actual handlers tree
   *
   * @return tree of actual {@link QSHandlerSettingsTree handlers}
   */
  Collection<QSHandlerSettingsTree> getTree();

  /**
   * Saves collection of {@link QSHandlerSettings handlers' settings}
   *
   * @param settings {@link QSHandlerSettings handlers' settings} to save
   * @return saved {@link QSHandlerSettings handlers' settings}
   */
  Collection<QSHandlerSettings> saveAll(Collection<? extends QSHandlerSettings> settings);

  /**
   * Deletes all {@link QSHandlerSettings handlers' settings}
   */
  void deleteAll();
}
