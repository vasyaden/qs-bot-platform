package qs.platform.settings.converters;

import org.springframework.stereotype.Service;
import qs.platform.botapi.QSHandlerSettings;
import qs.platform.settings.beans.QSHandlerSettingsBean;
import qs.platform.settings.beans.QSHandlerSettingsEntity;

@Service
public class QSHandlersSettingsBaseConverter implements QSEntityBeanConverter<QSHandlerSettingsEntity, QSHandlerSettings> {
  @Override
  public QSHandlerSettings toBean(QSHandlerSettingsEntity entity) {
    return QSHandlerSettingsBean.builder()
      .code(entity.getCode())
      .title(entity.getTitle())
      .parentCode(entity.getParentCode())
      .description(entity.getDescription())
      .filters(entity.getFilters())
      .dataQuery(entity.getDataQuery())
      .unique(entity.isUnique())
      .scenarioCode(entity.getScenarioCode())
      .scenarioParams(entity.getScenarioParams())
      .creationDate(entity.getCreationDate())
      .order(entity.getOrder())
      .build();
  }

  @Override
  public QSHandlerSettingsEntity toEntity(QSHandlerSettings bean) {
    return QSHandlerSettingsEntity.builder()
      .code(bean.getCode())
      .title(bean.getTitle())
      .parentCode(bean.getParentCode())
      .description(bean.getDescription())
      .filters(bean.getFilters())
      .dataQuery(bean.getDataQuery())
      .unique(bean.isUnique())
      .scenarioCode(bean.getScenarioCode())
      .scenarioParams(bean.getScenarioParams())
      .order(bean.getOrder())
      .creationDate(bean.getCreationDate())
      .build();
  }
}
