package qs.platform.settings.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import qs.platform.botapi.QSHandlerSettings;
import qs.platform.exceptions.QSNotFoundException;
import qs.platform.settings.QSHandlerSettingsTree;
import qs.platform.settings.QSHandlersSettingsService;
import qs.platform.settings.beans.QSHandlerSettingsEntity;
import qs.platform.settings.beans.QSHandlerSettingsTreeBean;
import qs.platform.settings.converters.QSEntityBeanConverter;
import qs.platform.settings.repositories.QSHandlersSettingsRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
public class QSHandlersSettingsBaseService implements QSHandlersSettingsService {

  private QSHandlersSettingsRepository repository;
  private QSEntityBeanConverter<QSHandlerSettingsEntity, QSHandlerSettings> converter;

  @Autowired
  private void setRepository(QSHandlersSettingsRepository repository) {
    this.repository = repository;
  }

  @Autowired
  private void setConverter(QSEntityBeanConverter<QSHandlerSettingsEntity, QSHandlerSettings> converter) {
    this.converter = converter;
  }

  @Override
  public Collection<QSHandlerSettings> getAll() {
    return repository.findAll().stream().map(converter::toBean).toList();
  }

  @Override
  public QSHandlerSettings getByCode(String code) {
    return repository.findByCode(code).map(converter::toBean)
      .orElseThrow(() -> new QSNotFoundException("handlersSettings.notFound"
        , "HandlerSettings with code = " + code + " not found"));
  }

  @Override
  public Collection<QSHandlerSettings> saveAll(Collection<? extends QSHandlerSettings> settings) {
    return repository.saveAll(settings.stream().map(converter::toEntity).toList()).stream().map(converter::toBean).toList();
  }

  @Override
  public void deleteAll() {
    repository.deleteAll();
  }

  @Override
  public Collection<QSHandlerSettingsTree> getTree() {
    Map<String, QSHandlerSettingsTreeBean.Builder> initialCollection = getAll().stream()
      .map(QSHandlerSettingsTreeBean::builder)
      .collect(Collectors.toMap(QSHandlerSettingsTreeBean.Builder::getCode, Function.identity()));
    Collection<QSHandlerSettingsTreeBean.Builder> finalCollection = new ArrayList<>();
    Collection<String> included = new HashSet<>();
    for (QSHandlerSettingsTreeBean.Builder current : initialCollection.values()) {
      String parentCode = current.getParentCode();
      QSHandlerSettingsTreeBean.Builder parent = initialCollection.get(parentCode);

      if (parent == null) {
        finalCollection.add(current);
      } else {
        parent.addChild(current);
        included.add(current.getCode());
      }

      QSHandlerSettingsTreeBean.Builder child = initialCollection.get(current.getDataQuery());
      if (child != null) {
        current.addChild(child);
        included.add(child.getCode());
      }

    }
    return finalCollection.stream()
      .filter(x -> !included.contains(x.getCode()))
      .map(QSHandlerSettingsTreeBean.Builder::build)
      .collect(Collectors.toList());
  }
}
