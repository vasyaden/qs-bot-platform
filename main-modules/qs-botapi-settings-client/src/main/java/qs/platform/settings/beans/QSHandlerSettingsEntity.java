package qs.platform.settings.beans;

import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;
import jakarta.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Map;

@Entity
@Table(name = "qs_handlers_settings")
public class QSHandlerSettingsEntity {
  @Id
  @Column(name = "code")
  private String code;
  @Column(name = "title")
  private String title;
  @Column(name = "parent_code")
  private String parentCode;
  @Column(name = "description")
  private String description;
  @JdbcTypeCode(SqlTypes.JSON)
  @Column(name = "filters")
  private Collection<String> filters;
  @Column(name = "data_query")
  private String dataQuery;
  @Column(name = "unique_handler")
  private boolean unique;
  @Column(name = "scenario_code")
  private String scenarioCode;
  @JdbcTypeCode(SqlTypes.JSON)
  @Column(name = "scenario_params")
  private Map<String, Object> scenarioParams;
  @Column(name = "menu_order")
  private Integer order;

  @Column(name = "creation_date", columnDefinition = "timestamptz DEFAULT now()", updatable = false)
  private LocalDateTime creationDate;


  public QSHandlerSettingsEntity() {
  }

  public static Builder builder() {
    return new Builder();
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }


  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getParentCode() {
    return parentCode;
  }

  public void setParentCode(String parentCode) {
    this.parentCode = parentCode;
  }

  public Integer getOrder() {
    return order;
  }

  public void setOrder(Integer order) {
    this.order = order;
  }

  public boolean isUnique() {
    return unique;
  }

  public void setUnique(boolean unique) {
    this.unique = unique;
  }

  public void setFilters(Collection<String> filters) {
    this.filters = filters;
  }

  public Collection<String> getFilters() {
    return filters;
  }

  public String getDataQuery() {
    return dataQuery;
  }

  public void setDataQuery(String dataQuery) {
    this.dataQuery = dataQuery;
  }

  public String getScenarioCode() {
    return scenarioCode;
  }

  public void setScenarioCode(String scenarioCode) {
    this.scenarioCode = scenarioCode;
  }

  public Map<String, Object> getScenarioParams() {
    return scenarioParams;
  }

  public void setScenarioParams(Map<String, Object> scenarioParams) {
    this.scenarioParams = scenarioParams;
  }

  public LocalDateTime getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(LocalDateTime creationDate) {
    this.creationDate = creationDate;
  }

  public static final class Builder {
    private String code;
    private String title;
    private String description;
    private String parentCode;
    private Integer order;
    private Collection<String> filters;
    private boolean unique;
    private String dataQuery;
    private String scenarioCode;
    private Map<String, Object> scenarioParams;
    private LocalDateTime creationDate;

    private Builder() {
    }

    public Builder code(String code) {
      this.code = code;
      return this;
    }

    public Builder title(String title) {
      this.title = title;
      return this;
    }

    public Builder description(String description) {
      this.description = description;
      return this;
    }

    public Builder parentCode(String parentCode) {
      this.parentCode = parentCode;
      return this;
    }

    public Builder order(Integer order) {
      this.order = order;
      return this;
    }

    public Builder filters(Collection<String> filters) {
      this.filters = filters;
      return this;
    }

    public Builder unique(boolean unique) {
      this.unique = unique;
      return this;
    }


    public Builder dataQuery(String dataQuery) {
      this.dataQuery = dataQuery;
      return this;
    }

    public Builder scenarioCode(String scenarioCode) {
      this.scenarioCode = scenarioCode;
      return this;
    }

    public Builder scenarioParams(Map<String, Object> scenarioParams) {
      this.scenarioParams = scenarioParams;
      return this;
    }

    public Builder creationDate(LocalDateTime creationDate) {
      this.creationDate = creationDate;
      return this;
    }

    public QSHandlerSettingsEntity build() {
      QSHandlerSettingsEntity entity = new QSHandlerSettingsEntity();
      entity.setCode(code);
      entity.setTitle(title);
      entity.setDescription(description);
      entity.setParentCode(parentCode);
      entity.setOrder(order);
      entity.setFilters(filters);
      entity.setUnique(unique);
      entity.setDataQuery(dataQuery);
      entity.setScenarioCode(scenarioCode);
      entity.setScenarioParams(scenarioParams);
      entity.setCreationDate(creationDate);
      return entity;
    }
  }
}
