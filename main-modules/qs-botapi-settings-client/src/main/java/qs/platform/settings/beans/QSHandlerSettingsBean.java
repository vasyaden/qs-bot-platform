package qs.platform.settings.beans;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import qs.platform.botapi.QSHandlerSettings;

import java.time.LocalDateTime;
import java.util.*;

public class QSHandlerSettingsBean implements QSHandlerSettings {
  private final String code;
  private final String title;
  private final String parentCode;
  private final String description;
  private final Collection<String> filters;
  private final String dataQuery;
  private final boolean unique;
  private final String scenarioCode;
  private final Map<String, Object> scenarioParams;
  private final Integer order;
  private final LocalDateTime creationDate;

  private QSHandlerSettingsBean(String code, String title, String parentCode, String description,
                                Collection<String> filters, String dataQuery, boolean unique, String scenarioCode,
                                Map<String, Object> scenarioParams, Integer order, LocalDateTime creationDate) {
    this.code = code;
    this.title = title;
    this.parentCode = parentCode;
    this.description = description;
    this.filters = CollectionUtils.isEmpty(filters) ?
      Collections.emptyList() : Collections.unmodifiableCollection(new ArrayList<>(filters));
    this.dataQuery = dataQuery;
    this.unique = unique;
    this.scenarioCode = scenarioCode;
    this.scenarioParams = MapUtils.isEmpty(scenarioParams) ? Collections.emptyMap() : Collections.unmodifiableMap(new HashMap<>(scenarioParams));
    this.order = order;
    this.creationDate = creationDate;
  }

  public static Builder builder() {
    return new Builder();
  }

  public String getCode() {
    return code;
  }

  public String getTitle() {
    return title;
  }

  public String getDescription() {
    return description;
  }

  public String getParentCode() {
    return parentCode;
  }

  public Integer getOrder() {
    return order;
  }

  public boolean isUnique() {
    return unique;
  }

  public Collection<String> getFilters() {
    return filters;
  }

  public String getDataQuery() {
    return dataQuery;
  }

  public String getScenarioCode() {
    return scenarioCode;
  }

  public Map<String, Object> getScenarioParams() {
    return scenarioParams;
  }

  public LocalDateTime getCreationDate() {
    return creationDate;
  }

  public static final class Builder {
    private String code;
    private String title;
    private String parentCode;
    private String description;
    private Collection<String> filters;
    private String dataQuery;
    private boolean unique;
    private String scenarioCode;
    private Map<String, Object> scenarioParams;
    private Integer order;
    private LocalDateTime creationDate;

    private Builder() {
    }

    public Builder code(String code) {
      this.code = code;
      return this;
    }

    public Builder title(String title) {
      this.title = title;
      return this;
    }

    public Builder description(String description) {
      this.description = description;
      return this;
    }

    public Builder parentCode(String parentCode) {
      this.parentCode = parentCode;
      return this;
    }

    public Builder order(Integer order) {
      this.order = order;
      return this;
    }

    public Builder filters(Collection<String> filters) {
      this.filters = filters;
      return this;
    }

    public Builder unique(boolean unique) {
      this.unique = unique;
      return this;
    }


    public Builder dataQuery(String dataQuery) {
      this.dataQuery = dataQuery;
      return this;
    }

    public Builder scenarioCode(String scenarioCode) {
      this.scenarioCode = scenarioCode;
      return this;
    }

    public Builder scenarioParams(Map<String, Object> scenarioParams) {
      this.scenarioParams = scenarioParams;
      return this;
    }

    public Builder creationDate(LocalDateTime creationDate) {
      this.creationDate = creationDate;
      return this;
    }

    public QSHandlerSettingsBean build() {
      return new QSHandlerSettingsBean(code, title, parentCode, description, filters, dataQuery, unique, scenarioCode,
        scenarioParams, order, creationDate);
    }
  }
}
