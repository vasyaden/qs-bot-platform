package qs.platform.settings.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import qs.platform.settings.beans.QSHandlerSettingsEntity;

import java.util.Optional;

@Repository
public interface QSHandlersSettingsRepository extends JpaRepository<QSHandlerSettingsEntity, String> {

  Optional<QSHandlerSettingsEntity> findByCode(String code);

}
