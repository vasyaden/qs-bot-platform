package qs.platform.settings.beans;

import org.apache.commons.collections4.CollectionUtils;
import qs.platform.botapi.QSHandlerSettings;
import qs.platform.settings.QSHandlerSettingsTree;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

public class QSHandlerSettingsTreeBean implements QSHandlerSettingsTree {
  private final QSHandlerSettings handlerSettings;
  private final Collection<QSHandlerSettingsTree> children;

  private QSHandlerSettingsTreeBean(QSHandlerSettings handlerSettings,
                                    Collection<QSHandlerSettingsTree> children) {
    this.handlerSettings = handlerSettings;
    this.children = CollectionUtils.isEmpty(children) ?
      Collections.emptyList() : Collections.unmodifiableCollection(new ArrayList<>(children));
  }

  @Override
  public String getCode() {
    return handlerSettings.getCode();
  }

  @Override
  public String getParentCode() {
    return handlerSettings.getParentCode();
  }

  @Override
  public String getTitle() {
    return handlerSettings.getTitle();
  }

  @Override
  public Integer getOrder() {
    return handlerSettings.getOrder();
  }

  @Override
  public boolean isUnique() {
    return handlerSettings.isUnique();
  }

  @Override
  public String getDescription() {
    return handlerSettings.getDescription();
  }

  @Override
  public Collection<String> getFilters() {
    return handlerSettings.getFilters();
  }

  @Override
  public String getDataQuery() {
    return handlerSettings.getDataQuery();
  }

  @Override
  public String getScenarioCode() {
    return handlerSettings.getScenarioCode();
  }

  @Override
  public Map<String, Object> getScenarioParams() {
    return handlerSettings.getScenarioParams();
  }

  @Override
  public LocalDateTime getCreationDate() {
    return handlerSettings.getCreationDate();
  }

  @Override
  public Collection<QSHandlerSettingsTree> getChildren() {
    return children;
  }

  public static Builder builder(QSHandlerSettings settings) {
    return new Builder(settings);
  }


  public static final class Builder {
    private final QSHandlerSettings settings;
    private final Collection<QSHandlerSettingsTreeBean.Builder> children = new ArrayList<>();

    private Builder(QSHandlerSettings settings) {
      this.settings = settings;
    }

    /**
     * @see QSHandlerSettings#getCode()
     */
    public String getCode() {
      return settings.getCode();
    }

    public String getParentCode() {
      return settings.getParentCode();
    }

    public String getDataQuery() {
      return settings.getDataQuery();
    }

    public Builder addChild(QSHandlerSettingsTreeBean.Builder child) {
      children.add(child);
      return this;
    }

    public QSHandlerSettingsTree build() {
      Collection<QSHandlerSettingsTree> child = children.stream().map(Builder::build).toList();
      return new QSHandlerSettingsTreeBean(settings, child);
    }
  }
}
