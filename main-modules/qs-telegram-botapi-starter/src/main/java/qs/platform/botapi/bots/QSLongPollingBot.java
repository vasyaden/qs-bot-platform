package qs.platform.botapi.bots;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.PartialBotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;
import qs.platform.botapi.QSHandlerContextChain;
import qs.platform.botapi.beans.QSEventBean;

import java.util.concurrent.CompletableFuture;

public class QSLongPollingBot extends TelegramLongPollingBot {
  private static final Logger LOG = LoggerFactory.getLogger(QSLongPollingBot.class);

  private final String username;
  private final QSHandlerContextChain context;

  public QSLongPollingBot(QSHandlerContextChain context, String token, String username, int maxThreads) {
    super(DefaultBotOptionsBuilder.newBuilder().maxThreads(maxThreads).build(), token);
    this.context = context;
    this.username = username;
  }

  protected void send(PartialBotApiMethod<?> response) {
    try {
      if (response instanceof BotApiMethod) {
        execute((BotApiMethod<?>) response);
      } else if (response instanceof SendPhoto sendPhoto) {
        execute(sendPhoto);
      } else if (response instanceof SendDocument sendDocument) {
        execute(sendDocument);
      }
    } catch (TelegramApiRequestException e) {
      LOG.error(e.getMessage() + ": " + e.getErrorCode() + " " + e.getApiResponse(), e);
    } catch (TelegramApiException e) {
      LOG.error(e.getMessage(), e);
    }
  }

  @Override
  public void onUpdateReceived(Update update) {
    if (update.hasMessage() || update.hasCallbackQuery()) {
      CompletableFuture.runAsync(() -> {
        try {
          context.tryNext(QSEventBean.from(update)).forEach(this::send);
        } catch (Exception e) {
          LOG.error(e.getMessage(), e);
        }
      });
    }
  }

  @Override
  public String getBotUsername() {
    return username;
  }
}
