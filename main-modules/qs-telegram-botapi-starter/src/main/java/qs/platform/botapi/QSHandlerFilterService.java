package qs.platform.botapi;

import java.util.Collection;

public interface QSHandlerFilterService {

  /**
   * Return a single filter found via its code.
   *
   * @param code identity of the filter. Stored in each filter's class.
   * @return a single {@link QSHandlerFilter filter}.
   */
  QSHandlerFilter getFilter(String code);

  /**
   * Return a collection of {@link QSHandlerFilter filters} identified by their codes.
   *
   * @param codes identities of filters. Stored in each filter's class.
   * @return collection of {@link QSHandlerFilter filters.}
   */
  Collection<QSHandlerFilter> getFilters(Collection<String> codes);

  /**
   * Return all filters present in the application.
   *
   * @return collection of {@link QSHandlerFilter filters.}
   */
  Collection<QSHandlerFilter> getAllFilters();
}
