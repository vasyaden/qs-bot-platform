package qs.platform.botapi.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import qs.platform.botapi.QSHandlerFilterService;
import qs.platform.botapi.QSHandlerSettings;
import qs.platform.botapi.QSHandlersFactory;
import qs.platform.botapi.content.QSContentDistributor;
import qs.platform.botapi.handlers.QSDynamicHandler;
import qs.platform.botapi.handlers.QSHandler;
import qs.platform.botapi.scenarios.QSHandlerScenarioBuilder;
import qs.platform.settings.QSHandlersSettingsService;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class QSHandlersBaseFactory implements QSHandlersFactory {
  private static final Logger LOG = LoggerFactory.getLogger(QSHandlersBaseFactory.class);
  private final Collection<QSHandler> staticHandlers;
  private QSHandlersSettingsService handlersSettingsService;
  private QSHandlerFilterService handlerFilterService;
  private Map<String, ? extends QSHandlerScenarioBuilder> scenarioBuilders;

  @Autowired(required = false)
  private QSHandlersBaseFactory(Collection<QSHandler> staticHandlers, Collection<QSContentDistributor<?>> contentDistributors) {
    if (CollectionUtils.isEmpty(staticHandlers)) {
      this.staticHandlers = Collections.emptyList();
    } else {
      this.staticHandlers = Collections.unmodifiableCollection(new ArrayList<>(staticHandlers));
    }
  }

  @Autowired
  private void setHandlersSettingsService(QSHandlersSettingsService handlersSettingsService) {
    this.handlersSettingsService = handlersSettingsService;
  }

  @Autowired
  private void setHandlerFilterService(QSHandlerFilterService handlerFilterService) {
    this.handlerFilterService = handlerFilterService;
  }

  @Autowired(required = false)
  private void setScenarioBuilders(Collection<? extends QSHandlerScenarioBuilder> scenarioBuilders) {
    this.scenarioBuilders = CollectionUtils.isEmpty(scenarioBuilders) ?
      Collections.emptyMap() : scenarioBuilders.stream()
      .collect(Collectors.toMap(QSHandlerScenarioBuilder::getCode, Function.identity()));
  }

  /**
   * Init dynamic and static handlers
   *
   * @return initialized map of {@link QSHandler handlers}
   */
  @Override
  public Map<String, QSHandler> getHandlers() {
    Collection<QSHandlerSettings> menuSettings = handlersSettingsService.getAll();
    Collection<QSHandler> dynamicHandlers = createDynamicHandlers(menuSettings);
    dynamicHandlers.addAll(staticHandlers);

    Map<String, QSHandler> handlers = dynamicHandlers.stream()
      .collect(Collectors.toMap(QSHandler::getCode, Function.identity()));

    initChildren(handlers);

    return handlers;
  }

  private Collection<QSHandler> createDynamicHandlers(Collection<QSHandlerSettings> menuSettings) {
    if (CollectionUtils.isEmpty(menuSettings)) {
      return new ArrayList<>();
    }
    return menuSettings.stream()
      .map(settings -> {
          QSHandlerScenarioBuilder scenarioBuilder = scenarioBuilders.get(settings.getScenarioCode());
          if (scenarioBuilder == null) {
            LOG.error(
              "Not found scenario with code='{}' for handler with code='{}'",
              settings.getScenarioCode(), settings.getCode()
            );
            return null;
          }
          return QSDynamicHandler.builder()
            .code(settings.getCode())
            .parentCode(settings.getParentCode())
            .title(settings.getTitle())
            .dataQuery(settings.getDataQuery())
            .order(settings.getOrder())
            .unique(settings.isUnique())
            .scenario(scenarioBuilder.build(settings.getScenarioParams()))
            .filters(handlerFilterService.getFilters(settings.getFilters()))
            .build();
        }
      )
      .filter(Objects::nonNull)
      .collect(Collectors.toList());
  }

  private void clearStaticChildren() {
    staticHandlers.forEach(QSHandler::clearChildren);
  }

  private void initChildren(Map<String, QSHandler> handlers) {
    clearStaticChildren();
    for (QSHandler handler : handlers.values()) {
      QSHandler parent = handlers.get(handler.getParentCode());
      if (parent != null) {
        parent.addChild(handler);
      }
    }
  }
}
