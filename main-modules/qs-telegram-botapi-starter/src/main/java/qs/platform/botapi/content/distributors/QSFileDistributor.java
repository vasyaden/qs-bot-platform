package qs.platform.botapi.content.distributors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.PartialBotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import qs.platform.botapi.content.beans.QSFile;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class QSFileDistributor extends QSAbstractContentDistributor<QSFile> {
  private static final Logger LOG = LoggerFactory.getLogger(QSFileDistributor.class);

  @Override
  public Collection<PartialBotApiMethod<?>> getContent(Stream<QSFile> content, String chatId) {
    return content.map(c -> getSendDocument(c.getInputFile(), chatId, c.getCaption()))
      .collect(Collectors.toList());
  }

  private SendDocument getSendDocument(InputFile file, String chatId, String caption) {
    SendDocument.SendDocumentBuilder document = SendDocument.builder()
      .chatId(chatId)
      .document(file);
    if (StringUtils.isNotBlank(caption)) {
      document.caption(caption);
    }
    return document.build();
  }
}
