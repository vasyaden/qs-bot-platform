package qs.platform.botapi.stateful;

import qs.platform.botapi.QSJsonData;

import java.time.LocalDateTime;

/**
 * User state
 */
public interface QSUserState {
  /**
   * @return telegramId
   */
  Long getTelegramId();

  /**
   * @return stateCode - code of handler which have to handle requests of user in statefull regime
   */
  String getStateCode();

  /**
   * @return {@link QSJsonData}
   */
  QSJsonData getStateData();

  /**
   * @return stateLifetime
   */
  LocalDateTime getStateLifeTime();

  /**
   * @return is state expired
   */
  boolean isExpired();
}
