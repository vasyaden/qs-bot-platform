package qs.platform.botapi.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import qs.platform.botapi.QSHandlerCache;
import qs.platform.botapi.QSHandlersFactory;
import qs.platform.botapi.events.QSHandlersCacheRefreshedEvent;
import qs.platform.botapi.handlers.QSHandler;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

@Service
public class QSHandlerCacheService implements QSHandlerCache {

  private final QSHandlersFactory handlersFactory;
  private final ApplicationEventPublisher publisher;
  private Map<String, QSHandler> cache;

  @Autowired
  private QSHandlerCacheService(QSHandlersFactory handlersFactory, ApplicationEventPublisher publisher) {
    this.handlersFactory = handlersFactory;
    this.publisher = publisher;
  }

  @EventListener
  private void handleContextRefreshed(ContextRefreshedEvent event) {
    refresh();
    publisher.publishEvent(QSHandlersCacheRefreshedEvent.builder().source(this).build());
  }

  @Override
  public void refresh() {
    this.cache = handlersFactory.getHandlers();
  }

  @Override
  public QSHandler getHandler(String code) {
    if (cache == null) {
      refresh();
    }
    return cache.get(code);
  }

  @Override
  public Collection<QSHandler> getHandlers() {
    if (cache == null) {
      refresh();
    }
    return Collections.unmodifiableCollection(cache.values());
  }
}
