package qs.platform.botapi.handlers;

import org.apache.commons.collections4.list.TreeList;
import org.telegram.telegrambots.meta.api.methods.PartialBotApiMethod;
import qs.platform.botapi.QSEvent;
import qs.platform.botapi.QSHandlerFilter;
import qs.platform.botapi.QSJsonData;
import qs.platform.botapi.scenarios.QSHandlerScenario;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.TreeSet;

/**
 * Base logic of the handler
 */
public abstract class QSBaseHandler implements QSHandler {

  private final QSHandlerScenario scenario;

  private Collection<QSHandler> mutableChildren = new TreeSet<>();
  private Collection<QSHandler> children = Collections.unmodifiableCollection(mutableChildren);

  protected QSBaseHandler(QSHandlerScenario scenario) {
    this.scenario = scenario;
  }

  /**
   * @see QSHandler#handle(QSEvent, QSJsonData)
   */
  @Override
  public Collection<PartialBotApiMethod<?>> handle(QSEvent event, QSJsonData jsonData) {
    return scenario.handle(this, event, jsonData);
  }

  @Override
  public void addChild(QSHandler handler) {
    mutableChildren.add(handler);
  }

  /**
   * Clears the list of handler's children
   */
  @Override
  public void clearChildren() {
    this.mutableChildren = new ArrayList<>();
    this.children = Collections.unmodifiableCollection(mutableChildren);
  }

  /**
   * @see QSHandler#getChildren()
   */
  @Override
  public Collection<QSHandler> getChildren() {
    return children;
  }

  /**
   * @see QSHandler#check(QSJsonData)
   */
  @Override
  public boolean check(QSJsonData jsonData) {
    return getFilters().stream()
      .filter(x -> !x.check(jsonData))
      .findFirst()
      .map(x -> Boolean.FALSE)
      .orElse(Boolean.TRUE);
  }

  /**
   * Filters assigned to this handler
   */
  protected Collection<QSHandlerFilter> getFilters() {
    return Collections.emptyList();
  }

  @Override
  public int compareTo(QSHandler o) {
    return getOrder().compareTo(o.getOrder());
  }
}
