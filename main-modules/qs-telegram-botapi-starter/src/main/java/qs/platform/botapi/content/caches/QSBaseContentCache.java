package qs.platform.botapi.content.caches;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import qs.platform.botapi.content.QSContent;
import qs.platform.botapi.content.QSContentCache;
import qs.platform.dbaccess.services.QSContentService;

import java.util.*;

public abstract class QSBaseContentCache<C extends QSContent> implements QSContentCache<C> {
  private static final Logger LOG = LoggerFactory.getLogger(QSBaseContentCache.class);

  private Map<String, Collection<C>> cache;

  private QSContentService<C> service;

  @Autowired
  private void setService(QSContentService<C> service) {
    this.service = service;
  }

  @EventListener
  private void handleContextRefreshed(ContextRefreshedEvent event) {
    refresh();
  }

  @Override
  public void refresh() {
    Map<String, Collection<C>> newCache = new HashMap<>();

    service
      .findCacheable()
      .stream()
      .sorted(
        Comparator.comparing(C::getOrder)
      )
      .forEach(
        content -> newCache
          .computeIfAbsent(
            content.getHandlerCode(),
            x -> new ArrayList<>()
          )
          .add(content)
      );

    this.cache = Collections.unmodifiableMap(newCache);
    LOG.info(this.getClass().getSimpleName() + " refreshed");
  }

  @Override
  public Collection<C> get(String handlerCode) {
    return getContent(handlerCode).orElse(Collections.emptyList());
  }

  @Override
  public Optional<Collection<C>> getContent(String handlerCode) {
    if (cache == null) {
      refresh();
    }
    return Optional.ofNullable(cache.get(handlerCode))
      .map(Collections::unmodifiableCollection);
  }


}
