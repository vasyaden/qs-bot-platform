package qs.platform.botapi.scenarios;

import org.springframework.beans.factory.annotation.Autowired;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import qs.platform.botapi.QSEventType;
import qs.platform.botapi.QSJsonData;
import qs.platform.botapi.QSRequestDataService;
import qs.platform.botapi.beans.QSJsonDataBean;
import qs.platform.botapi.scenarios.annotations.QSHandlerScenarioParam;
import qs.platform.botapi.scenarios.annotations.QSHandlerScenarioSetting;
import qs.platform.botapi.stateful.QSUserStateService;
import qs.platform.dbaccess.services.QSQueryService;
import qs.platform.exceptions.QSRuntimeException;
import qs.platform.types.QSType;

import java.util.Collections;
import java.util.Map;

/**
 * Handler for request any data from user by stateful mode
 */
@QSHandlerScenarioSetting(
  code = QSRequestToInsertParamHandler.CODE,
  title = "Stateful request of the data",
  description = "Send request-message to user and set him state",
  params = {
    @QSHandlerScenarioParam(
      code = QSRequestToInsertParamHandler.REQUEST_MESSAGE_TEXT,
      title = "Message",
      description = "Text of the request-message",
      type = QSType.STRING
    ),
    @QSHandlerScenarioParam(
      code = QSRequestToInsertParamHandler.TO_STATE,
      title = "State that should be set as result",
      type = QSType.STRING
    )
  }
)
public class QSRequestToInsertParamHandler extends QSBaseHandlerScenarioBuilder {
  public static final String CODE = "qsRequestInsertDataStatefulScenario";
  protected static final String REQUEST_MESSAGE_TEXT = "requestMessageText";
  protected static final String TO_STATE = "toState";

  @Autowired
  private QSRequestToInsertParamHandler(QSQueryService queryService, QSRequestDataService requestDataService, QSUserStateService stateService) {
    super(queryService, requestDataService, stateService);
  }

  @Override
  public QSHandlerScenario build(Map<String, Object> params) {
    checkParams(params);

    final String messageText = (String) params.get(REQUEST_MESSAGE_TEXT);
    final String toState = (String) params.get(TO_STATE);

    return (handler, event, jsonData) -> {
      Integer messageId = event.getMessageId();

      QSJsonData stateData = QSJsonDataBean.builder()
        .id(jsonData.getId())
        .code(jsonData.getCode())
        .userId(jsonData.getUserId())
        .data(jsonData.getData())
        .addParam(QSParamFromEventHandler.MESSAGE_ID, messageId)
        .addParam(QSParamFromEventHandler.MESSAGE_TEXT, messageText)
        .previousId(jsonData.getPreviousId())
        .build();

      toState(toState, stateData);

      if (event.getType() == QSEventType.CALLBACK) {
        return Collections.singleton(
          EditMessageText.builder()
            .chatId(event.getChatIdString())
            .messageId(messageId)
            .text(messageText)
            .replyMarkup(
              InlineKeyboardMarkup.builder()
                .keyboard(Collections.singleton(initBackButtons(getPreviousId(handler, jsonData))))
                .build()
            )
            .build()
        );
      } else {
        throw new QSRuntimeException("qs.invalid.event.type", "Invalid event type: " + event.getType());
      }
    };
  }
}
