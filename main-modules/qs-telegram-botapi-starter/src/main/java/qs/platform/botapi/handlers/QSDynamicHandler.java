package qs.platform.botapi.handlers;

import org.apache.commons.collections4.CollectionUtils;
import qs.platform.botapi.QSHandlerFilter;
import qs.platform.botapi.scenarios.QSHandlerScenario;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

/**
 * Main implementation of the {@link QSHandler}
 */
public class QSDynamicHandler extends QSBaseHandler {

  private final String code;
  private final String title;
  private final String dataQuery;
  private final String parentCode;
  private final Integer order;
  private final boolean unique;
  private final Collection<QSHandlerFilter> filters;

  private QSDynamicHandler(QSHandlerScenario scenario, String code, String title,
                           String dataQuery, String parentCode, Integer order,
                           boolean unique, Collection<QSHandlerFilter> filters) {
    super(scenario);
    this.code = code;
    this.title = title;
    this.dataQuery = dataQuery;
    this.parentCode = parentCode;
    this.order = order;
    this.unique = unique;
    this.filters = CollectionUtils.isEmpty(filters) ?
      Collections.emptyList() : Collections.unmodifiableCollection(new ArrayList<>(filters));
  }

  @Override
  public String getCode() {
    return code;
  }

  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public Optional<String> getDataQuery() {
    return Optional.ofNullable(dataQuery);
  }

  @Override
  public String getParentCode() {
    return parentCode;
  }

  @Override
  public Integer getOrder() {
    return order;
  }

  @Override
  public boolean isUnique() {
    return unique;
  }

  @Override
  protected Collection<QSHandlerFilter> getFilters() {
    return filters;
  }

  public static Builder builder() {
    return new Builder();
  }

  public static final class Builder {
    private String code;
    private String title;
    private String dataQuery;
    private String parentCode;
    private Collection<QSHandlerFilter> filters;
    private int order;
    private boolean unique = false;
    private QSHandlerScenario scenario;

    private Builder() {
    }

    public Builder code(String code) {
      this.code = code;
      return this;
    }

    public Builder title(String title) {
      this.title = title;
      return this;
    }

    public Builder dataQuery(String dataQuery) {
      this.dataQuery = dataQuery;
      return this;
    }

    public Builder order(int order) {
      this.order = order;
      return this;
    }

    public Builder parentCode(String parentCode) {
      this.parentCode = parentCode;
      return this;
    }

    public Builder filters(Collection<QSHandlerFilter> filters) {
      this.filters = filters;
      return this;
    }

    public Builder unique(boolean unique) {
      this.unique = unique;
      return this;
    }

    public Builder scenario(QSHandlerScenario scenario) {
      this.scenario = scenario;
      return this;
    }

    public QSDynamicHandler build() {
      return new QSDynamicHandler(scenario, code, title, dataQuery, parentCode, order, unique, filters);
    }
  }
}
