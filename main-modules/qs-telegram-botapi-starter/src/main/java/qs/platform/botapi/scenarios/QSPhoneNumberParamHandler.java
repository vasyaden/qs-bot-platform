package qs.platform.botapi.scenarios;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import qs.platform.botapi.QSEvent;
import qs.platform.botapi.QSRequestDataService;
import qs.platform.botapi.scenarios.annotations.QSHandlerScenarioParam;
import qs.platform.botapi.scenarios.annotations.QSHandlerScenarioSetting;
import qs.platform.botapi.stateful.QSUserStateService;
import qs.platform.dbaccess.services.QSQueryService;
import qs.platform.types.QSType;

/**
 * gets the phone number from the event
 */
@QSHandlerScenarioSetting(
  code = QSPhoneNumberParamHandler.CODE,
  title = "Phone number from message handler",
  description = "Extracts phone number as long from the message text",
  params = {
    @QSHandlerScenarioParam(
      code = QSParamFromEventHandler.ERROR_MESSAGE,
      title = "Error message",
      description = "Message will be sent if result is invalid",
      type = QSType.STRING
    ),
    @QSHandlerScenarioParam(
      code = QSParamFromEventHandler.NEXT_HANDLER,
      title = "Next handler code",
      description = "Code of the handler, which should be invoked as result with extracted output parameter",
      type = QSType.STRING
    ),
    @QSHandlerScenarioParam(
      code = QSParamFromEventHandler.OUTPUT_PARAM,
      title = "Output parameter",
      description = "Parameter where extracted text will be placed",
      type = QSType.STRING
    )
  }
)
public class QSPhoneNumberParamHandler extends QSParamFromEventHandler<Long> {
  public static final String CODE = "qsPhoneNumberFromMessageTextScenario";
  @Value("${qs.default-phone-number-code}")
  private String phoneNumberCode;

  @Autowired
  private QSPhoneNumberParamHandler(QSQueryService queryService, QSRequestDataService requestDataService, QSUserStateService stateService) {
    super(queryService, requestDataService, stateService);
  }

  @Override
  protected Long getValue(QSEvent event) {
    return parse(event.getMessageText());
  }

  private Long parse(String phoneNumber) {
    if (StringUtils.isBlank(phoneNumber)) {
      return null;
    }
    String newPhoneNumber = phoneNumber.trim().replaceAll("\\D", "");
    if (newPhoneNumber.startsWith("8")) {
      newPhoneNumber = phoneNumberCode + newPhoneNumber.substring(1);
    }
    try {
      return Long.parseLong(newPhoneNumber);
    } catch (Exception e) {
      return null;
    }
  }

  @Override
  protected String getInvalidValue(QSEvent event) {
    return event.getMessageText();
  }
}
