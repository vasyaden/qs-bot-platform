package qs.platform.botapi.bots;

import org.telegram.telegrambots.bots.DefaultBotOptions;

/**
 * Builder for {@link DefaultBotOptions}
 */
public class DefaultBotOptionsBuilder {
  private int maxThreads = 1;

  private DefaultBotOptionsBuilder() {
  }

  /**
   * New builder instance
   */
  public static DefaultBotOptionsBuilder newBuilder() {
    return new DefaultBotOptionsBuilder();
  }

  /**
   * Configure max threads for sending responses
   *
   * @param maxThreads max threads
   * @return {@link DefaultBotOptionsBuilder Builder}
   */
  public DefaultBotOptionsBuilder maxThreads(int maxThreads) {
    this.maxThreads = maxThreads;
    return this;
  }

  /**
   * Build {@link DefaultBotOptions}
   *
   * @return {@link DefaultBotOptions}
   */
  public DefaultBotOptions build() {
    DefaultBotOptions options = new DefaultBotOptions();
    options.setMaxThreads(maxThreads);
    return options;
  }
}
