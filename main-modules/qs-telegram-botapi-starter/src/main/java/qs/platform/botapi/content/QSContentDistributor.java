package qs.platform.botapi.content;

import org.telegram.telegrambots.meta.api.methods.PartialBotApiMethod;
import qs.platform.botapi.QSJsonData;

import java.util.Collection;
import java.util.stream.Stream;

public interface QSContentDistributor<C> {

  /**
   * Получает контентный ответ
   *
   * @param jsonData JSON data
   * @return контентный ответ
   */
  Collection<PartialBotApiMethod<?>> getContent(QSJsonData jsonData);

  /**
   * Получает контентный ответ
   * @param content контент
   * @param chatId id чата
   * @return контентный ответ
   */
  Collection<PartialBotApiMethod<?>> getContent(Stream<C> content, String chatId);

}
