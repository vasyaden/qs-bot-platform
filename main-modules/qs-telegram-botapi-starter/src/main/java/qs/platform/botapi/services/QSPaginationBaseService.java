package qs.platform.botapi.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import qs.platform.botapi.QSJsonData;
import qs.platform.botapi.QSPaginationService;
import qs.platform.botapi.QSRequestDataService;
import qs.platform.botapi.beans.QSJsonDataBean;
import qs.platform.converters.QSConverterService;

import java.util.*;

@Component
public class QSPaginationBaseService implements QSPaginationService {

  public static final String PAGE_NUMBER = "pageNum";
  public static final int FIRST_PAGE_NUM = 0;
  private static final int MAX_PAGINATION_BUTTONS = 5;
  private static final int SECOND_PAGE_NUM = 1;
  private static final int THIRD_PAGE_NUM = 2;
  public static final int MIN_PAGES_FOR_PAGINATION = 2;
  private static final int FOURTH_PAGE_NUM = 3;
  private static final String FIRST_PAGE_TITLE = "1";
  private static final String SECOND_PAGE_TITLE = "2";
  private static final String THIRD_PAGE_TITLE = "3";
  private static final String FOURTH_PAGE_TITLE = "4";
  private static final String TO_FIRST_ARROW = "<< ";
  private static final String TO_LAST_ARROW = " >>";
  private static final String CURRENT_SIGN = "-";

  private final QSRequestDataService requestDataService;
  private final QSConverterService converter;

  @Autowired
  public QSPaginationBaseService(QSRequestDataService requestDataService, QSConverterService converter) {
    this.requestDataService = requestDataService;
    this.converter = converter;
  }

  public List<InlineKeyboardButton> getButtons(String handlerCode,
                                               int totalPages,
                                               int currentPage,
                                               Long chatId,
                                               UUID previousId) {

    return getButtons(handlerCode, totalPages, currentPage, chatId, previousId, null);
  }

  public List<InlineKeyboardButton> getButtons(String handlerCode,
                                               int totalPages,
                                               int currentPage,
                                               Long chatId,
                                               UUID previousId,
                                               Map<String, Object> data) {

    if (totalPages < MIN_PAGES_FOR_PAGINATION) {
      return Collections.emptyList();
    }
    return getPaginationButtonsMap(totalPages, currentPage)
      .entrySet()
      .stream()
      .map(entry -> InlineKeyboardButton.builder()
        .callbackData(getJsonId(handlerCode, chatId, previousId, entry.getKey(), data).toString())
        .text(entry.getValue())
        .build()
      )
      .toList();
  }

  private Map<Integer, String> getPaginationButtonsMap(int totalPages, int currentPage) {
    if (totalPages <= MAX_PAGINATION_BUTTONS) {
      return getAllPagesButtonsMap(totalPages, currentPage);
    } else {
      return getShortedPagesButtonsMap(totalPages, currentPage);
    }
  }

  private Map<Integer, String> getAllPagesButtonsMap(int totalPages, int currentPage) {
    Map<Integer, String> numbers = new TreeMap<>();
    for (int i = 0; i < totalPages; i++) {
      numbers.put(i, currentPage == i ? "-" + (i + 1) + "-" : String.valueOf(i + 1));
    }
    return numbers;
  }

  private Map<Integer, String> getShortedPagesButtonsMap(int totalPages, int currentPage) {
    Map<Integer, String> numbers = new TreeMap<>();
    numbers.put(FIRST_PAGE_NUM, currentPage > 2 ? TO_FIRST_ARROW + FIRST_PAGE_TITLE : FIRST_PAGE_TITLE);
    numbers.put(totalPages - 1, totalPages - 3 > currentPage ? totalPages + TO_LAST_ARROW : String.valueOf(totalPages));
    numbers.put(currentPage, CURRENT_SIGN + (currentPage + 1) + CURRENT_SIGN);

    //Если текущая страница - первая или вторая
    if (currentPage <= SECOND_PAGE_NUM) {
      //Если текущая страница - первая
      if (currentPage == FIRST_PAGE_NUM) {
        numbers.put(SECOND_PAGE_NUM, SECOND_PAGE_TITLE);
      }
      numbers.put(THIRD_PAGE_NUM, THIRD_PAGE_TITLE);
      numbers.put(FOURTH_PAGE_NUM, FOURTH_PAGE_TITLE);
      return numbers;
    }
    //Если текущая страница - предпоследняя или последняя
    else if (currentPage >= totalPages - 2) {
      numbers.put(totalPages - 3, String.valueOf(totalPages - 2));
      numbers.put(totalPages - 4, String.valueOf(totalPages - 3));
      //Если текущая страница - последняя
      if (currentPage == totalPages - 1) {
        numbers.put(totalPages - 2, String.valueOf(totalPages - 1));
      }
      return numbers;
    } else {
      numbers.put(currentPage - 1, String.valueOf(currentPage));
      numbers.put(currentPage + 1, String.valueOf(currentPage + 2));
      return numbers;
    }
  }

  private UUID getJsonId(String code, Long chatId, UUID previousId, Integer value, Map<String, Object> data) {
    return requestDataService.save(
        QSJsonDataBean.builder()
          .userId(chatId)
          .code(code)
          .data(data)
          .addParam(PAGE_NUMBER, value)
          .previousId(previousId)
          .build()
      )
      .getId();
  }

  @Override
  public int getCurrentPageNumber(QSJsonData jsonData) {
    Integer currentPage = converter.convertTo(Integer.class, jsonData.getParam(PAGE_NUMBER));
    return currentPage == null ? FIRST_PAGE_NUM : currentPage;
  }
}

