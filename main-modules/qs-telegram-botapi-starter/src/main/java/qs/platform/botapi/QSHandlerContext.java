package qs.platform.botapi;

import org.telegram.telegrambots.meta.api.methods.PartialBotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;
import qs.platform.botapi.handlers.QSHandler;

import java.util.Collection;

/**
 * Context that stores the {@link QSHandler handlers}
 * and selects the desired {@link QSHandler handler} for given {@link Update}
 */
public interface QSHandlerContext {

  /**
   * Process {@link Update income update}
   *
   * @param event {@link Update income update} to be processed
   * @return collection of responses
   */
  Collection<PartialBotApiMethod<?>> processUpdate(QSEvent event);

}
