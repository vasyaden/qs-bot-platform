package qs.platform.botapi.content.caches;

import org.springframework.stereotype.Component;
import qs.platform.botapi.content.beans.QSFile;

@Component
public class QSFileCache extends QSBaseContentCache<QSFile> {
  @Override
  public String getCode() {
    return "qsFileCache";
  }
}
