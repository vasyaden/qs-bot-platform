package qs.platform.botapi.contexts;

import org.springframework.beans.factory.annotation.Autowired;
import org.telegram.telegrambots.meta.api.methods.PartialBotApiMethod;
import qs.platform.botapi.QSEvent;
import qs.platform.botapi.QSHandlerContext;
import qs.platform.botapi.QSHandlerContextChain;
import qs.platform.botapi.handlers.QSErrorHandler;
import qs.platform.exceptions.QSHandlerNotFoundException;

import java.util.Collection;
import java.util.function.Supplier;

/**
 * Base logic of {@link QSHandlerContextChain}
 */
public abstract class QSHandlerContextBaseChain implements QSHandlerContext, QSHandlerContextChain {

  private QSHandlerContextChain next;
  private QSErrorHandler errorHandler;

  @Autowired
  private void setErrorHandler(QSErrorHandler errorHandler) {
    this.errorHandler = errorHandler;
  }

  protected QSErrorHandler getErrorHandler() {
    return errorHandler;
  }

  @Override
  public void addLast(QSHandlerContextChain last) {
    if (this.next == null) {
      this.next = last;
    } else {
      this.next.addLast(last);
    }
  }

  @Override
  public Collection<PartialBotApiMethod<?>> tryNext(QSEvent event) {
    return withErrorHandling(event, () -> this.processUpdate(event));
  }

  protected Collection<PartialBotApiMethod<?>> withErrorHandling(QSEvent event, Supplier<Collection<PartialBotApiMethod<?>>> supplier) {
    try {
      return supplier.get();
    } catch (QSHandlerNotFoundException e) {
      if (next == null) {
        return errorHandler.handle(event, e);
      } else {
        return next.tryNext(event);
      }
    } catch (Exception e) {
      return errorHandler.handle(event, e);
    }
  }

}
