package qs.platform.botapi.content.beans;

import org.apache.commons.collections4.MapUtils;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import qs.platform.botapi.content.QSContent;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class QSImage implements QSContent {

  private final InputFile inputFile;
  private final String caption;
  private final String handlerCode;
  private final Map<String, Object> params;
  private final Integer order;

  private QSImage(InputFile inputFile, String caption, String handlerCode, Map<String, Object> params, Integer order) {
    this.inputFile = inputFile;
    this.caption = caption;
    this.handlerCode = handlerCode;
    this.params = MapUtils.isEmpty(params) ? Collections.emptyMap() : Collections.unmodifiableMap(new HashMap<>(params));
    this.order = order;
  }

  public InputFile getInputFile() {
    return inputFile;
  }

  public String getCaption() {
    return caption;
  }

  public String getHandlerCode() {
    return handlerCode;
  }

  public Map<String, Object> getParams() {
    return params;
  }

  public Integer getOrder() {
    return order;
  }

  public static Builder builder() {
    return new Builder();
  }
  public static final class Builder {
    private InputFile inputFile;
    private String caption;
    private String handlerCode;
    private Map<String, Object> params;
    private Integer order;

    private Builder(){}

    public Builder inputFile(InputFile inputFile) {
      this.inputFile = inputFile;
      return this;
    }

    public Builder caption(String caption) {
      this.caption = caption;
      return this;
    }

    public Builder handlerCode(String handlerCode) {
      this.handlerCode = handlerCode;
      return this;
    }

    public Builder params(Map<String, Object> params) {
      this.params = params;
      return this;
    }

    public Builder order(Integer order) {
      this.order = order;
      return this;
    }

    public QSImage build() {
      return new QSImage(inputFile, caption, handlerCode, params, order);
    }
  }
}
