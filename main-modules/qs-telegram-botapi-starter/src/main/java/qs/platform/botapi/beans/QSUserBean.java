package qs.platform.botapi.beans;

import org.telegram.telegrambots.meta.api.objects.User;
import qs.platform.botapi.QSUser;

/**
 * Encapsulates details of a Telegram user who initiated the event, providing a structured way to access user information
 */
public class QSUserBean implements QSUser {
  private final Long id;
  private final String firstName;
  private final String lastName;
  private final String userName;

  private QSUserBean(User from) {
    this.id = from.getId();
    this.firstName = from.getFirstName();
    this.lastName = from.getLastName();
    this.userName = from.getUserName();
  }

  @Override
  public Long getId() {
    return id;
  }

  @Override
  public String getFirstName() {
    return firstName;
  }

  @Override
  public String getLastName() {
    return lastName;
  }

  @Override
  public String getUserName() {
    return userName;
  }

  /**
   * Factory method to create an instance of {@link QSUserBean} from a Telegram User object
   *
   * @param from {@link User} object provided by the Telegram API
   * @return a new instance of {@link QSUserBean}
   */
  public static QSUserBean from(User from) {
    return new QSUserBean(from);
  }

}