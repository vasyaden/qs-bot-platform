package qs.platform.botapi.scenarios;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import qs.platform.botapi.QSJsonData;
import qs.platform.botapi.QSRequestDataService;
import qs.platform.botapi.beans.QSJsonDataBean;
import qs.platform.botapi.handlers.QSEmptyHandler;
import qs.platform.botapi.scenarios.annotations.QSHandlerScenarioParam;
import qs.platform.botapi.scenarios.annotations.QSHandlerScenarioSetting;
import qs.platform.botapi.stateful.QSUserStateService;
import qs.platform.converters.QSConverterService;
import qs.platform.dbaccess.services.QSQueryService;
import qs.platform.directories.QSDirectory;
import qs.platform.directories.services.MonthDirectoryService;
import qs.platform.directories.services.WeekdaysDirectoryService;
import qs.platform.exceptions.QSHandlerNotFoundException;
import qs.platform.types.QSType;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Stream;

import static org.apache.commons.lang3.StringUtils.SPACE;

@QSHandlerScenarioSetting(
  code = QSCalendarScenario.CODE,
  title = "Date-picker",
  description = "Interactive calendar, calls the next handler with chosen date as output parameter",
  params = {
    @QSHandlerScenarioParam(
      code = QSCalendarScenario.MESSAGE,
      title = "Message",
      description = "The message that will be sent along with the calendar",
      type = QSType.STRING,
      required = false
    ),
    @QSHandlerScenarioParam(
      code = QSCalendarScenario.NEXT_HANDLER,
      title = "Next handler",
      description = "The handler that will be called with the chosen date",
      type = QSType.STRING
    ),
    @QSHandlerScenarioParam(
      code = QSCalendarScenario.OUTPUT_PARAM,
      title = "Output parameter",
      description = "Code of the parameter, where chosen date should be placed",
      type = QSType.STRING
    )
  }
)
public final class QSCalendarScenario extends QSBaseHandlerScenarioBuilder {

  public static final String CODE = "qsCalendarScenario";
  static final String MESSAGE = "message";
  static final String NEXT_HANDLER = "nextHandler";
  static final String OUTPUT_PARAM = "outputAttr";
  private static final String CALENDAR_SYMBOL = "🗓";
  private static final String DEFAULT_MESSAGE = CALENDAR_SYMBOL + "Выберите дату:";
  private static final String TO_PREVIOUS_ARROW = "\u23EA";
  private static final String TO_NEXT_ARROW = "\u23E9";
  public static final String EXTERNAL_PREVIOUS_ID = "previousId";
  public static final String DATE = "date";
  public static final String QS_START_DATE = "qsStartDate";
  public static final String QS_END_DATE = "qsEndDate";
  private static final int FIRST = 1;
  private static final int WEEK_SIZE = 7;
  private static final int LAST_MONTH = 12;
  private final QSRequestDataService requestDataService;
  private final QSConverterService converterService;
  private MonthDirectoryService<? extends QSDirectory<Integer>> monthDirectoryService;
  private WeekdaysDirectoryService<? extends QSDirectory<Integer>> weekdaysDirectoryService;

  @Autowired
  private QSCalendarScenario(QSQueryService queryService, QSRequestDataService requestDataService,
                             QSUserStateService stateService, QSConverterService converterService) {
    super(queryService, requestDataService, Collections.emptyList(), stateService);
    this.requestDataService = requestDataService;
    this.converterService = converterService;
  }

  @Autowired
  public void setMonthDirectoryService(MonthDirectoryService<? extends QSDirectory<Integer>> monthDirectoryService) {
    this.monthDirectoryService = monthDirectoryService;
  }

  @Autowired
  public void setWeekdaysDirectoryService(WeekdaysDirectoryService<? extends QSDirectory<Integer>> weekdaysDirectoryService) {
    this.weekdaysDirectoryService = weekdaysDirectoryService;
  }

  @Override
  public QSHandlerScenario build(Map<String, Object> params) {
    checkParams(params);

    final String message = converterService.convertTo(String.class, params.get(MESSAGE));
    final String nextHandler = converterService.convertTo(String.class, params.get(NEXT_HANDLER));
    final String outputAttr = converterService.convertTo(String.class, params.get(OUTPUT_PARAM));

    return (handler, event, jsonData) -> {
      if (!event.hasCallbackQuery()) {
        throw new QSHandlerNotFoundException("invalid.handler.invocation.method", CODE + " scenario can handle only callback queries");
      }

      Long chatId = event.getChatId();
      LocalDate date = Optional.ofNullable(jsonData.getParam(DATE))
        .map(d -> converterService.convertTo(LocalDate.class, d))
        .orElse(LocalDate.now());
      UUID internalPreviousId = getPreviousId(handler, jsonData);
      UUID externalPreviousId = nextHandler.equals(handler.getParentCode()) ? getExternalPreviousId(jsonData) : jsonData.getId();

      int year = date.getYear();
      int month = date.getMonth().getValue();

      Collection<List<InlineKeyboardButton>> calendar = getDaysButtons(jsonData, nextHandler, outputAttr, year, month, chatId, externalPreviousId);
      calendar.add(getMonthButtons(jsonData, year, month, chatId, internalPreviousId));
      calendar.add(getYearsButtons(jsonData, year, month, chatId, internalPreviousId));
      calendar.add(initBackButtons(internalPreviousId));

      InlineKeyboardMarkup keyboard = InlineKeyboardMarkup.builder()
        .keyboard(calendar)
        .build();

      return Collections.singletonList(
        EditMessageText.builder()
          .chatId(chatId.toString())
          .text(getCalendarMessage(message))
          .replyMarkup(keyboard)
          .messageId(event.getMessageId())
          .build()
      );
    };
  }

  private UUID getExternalPreviousId(QSJsonData jsonData) {
    return converterService.convertTo(UUID.class, jsonData.getParam(EXTERNAL_PREVIOUS_ID));
  }

  private Collection<List<InlineKeyboardButton>> getDaysButtons(QSJsonData jsonData,
                                                                String nextHandler,
                                                                String outputAttr,
                                                                Integer year,
                                                                Integer month,
                                                                Long chatId,
                                                                UUID previousId) {
    Collection<List<InlineKeyboardButton>> thisMonth = new ArrayList<>();
    LocalDate start = parseDate(jsonData.getParam(QS_START_DATE));
    LocalDate end = parseDate(jsonData.getParam(QS_END_DATE));
    Pair<LocalDate, LocalDate> highlightingRange = getHighlightingRange(start, end);
    thisMonth.add(getDaysNameButtons(chatId));
    LocalDate from = LocalDate.of(year, month, FIRST);
    QSJsonDataBean.Builder jsonBuilder = QSJsonDataBean.builder()
      .code(nextHandler)
      .userId(chatId)
      .previousId(previousId)
      .data(clearCalendarParams(jsonData.getData()));
    while (from.getMonth().getValue() == month) {
      LocalDate to = from.plusDays(DayOfWeek.SUNDAY.getValue() - from.getDayOfWeek().getValue());
      if (to.getMonth().getValue() != month) {
        to = LocalDate.of(year, month, FIRST).plusMonths(1).minusDays(1);
      }
      thisMonth.add(getWeek(from, to, outputAttr, jsonBuilder, chatId, highlightingRange));
      from = to.plusDays(1);
    }
    return thisMonth;
  }

  private List<InlineKeyboardButton> getWeek(LocalDate from, LocalDate to, String outputAttr, QSJsonDataBean.Builder jsonBuilder, long chatId, Pair<LocalDate, LocalDate> highlightingRange) {
    List<InlineKeyboardButton> week = new ArrayList<>();
    DayOfWeek fromDayOfWeek = from.getDayOfWeek();
    DayOfWeek toDayOfWeek = to.getDayOfWeek();
    if (fromDayOfWeek != DayOfWeek.MONDAY) {
      List<InlineKeyboardButton> startEmpties = getEmptyButtons(fromDayOfWeek.getValue() - DayOfWeek.MONDAY.getValue(), chatId);
      week.addAll(startEmpties);
    }
    LocalDate day = LocalDate.from(from);
    while (!day.isAfter(to)) {
      String jsonId = getJsonId(jsonBuilder.addParam(outputAttr, day).build());
      String dayTitle = getDayTitle(highlightingRange, day);
      week.add(getButton(dayTitle, jsonId));
      day = day.plusDays(1);
    }

    if (toDayOfWeek != DayOfWeek.SUNDAY) {
      List<InlineKeyboardButton> endEmpties = getEmptyButtons(DayOfWeek.SUNDAY.getValue() - toDayOfWeek.getValue(), chatId);
      week.addAll(endEmpties);
    }
    return week;
  }

  private InlineKeyboardButton getEmptyButton(String title, long chatId) {
    UUID jsonId = requestDataService.save(QSJsonDataBean.builder()
      .code(QSEmptyHandler.CODE)
      .userId(chatId)
      .unique(true)
      .build()).getId();
    return getButton(title, String.valueOf(jsonId));
  }

  private List<InlineKeyboardButton> getEmptyButtons(int i, long chatId) {
    return Collections.nCopies(i, getEmptyButton(SPACE, chatId));
  }

  private List<InlineKeyboardButton> getDaysNameButtons(Long chatId) {
    List<InlineKeyboardButton> daysName = new ArrayList<>();
    for (int i = 0; i < WEEK_SIZE; i++) {
      daysName.add(getEmptyButton(weekdaysDirectoryService.getTitle(i), chatId));
    }
    return daysName;
  }

  private List<InlineKeyboardButton> getMonthButtons(QSJsonData jsonData, Integer year, Integer month, Long chatId, UUID previousId) {
    List<InlineKeyboardButton> buttons = new ArrayList<>();
    int previousMonth = month == FIRST ? LAST_MONTH : month - FIRST;
    int previousYear = month == FIRST ? year - 1 : year;
    int nextMonth = month == LAST_MONTH ? FIRST : month + FIRST;
    int nextYear = month == LAST_MONTH ? year + 1 : year;
    QSJsonDataBean.Builder jsonDataBuilder = QSJsonDataBean.builder()
      .code(jsonData.getCode())
      .userId(chatId)
      .previousId(previousId)
      .data(jsonData.getData());

    String toPreviousId = getJsonId(
      jsonDataBuilder
        .addParam(DATE, LocalDate.of(previousYear, previousMonth, FIRST))
        .build()
    );
    String toNextId = getJsonId(
      jsonDataBuilder
        .addParam(DATE, LocalDate.of(nextYear, nextMonth, FIRST))
        .build()
    );

    buttons.add(getButton(TO_PREVIOUS_ARROW, toPreviousId));
    buttons.add(getEmptyButton(monthDirectoryService.getTitle(month), chatId));
    buttons.add(getButton(TO_NEXT_ARROW, toNextId));

    return buttons;
  }

  private List<InlineKeyboardButton> getYearsButtons(QSJsonData jsonData, Integer year, Integer month, Long chatId, UUID previousId) {
    List<InlineKeyboardButton> buttons = new ArrayList<>();
    QSJsonDataBean.Builder jsonDataBuilder = QSJsonDataBean.builder()
      .code(jsonData.getCode())
      .userId(chatId)
      .previousId(previousId)
      .data(jsonData.getData());

    String toPreviousId = getJsonId(
      jsonDataBuilder
        .addParam(DATE, LocalDate.of(year - 1, month, FIRST))
        .build()
    );
    String toNextId = getJsonId(
      jsonDataBuilder
        .addParam(DATE, LocalDate.of(year + 1, month, FIRST))
        .build()
    );
    buttons.add(getButton(TO_PREVIOUS_ARROW, toPreviousId));
    buttons.add(getEmptyButton(year.toString(), chatId));
    buttons.add(getButton(TO_NEXT_ARROW, toNextId));

    return buttons;
  }

  private InlineKeyboardButton getButton(String title, String jsonId) {
    return InlineKeyboardButton.builder()
      .callbackData(jsonId)
      .text(title)
      .build();
  }

  private String getJsonId(QSJsonData jsonData) {
    return requestDataService.save(jsonData).getId().toString();
  }

  private String getCalendarMessage(String message) {
    return StringUtils.isNotBlank(message) ? CALENDAR_SYMBOL + message : DEFAULT_MESSAGE;
  }

  private String getDayTitle(Pair<LocalDate, LocalDate> highlightingRange, LocalDate day) {
    String dayTitle = String.valueOf(day.getDayOfMonth());

    if (highlightingRange == null || day.isBefore(highlightingRange.getLeft()) || day.isAfter(highlightingRange.getRight())) {
      return dayTitle;
    } else {
      return "-" + dayTitle + "-";
    }

  }

  private LocalDate parseDate(Object date) {
    return converterService.convertTo(LocalDate.class, date);
  }

  private Pair<LocalDate, LocalDate> getHighlightingRange(LocalDate start, LocalDate end) {
    List<LocalDate> range = Stream.of(start, end).filter(Objects::nonNull).sorted().toList();
    int size = range.size();
    if (size == 0) {
      return null;
    } else if (size == 1) {
      LocalDate date = range.iterator().next();
      return Pair.of(date, date);
    } else {
      return Pair.of(range.get(0), range.get(1));
    }

  }

  private Map<String, Object> clearCalendarParams(Map<String, Object> data) {
    Map<String, Object> newData = new HashMap<>(data);
    newData.remove(DATE);
    newData.remove(QS_START_DATE);
    newData.remove(QS_END_DATE);
    newData.remove(EXTERNAL_PREVIOUS_ID);
    return newData;
  }

}