package qs.platform.botapi.content.caches;

import org.springframework.stereotype.Component;
import qs.platform.botapi.content.beans.QSLocation;

@Component
public class QSLocationCache extends QSBaseContentCache<QSLocation> {
  @Override
  public String getCode() {
    return "qsLocationCache";
  }
}
