package qs.platform.botapi.scenarios.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import qs.platform.botapi.beans.QSHandlerScenarioParamBean;
import qs.platform.botapi.beans.QSHandlerScenarioSettingBean;
import qs.platform.botapi.scenarios.QSHandlerScenarioBuilder;
import qs.platform.botapi.scenarios.QSHandlerScenarioService;
import qs.platform.botapi.scenarios.QSHandlerScenarioSetting;
import qs.platform.botapi.scenarios.QSScenarioParam;
import qs.platform.botapi.scenarios.annotations.QSHandlerScenarioParam;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;

@Service
public class QSBaseHandlerScenarioService implements QSHandlerScenarioService {

  private final Collection<QSHandlerScenarioSetting> scenariosSettings;

  @Autowired
  public QSBaseHandlerScenarioService(Collection<QSHandlerScenarioBuilder> handlerScenarios) {
    this.scenariosSettings = convertHandlerScenariosToSettings(handlerScenarios);
  }

  @Override
  public Collection<QSHandlerScenarioSetting> getAll() {
    return scenariosSettings;
  }

  private Collection<QSHandlerScenarioSetting> convertHandlerScenariosToSettings(Collection<QSHandlerScenarioBuilder> handlerScenarios) {
    return handlerScenarios.stream()
      .map(this::getAnnotation)
      .filter(Objects::nonNull)
      .map(this::settingToBean)
      .toList();
  }

  private qs.platform.botapi.scenarios.annotations.QSHandlerScenarioSetting getAnnotation(QSHandlerScenarioBuilder handlerScenario) {
    Class<? extends QSHandlerScenarioBuilder> scenarioClass = handlerScenario.getClass();
    return scenarioClass.getAnnotation(qs.platform.botapi.scenarios.annotations.QSHandlerScenarioSetting.class);
  }

  private QSHandlerScenarioSetting settingToBean(qs.platform.botapi.scenarios.annotations.QSHandlerScenarioSetting setting) {
    return QSHandlerScenarioSettingBean.builder()
      .code(setting.code())
      .title(setting.title())
      .description(setting.description())
      .params(Arrays.stream(setting.params())
        .map(this::paramToBean)
        .toList())
      .build();
  }

  private QSScenarioParam paramToBean(QSHandlerScenarioParam param) {
    return QSHandlerScenarioParamBean.builder()
      .code(param.code())
      .description(param.description())
      .title(param.title())
      .type(param.type())
      .multiple(param.multiple())
      .required(param.required())
      .build();
  }
}
