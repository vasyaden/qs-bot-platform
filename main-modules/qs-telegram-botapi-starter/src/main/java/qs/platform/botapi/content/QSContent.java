package qs.platform.botapi.content;

import java.util.Map;

public interface QSContent {

  String getHandlerCode();

  Map<String, Object> getParams();

  Integer getOrder();
}
