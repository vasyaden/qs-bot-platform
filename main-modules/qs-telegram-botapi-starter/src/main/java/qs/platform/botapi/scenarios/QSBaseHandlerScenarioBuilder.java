package qs.platform.botapi.scenarios;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.telegram.telegrambots.meta.api.methods.PartialBotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import qs.platform.botapi.QSEvent;
import qs.platform.botapi.QSJsonData;
import qs.platform.botapi.QSPaginationService;
import qs.platform.botapi.QSRequestDataService;
import qs.platform.botapi.beans.QSJsonDataBean;
import qs.platform.botapi.content.QSContentDistributor;
import qs.platform.botapi.handlers.QSHandler;
import qs.platform.botapi.scenarios.annotations.QSHandlerScenarioParam;
import qs.platform.botapi.scenarios.annotations.QSHandlerScenarioSetting;
import qs.platform.botapi.stateful.QSUserStateService;
import qs.platform.dbaccess.services.QSQueryService;
import qs.platform.exceptions.QSRuntimeException;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Base implementation of the {@link QSHandlerScenarioBuilder}
 */
public abstract class QSBaseHandlerScenarioBuilder implements QSHandlerScenarioBuilder {
  protected static final String TITLE_CODE = "title";
  protected static final String BACK_TITLE = "Назад";
  protected static final String BACK_MESSAGE = "Возврат к предыдущей странице:";

  private final QSQueryService queryService;
  private final QSRequestDataService requestDataService;
  private final Collection<QSContentDistributor<?>> contentDistributors;
  private final QSUserStateService stateService;
  private final QSPaginationService paginationService;
  private final Collection<String> requiredScenarioParams;

  @Value("${qs.bot.pagination.page-size:5}")
  private Integer pageSize;

  protected QSBaseHandlerScenarioBuilder(QSQueryService queryService, QSRequestDataService requestDataService, QSUserStateService stateService) {
    this(queryService, requestDataService, null, stateService, null);
  }

  protected QSBaseHandlerScenarioBuilder(QSQueryService queryService, QSRequestDataService requestDataService,
                                         Collection<QSContentDistributor<?>> contentDistributors, QSUserStateService stateService) {
    this(queryService, requestDataService, contentDistributors, stateService, null);
  }

  protected QSBaseHandlerScenarioBuilder(QSQueryService queryService, QSRequestDataService requestDataService,
                                         Collection<QSContentDistributor<?>> contentDistributors, QSUserStateService stateService, QSPaginationService paginationService) {
    this.requestDataService = requestDataService;
    this.queryService = queryService;
    this.contentDistributors = contentDistributors == null ?
      Collections.emptyList() : Collections.unmodifiableCollection(new ArrayList<>(contentDistributors));
    this.stateService = stateService;
    this.paginationService = paginationService;
    QSHandlerScenarioSetting scenarioSetting = this.getClass().getAnnotation(QSHandlerScenarioSetting.class);
    this.requiredScenarioParams = scenarioSetting == null ?
      Collections.emptyList() : Arrays.stream(scenarioSetting.params())
      .filter(QSHandlerScenarioParam::required).map(QSHandlerScenarioParam::code)
      .collect(Collectors.toSet());
  }

  protected QSQueryService getQueryService() {
    return queryService;
  }

  protected void checkParams(Map<String, Object> params) {
    Collection<String> notFound = requiredScenarioParams.stream().filter(param -> {
        Object value = params.get(param);
        return value == null || (value instanceof String && StringUtils.isBlank((String) value));
      })
      .collect(Collectors.toSet());
    if (CollectionUtils.isNotEmpty(notFound)) {
      throw new QSRuntimeException("Not found required params for scenario '" + getCode() + "': " + notFound);
    }
  }

  protected EditMessageText initReplyEditMessage(QSEvent event, String message,
                                                 InlineKeyboardMarkup keyboardMarkup) {
    Long chatId = event.getChatId();
    Integer messageId = event.getMessageId();
    return EditMessageText.builder()
      .chatId(String.valueOf(chatId))
      .messageId(messageId)
      .text(message)
      .replyMarkup(keyboardMarkup)
      .build();
  }

  protected EditMessageText initReplyEditMessage(QSEvent event, String message) {
    Long chatId = event.getChatId();
    Integer messageId = event.getMessageId();
    return EditMessageText.builder()
      .chatId(String.valueOf(chatId))
      .messageId(messageId)
      .text(message)
      .build();
  }

  protected Collection<List<InlineKeyboardButton>> initButtons(QSHandler handler, QSJsonData jsonData) {
    Collection<List<InlineKeyboardButton>> buttons = initChildButtons(handler, jsonData);
    UUID previousId = getPreviousId(handler, jsonData);
    if (CollectionUtils.isNotEmpty(handler.getChildren()) && previousId != null) {
      buttons.add(initBackButtons(previousId));
    }
    return buttons;
  }

  protected Collection<List<InlineKeyboardButton>> initChildButtons(QSHandler handler, QSJsonData previous) {
    List<List<InlineKeyboardButton>> buttons = new ArrayList<>();
    handler.getChildren().forEach(child -> {
      Collection<Map<String, Object>> dataMaps =
        child.getDataQuery().map(dataQuery -> getData(dataQuery, previous.getData()))
          .orElse(List.of(new HashMap<>()));

      for (Map<String, Object> dataMap : dataMaps) {
        String buttonTitle = (String) dataMap.remove(TITLE_CODE);
        if (StringUtils.isBlank(buttonTitle)) {
          buttonTitle = child.getTitle();
        }

        QSJsonData qsJsonData = QSJsonDataBean.builder()
          .code(child.getCode())
          .userId(previous.getUserId())
          .data(dataMap)
          .previousId(previous.getId())
          .unique(child.isUnique())
          .build();

        if (!child.check(qsJsonData)) {
          break;
        }

        QSJsonData savedData = requestDataService.save(qsJsonData);
        InlineKeyboardButton button = InlineKeyboardButton.builder()
          .text(buttonTitle)
          .callbackData(savedData.getId().toString())
          .build();
        buttons.add(Collections.singletonList(button));
      }
    });
    return paginateChildren(previous, buttons);
  }

  private List<List<InlineKeyboardButton>> paginateChildren(QSJsonData jsonData, List<List<InlineKeyboardButton>> buttons) {
    if (paginationService == null) {
      return buttons;
    }
    int currentPage = paginationService.getCurrentPageNumber(jsonData);
    int size = buttons.size();
    int totalPages = size % pageSize == 0 ? size / pageSize : size / pageSize + 1;
    if (totalPages == 1) {
      return buttons;
    }
    List<InlineKeyboardButton> paginationButtons = paginationService.getButtons(jsonData.getCode(), totalPages, currentPage, jsonData.getUserId(), jsonData.getPreviousId(), jsonData.getData());
    int startIndex = currentPage * pageSize;
    int toIndex = startIndex + pageSize;
    List<List<InlineKeyboardButton>> page = buttons.subList(startIndex, Math.min(toIndex, size));
    page.add(paginationButtons);
    return page;
  }

  protected List<InlineKeyboardButton> initBackButtons(UUID previousId) {
    List<InlineKeyboardButton> buttons = new ArrayList<>();
    InlineKeyboardButton button = InlineKeyboardButton.builder()
      .text(BACK_TITLE)
      .callbackData(previousId.toString())
      .build();
    buttons.add(button);
    return buttons;
  }

  private Collection<Map<String, Object>> getData(String query, Map<String, Object> params) {
    return queryService.getData(query, params);
  }

  /**
   * Checks if handler has children
   *
   * @param handler handler
   */
  protected boolean isMenuHandler(QSHandler handler) {
    return CollectionUtils.isNotEmpty(handler.getChildren());
  }

  protected Collection<PartialBotApiMethod<?>> getContent(QSHandler handler, QSEvent event, String responseMessage, QSJsonData jsonData) {

    Collection<PartialBotApiMethod<?>> content = new ArrayList<>();
    if (event.hasMessage()) {
      content.add(
        SendMessage.builder()
          .chatId(event.getChatIdString())
          .text(responseMessage)
          .build()
      );
    } else if (event.hasCallbackQuery()) {
      content.add(
        initReplyEditMessage(event, responseMessage)
      );
    }
    contentDistributors.forEach(distributor -> content.addAll(distributor.getContent(jsonData)));
    UUID previousId = getPreviousId(handler, jsonData);
    if (previousId != null) {
      content.add(
        SendMessage.builder()
          .chatId(event.getChatIdString())
          .text(BACK_MESSAGE)
          .replyMarkup(
            InlineKeyboardMarkup.builder()
              .keyboard(Collections.singletonList(initBackButtons(previousId)))
              .build()
          )
          .build()
      );
    }
    return content;
  }

  protected UUID getPreviousId(QSHandler handler, QSJsonData jsonData) {
    UUID previousId = jsonData.getPreviousId();
    if (previousId == null && handler.getParentCode() != null) {
      QSJsonData startData = QSJsonDataBean.builder()
        .code(handler.getParentCode())
        .userId(jsonData.getUserId())
        .data(jsonData.getData())
        .build();
      previousId = requestDataService.save(startData).getId();
    }
    return previousId;
  }

  /**
   * put the user to state
   *
   * @param state     code of the state
   * @param stateData {@link QSJsonData data} of the state
   */
  protected void toState(String state, QSJsonData stateData) {
    if (state != null) {
      stateService.toState(state, stateData);
    } else {
      stateService.delete(stateData.getUserId());
    }
  }
}
