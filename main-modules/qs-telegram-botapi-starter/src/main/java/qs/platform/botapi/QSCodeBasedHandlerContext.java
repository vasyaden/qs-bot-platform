package qs.platform.botapi;

import org.telegram.telegrambots.meta.api.methods.PartialBotApiMethod;
import qs.platform.botapi.handlers.QSHandler;

import java.util.Collection;

/**
 * Handler's context that chooses {@link QSHandler handler} based on given handler's code
 */
public interface QSCodeBasedHandlerContext {

  /**
   * Process {@link QSEvent event} using {@link QSHandler handler} with given code
   *
   * @return collection of responses
   */
  Collection<PartialBotApiMethod<?>> handle(QSEvent event, String handlerCode);

  /**
   * Process {@link QSEvent event} using {@link QSJsonData jsonData} with given code
   *
   * @return collection of responses
   */
  Collection<PartialBotApiMethod<?>> handle(QSEvent event, QSJsonData jsonData);

}
