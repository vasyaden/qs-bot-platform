package qs.platform.botapi.scenarios;

import java.util.Collection;

/**
 * The interface defines methods for interacting with a collection of {@link qs.platform.botapi.scenarios.annotations.QSHandlerScenarioSetting}
 */
public interface QSHandlerScenarioService {
  /**
   * @return all {@link qs.platform.botapi.scenarios.annotations.QSHandlerScenarioSetting}
   */
  Collection<QSHandlerScenarioSetting> getAll();
}
