package qs.platform.botapi.content.distributors;

import org.springframework.beans.factory.annotation.Autowired;
import org.telegram.telegrambots.meta.api.methods.PartialBotApiMethod;
import qs.platform.botapi.QSJsonData;
import qs.platform.botapi.content.QSContent;
import qs.platform.botapi.content.QSContentCache;
import qs.platform.botapi.content.QSContentDistributor;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Stream;

public abstract class QSAbstractContentDistributor<C extends QSContent> implements QSContentDistributor<C> {

  private QSContentCache<C> cache;

  @Autowired
  private void setCache(QSContentCache<C> cache) {
    this.cache = cache;
  }

  public Collection<PartialBotApiMethod<?>> getContent(QSJsonData jsonData) {
    Stream<C> filteredStream = cache.getContent(jsonData.getCode())
      .orElse(Collections.emptyList())
      .stream()
      .filter(c -> checkParams(jsonData.getData(), c.getParams()));
    return getContent(filteredStream, jsonData.getUserId().toString());
  }

  /**
   * Проверят параметры
   * @param params Параметры
   * @param requiredParams Требуемые параметры
   * @return Входят ли requiredParams в params
   */
  public boolean checkParams(Map<String, Object> params, Map<String, Object> requiredParams) {
    return params.entrySet().containsAll(requiredParams.entrySet());
  }
}
