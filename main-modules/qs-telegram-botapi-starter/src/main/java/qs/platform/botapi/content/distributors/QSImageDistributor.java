package qs.platform.botapi.content.distributors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.PartialBotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import qs.platform.botapi.content.beans.QSImage;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class QSImageDistributor extends QSAbstractContentDistributor<QSImage> {
  private static final Logger LOG = LoggerFactory.getLogger(QSImageDistributor.class);

  @Override
  public Collection<PartialBotApiMethod<?>> getContent(Stream<QSImage> content, String chatId) {
    return content
      .map(c -> getSendPhoto(c.getInputFile(), chatId, c.getCaption()))
      .collect(Collectors.toList());
  }

  private SendPhoto getSendPhoto(InputFile file, String chatId, String caption) {
    SendPhoto.SendPhotoBuilder photo = SendPhoto.builder()
      .chatId(chatId)
      .photo(file);
    if (StringUtils.isNotBlank(caption)) {
      photo.caption(caption);
    }
    return photo.build();
  }
}
