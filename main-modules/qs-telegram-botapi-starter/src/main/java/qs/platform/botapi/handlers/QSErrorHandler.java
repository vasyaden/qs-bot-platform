package qs.platform.botapi.handlers;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.PartialBotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import qs.platform.botapi.QSEvent;
import qs.platform.exceptions.QSAppRuntimeException;
import qs.platform.exceptions.QSHandlerNotFoundException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

@Service
public class QSErrorHandler {
  private static final Logger LOG = LoggerFactory.getLogger(QSErrorHandler.class);
  private static final String HANDLER_NOT_FOUND_MESSAGE = "Неизвестная команда\uD83D\uDE14 \nЧтобы воспользоваться главным меню, нажмите /start";
  private static final String UNKNOWN_SERVER_ERROR_USER_MESSAGE = "Произошла непредвиденная ошибка. Мы уже её чиним! \nЧтобы воспользоваться главным меню, нажмите /start";
  private static final String UNKNOWN_SERVER_ERROR_ADMIN_MESSAGE = "В боте @%s произошла ошибка: %s";
  //TODO: make this messages @Value
  @Value("${qs.bot.admin-id:}")
  private Collection<String> adminIds;

  @Value("${qs.bot.username}")
  private String botName;

  public Collection<PartialBotApiMethod<?>> handle(QSEvent event, Exception e) {
    LOG.error(e.getMessage(), e);

    if (e instanceof QSHandlerNotFoundException) {
      return Collections.singletonList(
        SendMessage.builder()
          .chatId(event.getChatIdString())
          .text(HANDLER_NOT_FOUND_MESSAGE)
          .build()
      );
    } else if (e instanceof QSAppRuntimeException) {
      return Collections.singletonList(
        SendMessage.builder()
          .chatId(event.getChatIdString())
          .text(e.getMessage())
          .build()
      );
    } else {
      Collection<PartialBotApiMethod<?>> responses = new ArrayList<>();
      if (CollectionUtils.isNotEmpty(adminIds)) {
        SendMessage.SendMessageBuilder builder = SendMessage.builder()
          .text(String.format(UNKNOWN_SERVER_ERROR_ADMIN_MESSAGE, botName, e.getMessage()));
        for (String id : adminIds) {
          responses.add(builder
            .chatId(id)
            .build());
        }
      }
      responses.add(
        SendMessage.builder()
          .chatId(event.getChatIdString())
          .text(UNKNOWN_SERVER_ERROR_USER_MESSAGE)
          .build()
      );
      return responses;
    }
  }

}
