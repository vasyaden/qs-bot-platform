package qs.platform.botapi;

public interface QSCache<K, V> {
  void refresh();

  String getCode();

  V get(K key);
}
