package qs.platform.botapi.contexts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.PartialBotApiMethod;
import qs.platform.botapi.*;
import qs.platform.botapi.handlers.QSHandler;
import qs.platform.botapi.stateful.QSUserState;
import qs.platform.botapi.stateful.QSUserStateService;
import qs.platform.exceptions.QSHandlerForbiddenException;
import qs.platform.exceptions.QSHandlerNotFoundException;

import jakarta.annotation.Priority;
import java.util.Collection;
import java.util.Optional;

/**
 * Handler's context that chooses {@link QSHandler handler} based on user state {@link QSUserState}
 */
@Component
@Priority(QSHandlerContextChain.Orders.STATE)
public class QSStateBasedHandlerContext extends QSHandlerContextBaseChain implements QSHandlerContext {
  private static final Logger LOG = LoggerFactory.getLogger(QSStateBasedHandlerContext.class);
  private QSHandlerCache handlerCache;
  private QSUserStateService stateService;
  private QSRequestDataService requestDataService;

  @Autowired
  private void setHandlerCache(QSHandlerCache handlerCache) {
    this.handlerCache = handlerCache;
  }

  @Autowired
  private void setRequestDataService(QSRequestDataService requestDataService) {
    this.requestDataService = requestDataService;
  }

  @Autowired
  private void setStateService(QSUserStateService stateService) {
    this.stateService = stateService;
  }

  /**
   * Process {@link QSEvent income event}
   *
   * @param event {@link QSEvent income event} to be processed
   * @return collection of responses
   */
  @Override
  public Collection<PartialBotApiMethod<?>> processUpdate(QSEvent event) {
    Long telegramId = event.getChatId();
    QSUserState state = Optional.ofNullable(stateService.get(telegramId))
      .orElseThrow(() -> new QSHandlerNotFoundException(telegramId.toString(), "Not found state for user with telegramId = " + telegramId));

    QSHandler handler = findHandler(state);

    return handle(event, handler, state.getStateData());
  }

  /**
   * @param state {@link QSUserState}
   * @return handler {@link QSHandler}
   */
  private QSHandler findHandler(QSUserState state) {
    return Optional.ofNullable(handlerCache.getHandler(state.getStateCode()))
      .orElseThrow(() -> new QSHandlerNotFoundException(state.getStateCode(), "Not found Handler with code = " + state.getStateCode()));
  }

  /**
   * @param event    {@link QSEvent}
   * @param handler  {@link QSHandler}
   * @param jsonData {@link QSJsonData}
   * @return collection of responses
   */
  private Collection<PartialBotApiMethod<?>> handle(QSEvent event, QSHandler handler, QSJsonData jsonData) {
    if (handler.check(jsonData)) {
      jsonData = requestDataService.save(jsonData);
      return handler.handle(event, jsonData);
    } else {
      throw new QSHandlerForbiddenException();
    }
  }

}
