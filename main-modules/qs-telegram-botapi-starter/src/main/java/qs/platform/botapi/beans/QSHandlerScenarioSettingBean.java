package qs.platform.botapi.beans;

import org.springframework.util.CollectionUtils;
import qs.platform.botapi.scenarios.QSHandlerScenarioSetting;
import qs.platform.botapi.scenarios.QSScenarioParam;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class QSHandlerScenarioSettingBean implements QSHandlerScenarioSetting {

  /**
   * @see qs.platform.botapi.scenarios.annotations.QSHandlerScenarioSetting#code()
   */
  private String code;

  /**
   * @see qs.platform.botapi.scenarios.annotations.QSHandlerScenarioSetting#title()
   */
  private String title;

  /**
   * @see qs.platform.botapi.scenarios.annotations.QSHandlerScenarioSetting#description()
   */
  private String description;

  /**
   * @see qs.platform.botapi.scenarios.annotations.QSHandlerScenarioSetting#params()
   */
  private Collection<QSScenarioParam> params;

  public QSHandlerScenarioSettingBean() {
  }

  public static QSHandlerScenarioSettingBean.Builder builder() {
    return new QSHandlerScenarioSettingBean.Builder();
  }

  private QSHandlerScenarioSettingBean(String code, String title, String description, Collection<QSScenarioParam> params) {
    this.code = code;
    this.title = title;
    this.description = description;
    this.params = CollectionUtils.isEmpty(params) ?
      Collections.emptyList() : Collections.unmodifiableCollection(new ArrayList<>(params));
  }

  public String getCode() {
    return code;
  }

  public String getTitle() {
    return title;
  }

  public String getDescription() {
    return description;
  }

  public Collection<QSScenarioParam> getParams() {
    return params;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setParams(Collection<QSScenarioParam> params) {
    this.params = params;
  }

  public static final class Builder {
    private String code;
    private String title;
    private String description;
    private Collection<QSScenarioParam> params = Collections.emptyList();

    private Builder() {
    }

    public QSHandlerScenarioSettingBean.Builder code(String code) {
      this.code = code;
      return this;
    }

    public QSHandlerScenarioSettingBean.Builder title(String title) {
      this.title = title;
      return this;
    }

    public QSHandlerScenarioSettingBean.Builder description(String description) {
      this.description = description;
      return this;
    }

    public QSHandlerScenarioSettingBean.Builder params(Collection<QSScenarioParam> params) {
      this.params = params;
      return this;
    }

    public QSHandlerScenarioSettingBean build() {
      return new QSHandlerScenarioSettingBean(code, title, description, params);
    }
  }
}
