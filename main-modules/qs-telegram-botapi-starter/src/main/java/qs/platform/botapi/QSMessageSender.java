package qs.platform.botapi;

import java.util.Collection;

public interface QSMessageSender {

  void send(Collection<QSSendMessage> messages);

  Integer send(QSSendMessage message);

  void pinChatMessage(Long chatId, Integer messageId);

  void unpinChatMessage(Long chatId, Integer messageId);
}
