package qs.platform.botapi;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * service providing a list of buttons with numbers of pages for paginating
 */
public interface QSPaginationService {


  /**
   * @param handlerCode code of handler
   * @param totalPages  total number of pages with objects
   * @param currentPage current page number
   * @param chatId      telegram-chat ID
   * @param previousId  ID of the previous json
   * @param data        necessary information for transit to subsequent handlers
   * @return a collection of page-numbered handler buttons
   */
  List<InlineKeyboardButton> getButtons(String handlerCode,
                                        int totalPages,
                                        int currentPage,
                                        Long chatId,
                                        UUID previousId,
                                        Map<String, Object> data);

  /**
   * @param handlerCode code of handler
   * @param totalPages  total number of pages with objects
   * @param currentPage current page number
   * @param chatId      telegram-chat ID
   * @param previousId  ID of the previous json
   * @return a collection of page-numbered handler buttons
   */
  List<InlineKeyboardButton> getButtons(String handlerCode,
                                        int totalPages,
                                        int currentPage,
                                        Long chatId,
                                        UUID previousId);

  /**
   * Extract current page number from {@link QSJsonData}
   *
   * @param jsonData handler data
   * @return current page number
   */
  int getCurrentPageNumber(QSJsonData jsonData);
}
