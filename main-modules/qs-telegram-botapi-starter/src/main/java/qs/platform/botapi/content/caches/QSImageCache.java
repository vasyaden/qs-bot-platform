package qs.platform.botapi.content.caches;

import org.springframework.stereotype.Component;
import qs.platform.botapi.content.beans.QSImage;

@Component
public class QSImageCache extends QSBaseContentCache<QSImage> {
  @Override
  public String getCode() {
    return "qsImageCache";
  }
}
