package qs.platform.botapi.beans;

import qs.platform.botapi.QSJsonData;
import qs.platform.botapi.stateful.QSUserState;

import java.time.LocalDateTime;

public class QSUserStateBean implements QSUserState {
  private final Long telegramId;
  private final String stateCode;
  private final LocalDateTime stateLifeTime;
  private final QSJsonData stateData;

  private QSUserStateBean(Long telegramId, String stateCode, QSJsonData stateData, LocalDateTime stateLifeTime) {
    this.telegramId = telegramId;
    this.stateCode = stateCode;
    this.stateData = stateData;
    this.stateLifeTime = stateLifeTime;
  }

  public LocalDateTime getStateLifeTime() {
    return stateLifeTime;
  }

  @Override
  public boolean isExpired() {
    return LocalDateTime.now().isAfter(stateLifeTime);
  }

  public Long getTelegramId() {
    return telegramId;
  }

  public String getStateCode() {
    return stateCode;
  }

  public QSJsonData getStateData() {
    return stateData;
  }

  public static Builder builder() {
    return new Builder();
  }

  public static final class Builder {
    private Long telegramId;
    private String stateCode;
    private LocalDateTime stateLifeTime;
    private QSJsonData stateData;

    public Builder telegramId(Long telegramId) {
      this.telegramId = telegramId;
      return this;
    }

    public Builder stateCode(String stateCode) {
      this.stateCode = stateCode;
      return this;
    }

    public Builder stateData(QSJsonData stateData) {
      this.stateData = stateData;
      return this;
    }

    public Builder stateLifeTime(LocalDateTime stateLifeTime) {
      this.stateLifeTime = stateLifeTime;
      return this;
    }

    public QSUserStateBean build() {
      return new QSUserStateBean(telegramId, stateCode, stateData, stateLifeTime);
    }
  }
}
