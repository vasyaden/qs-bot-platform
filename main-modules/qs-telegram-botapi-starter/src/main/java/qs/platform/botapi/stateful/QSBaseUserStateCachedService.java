package qs.platform.botapi.stateful;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import qs.platform.botapi.QSJsonData;
import qs.platform.botapi.beans.QSJsonDataBean;
import qs.platform.botapi.beans.QSUserStateBean;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Cache of {@link QSUserState UserState}
 */
@Service
@EnableScheduling
public class QSBaseUserStateCachedService implements QSUserStateService {
  private final Map<Long, QSUserState> states = new ConcurrentHashMap<>();
  @Value("${qs.bot.stateful.state-life-time}")
  private Long stateLifeTime;

  @Scheduled(fixedDelayString = "${qs.bot.stateful.clear-delay}")
  private void clear() {
    if (!states.isEmpty()) {
      Map<Long, QSUserState> statesCopy = new HashMap<>(states);
      statesCopy.forEach((key, value) -> {
        if (value.isExpired()) {
          states.remove(key, value);
        }
      });
    }
  }

  @Override
  public QSUserState get(Long key) {
    return states.get(key);
  }

  @Override
  public void delete(Long telegramId) {
    states.remove(telegramId);
  }

  @Override
  public void toState(String stateCode, QSJsonData stateData) {
    Long telegramId = stateData.getUserId();
    QSJsonData jsonData = QSJsonDataBean.builder()
      .userId(telegramId)
      .data(stateData.getData())
      .code(stateCode)
      .previousId(stateData.getPreviousId())
      .build();
    states.put(telegramId, QSUserStateBean.builder()
      .telegramId(telegramId)
      .stateCode(stateCode)
      .stateData(jsonData)
      .stateLifeTime(LocalDateTime.now().plusMinutes(stateLifeTime))
      .build());
  }
}

