package qs.platform.botapi.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import qs.platform.botapi.QSCache;
import qs.platform.botapi.QSCacheManagementService;
import qs.platform.exceptions.QSNotFoundException;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class QSBaseCacheManagementService implements QSCacheManagementService {
  private Map<String, QSCache<?, ?>> caches;

  @Autowired
  private void setCaches(Collection<QSCache<?, ?>> caches) {
    this.caches = caches.stream().collect(Collectors.toMap(QSCache::getCode, Function.identity()));
  }

  @Override
  public void refresh() {
    caches.values().forEach(QSCache::refresh);
  }

  @Override
  public void refresh(String code) {
    QSCache<?, ?> cache = caches.get(code);
    if (cache != null) {
      cache.refresh();
    } else {
      throw new QSNotFoundException("cache.not.found", "Не найден кэш с кодом " + code);
    }
  }
}
