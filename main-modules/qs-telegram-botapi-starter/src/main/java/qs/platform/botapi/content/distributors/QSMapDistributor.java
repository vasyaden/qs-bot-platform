package qs.platform.botapi.content.distributors;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.PartialBotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendLocation;
import qs.platform.botapi.content.beans.QSLocation;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class QSMapDistributor extends QSAbstractContentDistributor<QSLocation> {

  @Override
  public Collection<PartialBotApiMethod<?>> getContent(Stream<QSLocation> content, String chatId) {
    return content.map(c -> getSendDocument(c, chatId))
      .collect(Collectors.toList());
  }

  private SendLocation getSendDocument(QSLocation location, String chatId) {
    return SendLocation.builder()
      .chatId(chatId)
      .latitude(location.getLatitude())
      .longitude(location.getLongitude())
      .build();
  }

}
