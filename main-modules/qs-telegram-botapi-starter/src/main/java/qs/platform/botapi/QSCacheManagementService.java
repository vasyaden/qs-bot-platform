package qs.platform.botapi;

public interface QSCacheManagementService {
  void refresh();

  void refresh(String code);
}
