package qs.platform.botapi;

import qs.platform.botapi.handlers.QSHandler;

import java.util.Map;

/**
 * Factory of the {@link QSHandler handlers}
 */
public interface QSHandlersFactory {
  /**
   * Collect all {@link QSHandler handlers} return them
   *
   * @return map of {@link QSHandler handlers} by {@link QSHandler#getCode() code}
   */
  Map<String, QSHandler> getHandlers();
}
