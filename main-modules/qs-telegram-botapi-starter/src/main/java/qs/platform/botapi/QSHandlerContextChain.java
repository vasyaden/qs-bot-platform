package qs.platform.botapi;

import org.telegram.telegrambots.meta.api.methods.PartialBotApiMethod;

import java.util.Collection;

/**
 * Chain of handlers' contexts
 */
public interface QSHandlerContextChain {

  /**
   * Add next element to the end chain
   *
   * @param last new last element of chain
   */
  void addLast(QSHandlerContextChain last);

  /**
   * Tries to get response from next element of chain
   */
  Collection<PartialBotApiMethod<?>> tryNext(QSEvent event);

  /**
   * Order numbers of the contexts
   */
  interface Orders {
    /**
     * Order of the main handlers' context
     */
    int MAIN = 0;
    /**
     * Order of the state based handlers' context
     */
    int STATE = 100;
  }
}
