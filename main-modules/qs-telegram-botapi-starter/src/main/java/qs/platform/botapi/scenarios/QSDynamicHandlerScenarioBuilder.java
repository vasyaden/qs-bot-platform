package qs.platform.botapi.scenarios;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import qs.platform.botapi.QSPaginationService;
import qs.platform.botapi.QSRequestDataService;
import qs.platform.botapi.content.QSContentDistributor;
import qs.platform.botapi.scenarios.annotations.QSHandlerScenarioParam;
import qs.platform.botapi.scenarios.annotations.QSHandlerScenarioSetting;
import qs.platform.botapi.stateful.QSUserStateService;
import qs.platform.dbaccess.services.QSQueryService;
import qs.platform.exceptions.QSRuntimeException;
import qs.platform.types.QSType;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

@QSHandlerScenarioSetting(
  code = QSDynamicHandlerScenarioBuilder.CODE,
  title = "Simple menu",
  description = "Menu with a buttons of the child-handlers",
  params = {
    @QSHandlerScenarioParam(
      code = QSDynamicHandlerScenarioBuilder.MESSAGE,
      title = "Message",
      description = "Static message will be sent to user as result of the handler scenario",
      type = QSType.STRING
    ),
    @QSHandlerScenarioParam(
      code = QSDynamicHandlerScenarioBuilder.MESSAGE_QUERY,
      title = "Message query",
      description = """
        SQL query that should return one string field, that will be sent to user as result of the handler scenario\s
        instead of the static one
        """,
      type = QSType.STRING,
      required = false
    ),
    @QSHandlerScenarioParam(
      code = QSDynamicHandlerScenarioBuilder.TO_STATE,
      title = "State",
      description = """
        Code of the state that should be set to user as result of the handler
        """,
      type = QSType.STRING,
      required = false
    )
  }
)
/**
 * Dynamic handler
 */
public class QSDynamicHandlerScenarioBuilder extends QSBaseHandlerScenarioBuilder {

  protected static final String CODE = "qsSimpleMenuScenario";
  protected static final String MESSAGE = "message";
  protected static final String MESSAGE_QUERY = "messageQuery";
  protected static final String TO_STATE = "toState";

  @Autowired
  private QSDynamicHandlerScenarioBuilder(QSQueryService queryService, QSRequestDataService requestDataService, Collection<QSContentDistributor<?>> contentDistributors, QSUserStateService stateService, QSPaginationService paginationService) {
    super(queryService, requestDataService, contentDistributors, stateService, paginationService);
  }

  @Override
  public QSHandlerScenario build(Map<String, Object> params) {
    checkParams(params);

    final String message = (String) params.get(MESSAGE);
    final String messageQuery = (String) params.get(MESSAGE_QUERY);
    final String toState = (String) params.get(TO_STATE);

    return (handler, event, jsonData) -> {
      String responseMessage = message;

      if (StringUtils.isNotBlank(messageQuery)) {
        String dynamicMessage = getQueryService().getMessage(messageQuery, jsonData.getData());
        if (StringUtils.isNotBlank(dynamicMessage)) {
          responseMessage = dynamicMessage;
        }
      }
      toState(toState, jsonData);
      if (isMenuHandler(handler)) {
        InlineKeyboardMarkup keyboard = InlineKeyboardMarkup.builder()
          .keyboard(initButtons(handler, jsonData))
          .build();
        if (event.hasMessage()) {
          return Collections.singletonList(
            SendMessage.builder()
              .chatId(event.getChatIdString())
              .text(responseMessage)
              .replyMarkup(keyboard)
              .build()
          );
        } else if (event.hasCallbackQuery()) {
          return Collections.singletonList(
            initReplyEditMessage(
              event, responseMessage, keyboard
            )
          );
        } else {
          throw new QSRuntimeException("unknown update type: " + event);
        }
      } else {
        return getContent(handler, event, responseMessage, jsonData);
      }
    };
  }

}
