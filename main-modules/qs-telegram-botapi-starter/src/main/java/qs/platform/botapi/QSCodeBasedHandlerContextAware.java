package qs.platform.botapi;

public interface QSCodeBasedHandlerContextAware {

  void setCodeBasedHandlerContext(QSCodeBasedHandlerContext context);

}
