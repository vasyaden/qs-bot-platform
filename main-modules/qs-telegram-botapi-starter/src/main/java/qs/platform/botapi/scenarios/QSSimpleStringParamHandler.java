package qs.platform.botapi.scenarios;

import org.springframework.beans.factory.annotation.Autowired;
import qs.platform.botapi.QSEvent;
import qs.platform.botapi.QSRequestDataService;
import qs.platform.botapi.scenarios.annotations.QSHandlerScenarioParam;
import qs.platform.botapi.scenarios.annotations.QSHandlerScenarioSetting;
import qs.platform.botapi.stateful.QSUserStateService;
import qs.platform.dbaccess.services.QSQueryService;
import qs.platform.types.QSType;

/**
 * Handles simple string message
 */
@QSHandlerScenarioSetting(
  code = QSSimpleStringParamHandler.CODE,
  title = "Simple string handler",
  description = "Simple string handler scenario: get text of the message and put it to output parameter",
  params = {
    @QSHandlerScenarioParam(
      code = QSParamFromEventHandler.ERROR_MESSAGE,
      title = "Error message",
      description = "Message will be sent if result is invalid",
      type = QSType.STRING
    ),
    @QSHandlerScenarioParam(
      code = QSParamFromEventHandler.NEXT_HANDLER,
      title = "Next handler code",
      description = "Code of the handler, which should be invoked as result with extracted output parameter",
      type = QSType.STRING
    ),
    @QSHandlerScenarioParam(
      code = QSParamFromEventHandler.OUTPUT_PARAM,
      title = "Output parameter",
      description = "Parameter where extracted text will be placed",
      type = QSType.STRING
    )
  }
)
public class QSSimpleStringParamHandler extends QSParamFromEventHandler<String> {
  public static final String CODE = "qsSimpleStringFromMessageTextScenario";

  @Autowired
  private QSSimpleStringParamHandler(QSQueryService queryService, QSRequestDataService requestDataService, QSUserStateService stateService) {
    super(queryService, requestDataService, stateService);
  }

  @Override
  protected String getValue(QSEvent event) {
    return event.getMessageText();
  }

  @Override
  protected String getInvalidValue(QSEvent event) {
    return event.getMessageText();
  }
}
