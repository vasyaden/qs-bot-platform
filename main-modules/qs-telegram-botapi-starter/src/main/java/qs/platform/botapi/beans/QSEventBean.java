package qs.platform.botapi.beans;

import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Contact;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import qs.platform.botapi.QSEvent;
import qs.platform.botapi.QSEventType;
import qs.platform.botapi.QSUser;
import qs.platform.exceptions.QSRuntimeException;

import java.util.Optional;

public class QSEventBean implements QSEvent {
  private final Update update;
  private final Long chatId;
  private final String chatIdString;
  private final String messageText;
  private final String replyToMessageText;
  private final String callbackData;
  private final Integer messageId;
  private final QSEventType type;
  private final QSUser user;

  private QSEventBean(Update update) {
    this.update = update;
    this.type = QSEventType.from(update);
    Message message;
    if (type == QSEventType.MESSAGE) {
      message = update.getMessage();
      this.callbackData = null;
    } else if (type == QSEventType.CALLBACK) {
      CallbackQuery callbackQuery = update.getCallbackQuery();
      message = callbackQuery.getMessage();
      this.callbackData = callbackQuery.getData();
    } else {
      throw new QSRuntimeException("Unsupported update type: " + type.name());
    }
    this.chatId = message.getChatId();
    this.messageText = message.getText();
    this.messageId = message.getMessageId();
    this.replyToMessageText = Optional.ofNullable(message.getReplyToMessage()).map(Message::getText).orElse(null);
    this.user = QSUserBean.from(message.getFrom());
    this.chatIdString = String.valueOf(chatId);
  }

  private QSEventBean(Update update, Long chatId, String chatIdString, String messageText, String replyToMessageText, String callbackData, Integer messageId, QSEventType type, QSUser user) {
    this.update = update;
    this.chatId = chatId;
    this.chatIdString = chatIdString;
    this.messageText = messageText;
    this.replyToMessageText = replyToMessageText;
    this.callbackData = callbackData;
    this.messageId = messageId;
    this.type = type;
    this.user = user;
  }

  public static QSEvent from(Update update) {
    return new QSEventBean(update);
  }

  public static Builder builder(QSEvent event) {
    return new Builder()
      .update(event.getUpdate())
      .chatId(event.getChatId())
      .chatIdString(event.getChatIdString())
      .messageText(event.getMessageText())
      .replyToMessageText(event.getReplyToMessageText())
      .callbackData(event.getCallbackData())
      .messageId(event.getMessageId())
      .type(event.getType())
      .user(event.getUser());
  }

  public static Builder builder() {
    return new Builder();
  }

  @Override
  public Long getChatId() {
    return chatId;
  }

  @Override
  public String getChatIdString() {
    return chatIdString;
  }

  @Override
  public QSEventType getType() {
    return type;
  }

  @Override
  public boolean hasMessage() {
    return update.hasMessage();
  }

  @Override
  public boolean hasCallbackQuery() {
    return update.hasCallbackQuery();
  }

  @Override
  public boolean isType(QSEventType type) {
    return this.type == type;
  }

  @Override
  public String getMessageText() {
    return messageText;
  }

  @Override
  public String getReplyToMessageText() {
    return replyToMessageText;
  }

  @Override
  public String getCallbackData() {
    return callbackData;
  }

  @Override
  public Contact getContact() {
    if (update.hasMessage()) {
      return update.getMessage().getContact();
    } else {
      return null;
    }
  }

  @Override
  public Integer getMessageId() {
    return messageId;
  }

  @Override
  public Update getUpdate() {
    return update;
  }

  @Override
  public QSUser getUser() {
    return user;
  }

  @Override
  public String toString() {
    return update.toString();
  }

  public static final class Builder {
    private Update update;
    private Long chatId;
    private String chatIdString;
    private String messageText;
    private String replyToMessageText;
    private String callbackData;
    private Integer messageId;
    private QSEventType type;
    private QSUser user;

    public Builder update(Update update) {
      this.update = update;
      return this;
    }

    public Builder chatId(Long chatId) {
      this.chatId = chatId;
      return this;
    }

    public Builder chatIdString(String chatIdString) {
      this.chatIdString = chatIdString;
      return this;
    }

    public Builder messageText(String messageText) {
      this.messageText = messageText;
      return this;
    }

    public Builder replyToMessageText(String replyToMessageText) {
      this.replyToMessageText = replyToMessageText;
      return this;
    }

    public Builder callbackData(String callbackData) {
      this.callbackData = callbackData;
      return this;
    }

    public Builder messageId(Integer messageId) {
      this.messageId = messageId;
      return this;
    }

    public Builder type(QSEventType type) {
      this.type = type;
      return this;
    }

    public Builder user(QSUser user) {
      this.user = user;
      return this;
    }

    public QSEvent build() {
      return new QSEventBean(update, chatId, chatIdString, messageText, replyToMessageText, callbackData, messageId, type, user);
    }
  }
}
