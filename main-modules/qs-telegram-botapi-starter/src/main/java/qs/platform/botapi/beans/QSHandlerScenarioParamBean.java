package qs.platform.botapi.beans;

import qs.platform.botapi.scenarios.QSScenarioParam;
import qs.platform.botapi.scenarios.annotations.QSHandlerScenarioParam;
import qs.platform.types.QSType;

/**
 * bean of the {@link QSHandlerScenarioParam}
 */
public class QSHandlerScenarioParamBean implements QSScenarioParam {

  /**
   * @see QSHandlerScenarioParam#code()
   */
  private String code;

  /**
   * @see QSHandlerScenarioParam#title()
   */
  private String title;

  /**
   * @see QSHandlerScenarioParam#description()
   */
  private String description;

  /**
   * @see QSHandlerScenarioParam#type()
   */
  private QSType type;

  /**
   * @see QSHandlerScenarioParam#required()
   */
  private boolean required;

  /**
   * @see QSHandlerScenarioParam#multiple()
   */
  private boolean multiple;

  public QSHandlerScenarioParamBean() {
  }

  public static QSHandlerScenarioParamBean.Builder builder() {
    return new QSHandlerScenarioParamBean.Builder();
  }

  private QSHandlerScenarioParamBean(String code, String title, String description, QSType type, boolean required, boolean multiple) {
    this.code = code;
    this.title = title;
    this.description = description;
    this.type = type;
    this.required = required;
    this.multiple = multiple;
  }

  public String getCode() {
    return code;
  }

  public String getTitle() {
    return title;
  }

  public String getDescription() {
    return description;
  }

  public QSType getType() {
    return type;
  }

  public boolean isRequired() {
    return required;
  }

  public boolean isMultiple() {
    return multiple;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public void setType(QSType type) {
    this.type = type;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setRequired(boolean required) {
    this.required = required;
  }

  public void setMultiple(boolean multiple) {
    this.multiple = multiple;
  }

  public static final class Builder {
    private String code;
    private String title;
    private String description = "";
    private QSType type;
    private boolean required = true;
    private boolean multiple = false;

    private Builder() {
    }

    public QSHandlerScenarioParamBean.Builder code(String code) {
      this.code = code;
      return this;
    }

    public QSHandlerScenarioParamBean.Builder title(String title) {
      this.title = title;
      return this;
    }

    public QSHandlerScenarioParamBean.Builder description(String description) {
      this.description = description;
      return this;
    }

    public QSHandlerScenarioParamBean.Builder type(QSType type) {
      this.type = type;
      return this;
    }

    public QSHandlerScenarioParamBean.Builder required(boolean required) {
      this.required = required;
      return this;
    }

    public QSHandlerScenarioParamBean.Builder multiple(boolean multiple) {
      this.multiple = multiple;
      return this;
    }

    public QSHandlerScenarioParamBean build() {
      return new QSHandlerScenarioParamBean(code, title, description, type, required, multiple);
    }
  }
}
