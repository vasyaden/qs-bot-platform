package qs.platform.botapi.beans;

import qs.platform.botapi.QSSendMessage;

public class QSSendMessageBean implements QSSendMessage {

  private final Long id;
  private final String text;
  private final Integer replyTo;

  private QSSendMessageBean(Long id, String text, Integer replyTo) {
    this.id = id;
    this.text = text;
    this.replyTo = replyTo;
  }

  @Override
  public Long getId() {
    return id;
  }

  @Override
  public String getText() {
    return text;
  }

  @Override
  public Integer getReplyTo() {
    return replyTo;
  }

  public static Builder builder() {
    return new Builder();
  }

  public static final class Builder {
    private Long id;
    private String text;
    private Integer replyTo;

    private Builder() {
    }

    public Builder id(Long id) {
      this.id = id;
      return this;
    }

    public Builder text(String text) {
      this.text = text;
      return this;
    }

    public Builder replyTo(Integer replyTo) {
      this.replyTo = replyTo;
      return this;
    }

    public QSSendMessageBean build() {
      return new QSSendMessageBean(id, text, replyTo);
    }
  }
}
