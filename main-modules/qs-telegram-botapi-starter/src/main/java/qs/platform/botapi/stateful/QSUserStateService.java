package qs.platform.botapi.stateful;

import qs.platform.botapi.QSJsonData;

/**
 * User state service
 */
public interface QSUserStateService {
  /**
   * @param telegramId
   * @return state of user {@link QSUserState}
   */
  QSUserState get(Long telegramId);

  /**
   * delete user state from states
   *
   * @param telegramId
   */
  void delete(Long telegramId);

  /**
   * put the user to state
   *
   * @param stateCode
   * @param stateData
   */
  void toState(String stateCode, QSJsonData stateData);
}
