package qs.platform.botapi.content;

import qs.platform.botapi.QSCache;

import java.util.Collection;
import java.util.Optional;

/**
 * Кэш контента
 */
public interface QSContentCache<C> extends QSCache<String, Collection<C>> {

  /**
   * Получает контент
   *
   * @param handlerCode код хэндлера
   * @return контент
   */
  Optional<Collection<C>> getContent(String handlerCode);
}
