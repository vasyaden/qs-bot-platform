package qs.platform.botapi.scenarios;

import org.springframework.beans.factory.annotation.Autowired;
import org.telegram.telegrambots.meta.api.methods.PartialBotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import qs.platform.botapi.QSCodeBasedHandlerContext;
import qs.platform.botapi.QSCodeBasedHandlerContextAware;
import qs.platform.botapi.QSEvent;
import qs.platform.botapi.QSRequestDataService;
import qs.platform.botapi.beans.QSJsonDataBean;
import qs.platform.botapi.stateful.QSUserStateService;
import qs.platform.dbaccess.services.QSQueryService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/**
 * handles event to the specific parameter
 *
 * @param <T> type of the handled data
 */
public abstract class QSParamFromEventHandler<T> extends QSBaseHandlerScenarioBuilder implements QSCodeBasedHandlerContextAware {
  protected static final String OUTPUT_PARAM = "outputParam";
  protected static final String NEXT_HANDLER = "nextHandler";
  protected static final String ERROR_MESSAGE = "errorMessage";
  public static final String MESSAGE_ID = "messageId";
  public static final String MESSAGE_TEXT = "messageText";
  private final QSRequestDataService requestDataService;
  private QSCodeBasedHandlerContext handlerContext;

  @Autowired
  protected QSParamFromEventHandler(QSQueryService queryService, QSRequestDataService requestDataService, QSUserStateService stateService) {
    super(queryService, requestDataService, stateService);
    this.requestDataService = requestDataService;
  }

  @Override
  public void setCodeBasedHandlerContext(QSCodeBasedHandlerContext context) {
    this.handlerContext = context;
  }

  @Override
  public QSHandlerScenario build(Map<String, Object> params) {
    checkParams(params);

    final String outputParam = (String) params.get(OUTPUT_PARAM);
    final String nextHandler = (String) params.get(NEXT_HANDLER);
    final String errorMessage = (String) params.get(ERROR_MESSAGE);

    return (handler, event, jsonData) -> {
      toState(null, jsonData);
      T value = getValue(event);
      Collection<PartialBotApiMethod<?>> response = new ArrayList<>();
      QSEvent mockEvent;
      QSJsonDataBean.Builder data = QSJsonDataBean.builder()
        .code(nextHandler)
        .userId(jsonData.getUserId())
        .data(jsonData.getData());

      //Remove buttons from request-message
      response.add(
        EditMessageText.builder()
          .chatId(event.getChatIdString())
          .messageId((Integer) jsonData.getParamOrThrow(MESSAGE_ID))
          .text((String) jsonData.getParamOrThrow(MESSAGE_TEXT))
          .build()
      );
      if (value == null) {
        //Send error message
        response.add(
          SendMessage.builder()
            .chatId(event.getChatIdString())
            .text(errorMessage + getInvalidValue(event))
            .build()
        );
      } else {
        data.addParam(outputParam, value);
      }
      response.addAll(handlerContext.handle(event, requestDataService.save(data.build())));
      return response;
    };
  }

  /**
   * Extract value from the event
   *
   * @param event {@link QSEvent event}
   * @return extracted value
   */
  protected abstract T getValue(QSEvent event);

  /**
   * Get raw value from the event as string
   *
   * @param event {@link QSEvent event}
   * @return raw value
   */
  protected abstract String getInvalidValue(QSEvent event);
}
