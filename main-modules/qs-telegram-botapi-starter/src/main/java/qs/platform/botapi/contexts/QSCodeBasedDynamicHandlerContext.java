package qs.platform.botapi.contexts;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.PartialBotApiMethod;
import qs.platform.botapi.*;
import qs.platform.botapi.beans.QSJsonDataBean;
import qs.platform.botapi.handlers.QSHandler;
import qs.platform.exceptions.QSHandlerForbiddenException;
import qs.platform.exceptions.QSHandlerNotFoundException;
import qs.platform.exceptions.QSRuntimeException;

import jakarta.annotation.Priority;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

/**
 * Handler's context that chooses {@link QSHandler handler} based on given handler's code
 */
@Priority(QSHandlerContextChain.Orders.MAIN)
@Component
public class QSCodeBasedDynamicHandlerContext extends QSHandlerContextBaseChain
  implements QSHandlerContext, QSCodeBasedHandlerContext {
  private static final Logger LOG = LoggerFactory.getLogger(QSCodeBasedDynamicHandlerContext.class);
  private static final String BOT_NAME_DELIMITER = "@";

  private QSHandlerCache handlerCache;
  private QSRequestDataService requestDataService;

  @Autowired
  private void setHandlerCache(QSHandlerCache handlerCache) {
    this.handlerCache = handlerCache;
  }

  @Autowired
  private void setRequestDataService(QSRequestDataService requestDataService) {
    this.requestDataService = requestDataService;
  }

  @Autowired(required = false)
  private void setAware(Collection<QSCodeBasedHandlerContextAware> aware) {
    if (CollectionUtils.isNotEmpty(aware)) {
      aware.forEach(x -> x.setCodeBasedHandlerContext(this));
    }
  }

  @Override
  public Collection<PartialBotApiMethod<?>> processUpdate(QSEvent event) {
    Optional<QSHandler> optional = findHandler(event);
    if (optional.isPresent()) {
      QSHandler handler = optional.get();
      return handle(event, handler);
    } else {
      QSJsonData request = getJsonData(event);
      QSHandler currentHandler = handlerCache.getHandler(request.getCode());
      if (currentHandler == null) {
        throw new QSHandlerNotFoundException(request.getCode(), "Not found handler with code=" + request.getCode());
      }
      return handle(event, currentHandler, request);
    }
  }

  @Override
  public Collection<PartialBotApiMethod<?>> handle(QSEvent event, String handlerCode) {
    return Optional.ofNullable(handlerCache.getHandler(handlerCode))
      .map(handler -> handle(event, handler))
      .orElseGet(() ->
        getErrorHandler()
          .handle(event, new QSHandlerNotFoundException(handlerCode, "Not found handler with code=" + handlerCode))
      );
  }

  private Collection<PartialBotApiMethod<?>> handle(QSEvent event, QSHandler handler) {
    QSJsonData jsonData = requestDataService.save(
      QSJsonDataBean.builder()
        .code(handler.getCode())
        .userId(event.getChatId())
        .unique(handler.isUnique())
        .build()
    );
    return handle(event, handler, jsonData);
  }

  @Override
  public Collection<PartialBotApiMethod<?>> handle(QSEvent event, QSJsonData jsonData) {
    String handlerCode = jsonData.getCode();
    return Optional.ofNullable(handlerCache.getHandler(handlerCode))
      .map(handler -> handler.handle(event, jsonData))
      .orElseGet(() ->
        getErrorHandler()
          .handle(event, new QSHandlerNotFoundException(handlerCode, "Not found handler with code=" + handlerCode))
      );
  }

  private Collection<PartialBotApiMethod<?>> handle(QSEvent event, QSHandler handler, QSJsonData jsonData) {
    if (handler.check(jsonData)) {
      return handler.handle(event, jsonData);
    } else {
      throw new QSHandlerForbiddenException();
    }
  }

  private Optional<QSHandler> findHandler(QSEvent event) {
    if (QSEventType.MESSAGE == event.getType()) {
      Optional<String> msg = Optional.ofNullable(event.getMessageText());
      return Optional.ofNullable(event.getReplyToMessageText())
        .map(handlerCache::getHandler)
        .or(
          () -> msg.map(handlerCache::getHandler)
        )
        .or(
          () -> msg.map(this::getCode)
            .map(handlerCache::getHandler)
        );
    }
    return Optional.empty();
  }

  private String getCode(String message) {
    String[] split = message.split(StringUtils.SPACE);
    String[] command = split[0].split(BOT_NAME_DELIMITER);
    return command[0];
  }

  private QSJsonData getJsonData(QSEvent event) {
    if (QSEventType.CALLBACK == event.getType()) {
      try {
        UUID callbackDataId = UUID.fromString(event.getCallbackData());
        Long userId = event.getChatId();
        return requestDataService.getJsonDataByIdAndUserId(callbackDataId, userId);
      } catch (Exception e) {
        throw new QSRuntimeException(e.getMessage(), e);
      }
    } else {
      throw new QSHandlerNotFoundException("request.unknown", "Unknown request: " + event);
    }
  }

}
