package qs.platform.botapi.bots;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.PartialBotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.updates.SetWebhook;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;
import org.telegram.telegrambots.starter.SpringWebhookBot;
import qs.platform.botapi.QSHandlerContextChain;
import qs.platform.botapi.beans.QSEventBean;

import java.io.File;

public class QSWebHookBot extends SpringWebhookBot {
  public static final String WEBHOOK_ENDPOINT = "bot/v1/event";
  private static final Logger LOG = LoggerFactory.getLogger(QSWebHookBot.class);
  private final String username;
  private final String webhookPath;
  private final QSHandlerContextChain context;

  public QSWebHookBot(QSHandlerContextChain context, String token,
                      String username, String webhookPath, int maxThreads, File cert) {
    super(
      DefaultBotOptionsBuilder.newBuilder()
        .maxThreads(maxThreads)
        .build(),
      SetWebhook.builder()
        .url(webhookPath)
        .certificate(new InputFile(cert))
        .build(),
      token
    );
    this.context = context;
    this.username = username;
    this.webhookPath = webhookPath;
  }

  private void send(PartialBotApiMethod<?> response) {
    try {
      if (response instanceof BotApiMethod) {
        execute((BotApiMethod<?>) response);
      } else if (response instanceof SendPhoto sendPhoto) {
        execute(sendPhoto);
      } else if (response instanceof SendDocument sendDocument) {
        execute(sendDocument);
      }
    } catch (TelegramApiRequestException e) {
      LOG.error(e.getMessage() + ": " + e.getErrorCode() + " " + e.getApiResponse(), e);
    } catch (TelegramApiException e) {
      LOG.error(e.getMessage(), e);
    }
  }

  @Override
  public BotApiMethod<?> onWebhookUpdateReceived(Update update) {
    if (update.hasMessage() || update.hasCallbackQuery()) {
      context.tryNext(QSEventBean.from(update)).forEach(this::send);
    }
    return null;
  }

  @Override
  public String getBotUsername() {
    return username;
  }

  @Override
  public String getBotPath() {
    return webhookPath;
  }
}
