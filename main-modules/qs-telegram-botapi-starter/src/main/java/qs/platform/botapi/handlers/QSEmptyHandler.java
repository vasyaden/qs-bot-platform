package qs.platform.botapi.handlers;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.PartialBotApiMethod;
import qs.platform.botapi.QSEvent;
import qs.platform.botapi.QSJsonData;

import java.util.Collection;
import java.util.Collections;

@Component
public class QSEmptyHandler implements QSHandler {

  public static final String CODE = "/empty";

  @Override
  public Collection<PartialBotApiMethod<?>> handle(QSEvent event, QSJsonData jsonData) {
    return Collections.emptyList();
  }

  @Override
  public String getCode() {
    return CODE;
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public String getParentCode() {
    return null;
  }

  @Override
  public Integer getOrder() {
    return 0;
  }

  @Override
  public void addChild(QSHandler handler) {
  }

  @Override
  public Collection<QSHandler> getChildren() {
    return Collections.emptyList();
  }

  @Override
  public int compareTo(QSHandler o) {
    return 0;
  }

  @Override
  public void clearChildren() {
  }

  @Override
  public boolean isUnique() {
    return true;
  }
}
