package qs.platform.botapi.content.beans;

import org.apache.commons.collections4.MapUtils;
import qs.platform.botapi.content.QSContent;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class QSLocation implements QSContent {

  private final Double longitude;
  private final Double latitude;
  private final String handlerCode;
  private final Map<String, Object> params;
  private final Integer order;

  private QSLocation(Double longitude, Double latitude, String handlerCode, Map<String, Object> params, Integer order) {
    this.longitude = longitude;
    this.latitude = latitude;
    this.handlerCode = handlerCode;
    this.params = MapUtils.isEmpty(params) ? Collections.emptyMap() : Collections.unmodifiableMap(new HashMap<>(params));
    this.order = order;
  }

  public static Builder builder() {
    return new Builder();
  }

  public Double getLongitude() {
    return longitude;
  }

  public Double getLatitude() {
    return latitude;
  }

  public String getHandlerCode() {
    return handlerCode;
  }

  public Map<String, Object> getParams() {
    return params;
  }

  public Integer getOrder() {
    return order;
  }

  public static final class Builder {
    private Double longitude;
    private Double latitude;
    private String handlerCode;
    private Map<String, Object> params;
    private Integer order;

    private Builder(){}

    public Builder longitude(Double longitude) {
      this.longitude = longitude;
      return this;
    }

    public Builder latitude(Double latitude) {
      this.latitude = latitude;
      return this;
    }

    public Builder handlerCode(String handlerCode) {
      this.handlerCode = handlerCode;
      return this;
    }

    public Builder params(Map<String, Object> params) {
      this.params = params;
      return this;
    }

    public Builder order(Integer order) {
      this.order = order;
      return this;
    }

    public QSLocation build() {
      return new QSLocation(longitude, latitude, handlerCode, params, order);
    }
  }
}
