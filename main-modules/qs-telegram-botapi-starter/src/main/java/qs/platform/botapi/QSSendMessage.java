package qs.platform.botapi;

public interface QSSendMessage {

  Long getId();

  String getText();

  Integer getReplyTo();

}
