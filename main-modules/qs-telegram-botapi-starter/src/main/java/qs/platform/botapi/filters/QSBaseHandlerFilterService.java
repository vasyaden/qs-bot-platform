package qs.platform.botapi.filters;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import qs.platform.botapi.QSHandlerFilter;
import qs.platform.botapi.QSHandlerFilterService;
import qs.platform.exceptions.QSNotFoundException;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class QSBaseHandlerFilterService implements QSHandlerFilterService {

  private final Map<String, QSHandlerFilter> filters;

  @Autowired(required = false)
  private QSBaseHandlerFilterService(Collection<QSHandlerFilter> filters) {
    this.filters = CollectionUtils.isEmpty(filters) ?
      Collections.emptyMap() :
      Collections.unmodifiableMap(
        filters.stream().collect(Collectors.toMap(QSHandlerFilter::getCode, Function.identity()))
      );
  }

  @Override
  public QSHandlerFilter getFilter(String code) {
    QSHandlerFilter filter = filters.get(code);
    if (filter == null) {
      throw new QSNotFoundException("handler.filter", "Not found filter with code=" + code);
    }
    return filter;
  }

  @Override
  public Collection<QSHandlerFilter> getFilters(Collection<String> codes) {
    return codes.stream()
      .map(this::getFilter)
      .toList();
  }

  @Override
  public Collection<QSHandlerFilter> getAllFilters() {
    return filters.values();
  }
}
