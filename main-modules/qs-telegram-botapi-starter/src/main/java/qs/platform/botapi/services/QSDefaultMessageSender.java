package qs.platform.botapi.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.pinnedmessages.PinChatMessage;
import org.telegram.telegrambots.meta.api.methods.pinnedmessages.UnpinChatMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import qs.platform.botapi.QSMessageSender;
import qs.platform.botapi.QSSendMessage;
import qs.platform.exceptions.QSRuntimeException;

import java.util.Collection;

@Service
public class QSDefaultMessageSender implements QSMessageSender {
  private static final Logger LOG = LoggerFactory.getLogger(QSDefaultMessageSender.class);

  private DefaultAbsSender bot;

  @Autowired
  private void setBot(DefaultAbsSender bot) {
    this.bot = bot;
  }

  @Override
  public void send(Collection<QSSendMessage> messages) {
    messages.forEach(message -> {
      try {
        send(message);
      } catch (Exception e) {
        LOG.error("Message [{}] not sent to user id={}, cause {}", message.getText(), message.getId(), e.getMessage());
      }
    });
  }

  @Override
  public Integer send(QSSendMessage message) {
    try {
      return bot.execute(
        SendMessage.builder()
          .chatId(String.valueOf(message.getId()))
          .text(message.getText())
          .replyToMessageId(message.getReplyTo())
          .build()
      ).getMessageId();
    } catch (Exception e) {
      throw new QSRuntimeException(e.getMessage(), e);
    }
  }

  @Override
  public void pinChatMessage(Long chatId, Integer messageId) {
    try {
      bot.execute(
        PinChatMessage.builder()
          .chatId(String.valueOf(chatId))
          .messageId(messageId)
          .build()
      );
    } catch (Exception e) {
      LOG.error("Cannot pin message={} in the chat={} cause {}", messageId, chatId, e.getMessage(), e);
    }
  }

  @Override
  public void unpinChatMessage(Long chatId, Integer messageId) {
    try {
      bot.execute(
        UnpinChatMessage.builder()
          .chatId(String.valueOf(chatId))
          .messageId(messageId)
          .build()
      );
    } catch (Exception e) {
      LOG.error("Cannot unpin message={} in the chat={} cause {}", messageId, chatId, e.getMessage(), e);
    }
  }
}
