package qs.platform.dbaccess.beans;

import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;
import jakarta.persistence.*;
import java.util.Map;

@Entity
@Table(name = "photo_content")
public class QSImageEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "telegram_id")
  private String telegramId;

  @Column(name = "handler_code")
  private String handlerCode;

  @Column(name = "description")
  private String description;

  @Column(name = "caption")
  private String caption;

  @Column(name = "file_name")
  private String fileName;

  @JdbcTypeCode(SqlTypes.JSON)
  @Column(name = "params")
  private Map<String, Object> params;

  @Column(name = "content_order")
  private Integer order;

  @Embedded
  private FileInfo fileInfo;

  public QSImageEntity() {
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTelegramId() {
    return telegramId;
  }

  public void setTelegramId(String telegramId) {
    this.telegramId = telegramId;
  }

  public String getHandlerCode() {
    return handlerCode;
  }

  public void setHandlerCode(String handlerCode) {
    this.handlerCode = handlerCode;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getCaption() {
    return caption;
  }

  public void setCaption(String caption) {
    this.caption = caption;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public Map<String, Object> getParams() {
    return params;
  }

  public void setParams(Map<String, Object> params) {
    this.params = params;
  }

  public Integer getOrder() {
    return order;
  }

  public void setOrder(Integer order) {
    this.order = order;
  }

  public FileInfo getFileInfo() {
    return fileInfo;
  }

  public void setFileInfo(FileInfo fileInfo) {
    this.fileInfo = fileInfo;
  }
}
