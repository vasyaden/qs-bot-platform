package qs.platform.dbaccess.services.converters;

import org.springframework.stereotype.Component;
import qs.platform.botapi.content.beans.QSLocation;
import qs.platform.dbaccess.beans.QSMapEntity;
import qs.platform.dbaccess.beans.QSMapView;

@Component
public class QSMapConverter implements QSContentConverter<QSMapEntity, QSLocation> {

  @Override
  public QSLocation toContent(QSMapEntity entity) {
    return QSLocation.builder()
      .longitude(entity.getLongitude())
      .latitude(entity.getLatitude())
      .order(entity.getOrder())
      .build();
  }

  public QSLocation toContent(QSMapView entity) {
    return QSLocation.builder()
      .longitude(entity.getLongitude())
      .latitude(entity.getLatitude())
      .order(entity.getOrder())
      .build();
  }
}
