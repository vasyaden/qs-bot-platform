package qs.platform.dbaccess.services.converters;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import qs.platform.botapi.content.beans.QSImage;
import qs.platform.dbaccess.beans.QSImageEntity;
import qs.platform.dbaccess.beans.QSImageView;

@Component
public class QSImageConverter implements QSContentConverter<QSImageEntity, QSImage> {

  @Override
  public QSImage toContent(QSImageEntity entity) {
    if (StringUtils.isBlank(entity.getTelegramId())) {
      return null;
    }
    return QSImage.builder()
      .inputFile(new InputFile(entity.getTelegramId()))
      .caption(entity.getCaption())
      .handlerCode(entity.getHandlerCode())
      .params(entity.getParams())
      .order(entity.getOrder())
      .build();
  }

  public QSImage toContent(QSImageView entity) {
    if (StringUtils.isBlank(entity.getTelegramId())) {
      return null;
    }
    return QSImage.builder()
      .inputFile(new InputFile(entity.getTelegramId()))
      .caption(entity.getCaption())
      .handlerCode(entity.getHandlerCode())
      .params(entity.getParams())
      .order(entity.getOrder())
      .build();
  }
}
