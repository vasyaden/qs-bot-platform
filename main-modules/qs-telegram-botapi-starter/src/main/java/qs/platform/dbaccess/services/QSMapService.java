package qs.platform.dbaccess.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import qs.platform.botapi.content.beans.QSLocation;
import qs.platform.dbaccess.beans.QSMapEntity;
import qs.platform.dbaccess.repositories.QSMapRepository;
import qs.platform.dbaccess.services.converters.QSMapConverter;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class QSMapService extends QSBaseContentService<QSLocation, QSMapEntity> {

  private QSMapConverter converter;

  @Autowired
  public void setConverter(QSMapConverter converter) {
    this.converter = converter;
  }

  @Override
  public Collection<QSLocation> findCacheable() {
    return ((QSMapRepository) getRepository())
      .findByHandlerCodeIsNotNull()
      .stream()
      .map(converter::toContent)
      .filter(Objects::nonNull)
      .collect(Collectors.toList());
  }
}
