package qs.platform.dbaccess.repositories;

import qs.platform.dbaccess.beans.QSImageEntity;
import qs.platform.dbaccess.beans.QSImageView;

import java.util.Collection;

public interface QSImageRepository extends QSContentRepository<QSImageEntity, Long> {

  Collection<QSImageEntity> findByTelegramIdIsNull();

  Collection<QSImageView> findByTelegramIdIsNotNullAndHandlerCodeIsNotNull();
}
