package qs.platform.dbaccess.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Map;

/**
 * Repository for getting data by SQL-query
 */
@Repository
public class QSJdbcQueryRepository implements QSQueryRepository {
  private final NamedParameterJdbcTemplate jdbcTemplate;

  @Autowired
  public QSJdbcQueryRepository(NamedParameterJdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  /**
   * Get message from database by given query
   *
   * @param query sql query to get message
   * @param args  args for the given query
   * @return result of the query
   */
  public String getMessage(String query, Map<String, Object> args) {
    return jdbcTemplate.queryForObject(query, args, String.class);
  }

  /**
   * Get data from database by given query
   *
   * @param query sql query to get data
   * @param args  args for the given query
   * @return result of the query, where every map is a row of the resultSet
   */
  public Collection<Map<String, Object>> getData(String query, Map<String, Object> args) {
    return jdbcTemplate.queryForList(query, args);
  }

}
