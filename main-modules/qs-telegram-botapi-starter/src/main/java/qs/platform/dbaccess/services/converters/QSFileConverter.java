package qs.platform.dbaccess.services.converters;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import qs.platform.botapi.content.beans.QSFile;
import qs.platform.dbaccess.beans.QSFileEntity;
import qs.platform.dbaccess.beans.QSFileView;

@Component
public class QSFileConverter implements QSContentConverter<QSFileEntity, QSFile> {

  @Override
  public QSFile toContent(QSFileEntity entity) {
    if (StringUtils.isBlank(entity.getTelegramId())) {
      return null;
    }
    return QSFile
      .builder()
      .inputFile(new InputFile(entity.getTelegramId()))
      .caption(entity.getCaption())
      .handlerCode(entity.getHandlerCode())
      .params(entity.getParams())
      .order(entity.getOrder())
      .build();
  }

  public QSFile toContent(QSFileView entity) {
    if (StringUtils.isBlank(entity.getTelegramId())) {
      return null;
    }
    return QSFile
      .builder()
      .inputFile(new InputFile(entity.getTelegramId()))
      .caption(entity.getCaption())
      .handlerCode(entity.getHandlerCode())
      .params(entity.getParams())
      .order(entity.getOrder())
      .build();
  }

}
