package qs.platform.dbaccess.services.converters;

public interface QSContentConverter<E, C> {

  C toContent(E entity);

}
