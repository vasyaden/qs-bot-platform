package qs.platform.dbaccess.services;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.PhotoSize;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;
import qs.platform.botapi.content.QSContentCache;
import qs.platform.botapi.content.beans.QSFile;
import qs.platform.botapi.content.beans.QSImage;
import qs.platform.dataaccess.fs.QSFileStorage;
import qs.platform.dataaccess.fs.QSFileStorageService;
import qs.platform.dbaccess.beans.FileInfo;
import qs.platform.dbaccess.beans.QSFileEntity;
import qs.platform.dbaccess.beans.QSImageEntity;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Optional;

@Service
public class QSContentUploaderService implements QSContentUploader {
  private static final Logger LOG = LoggerFactory.getLogger(QSContentUploaderService.class);
  private DefaultAbsSender bot;
  private QSFileService fileService;
  private QSImageService imageService;
  private QSContentCache<QSFile> fileCache;
  private QSContentCache<QSImage> imageCache;
  @Value("${qs.bot.admin-id:}")
  private Collection<String> adminIds;
  private QSFileStorageService fsService;

  @Autowired
  private void setFsService(QSFileStorageService fsService) {
    this.fsService = fsService;
  }

  @Autowired
  private void setBot(DefaultAbsSender bot) {
    this.bot = bot;
  }

  @Autowired
  private void setFileService(QSFileService fileService) {
    this.fileService = fileService;
  }

  @Autowired
  private void setImageService(QSImageService imageService) {
    this.imageService = imageService;
  }

  @Autowired
  public void setFileCache(QSContentCache<QSFile> fileCache) {
    this.fileCache = fileCache;
  }

  @Autowired
  public void setImageCache(QSContentCache<QSImage> imageCache) {
    this.imageCache = imageCache;
  }

  @EventListener
  private void handleContextRefreshedEvent(ContextRefreshedEvent event) {
    uploadAll();
  }

  @Override
  public void uploadAll() {
    if (CollectionUtils.isNotEmpty(adminIds)) {
      String adminId = adminIds.iterator().next();
      if (uploadFiles(adminId)) {
        fileCache.refresh();
      }
      if (uploadImages(adminId)) {
        imageCache.refresh();
      }
    }
  }

  private boolean uploadFiles(String adminId) {
    Collection<QSFileEntity> notSentFiles = fileService.findNotUploaded();
    Collection<QSFileEntity> sentFiles = new ArrayList<>();

    notSentFiles.forEach(qsFile -> {
      FileInfo fileInfo = qsFile.getFileInfo();
      QSFileStorage storage = fsService.getStorage(fileInfo.getStorageCode());
      if (storage != null) {
        try (InputStream file = storage.read(fileInfo.getStorageFilePath(), fileInfo.getStorageFileName())) {
          String fileId = uploadFile(file, qsFile.getFileName(), adminId);
          qsFile.setTelegramId(fileId);
          sentFiles.add(qsFile);

        } catch (TelegramApiRequestException e) {
          LOG.error(e.getMessage() + ": " + e.getErrorCode() + " " + e.getApiResponse(), e);
        } catch (Exception e) {
          LOG.error(e.getMessage(), e);
        }
      } else {
        LOG.error("Can not upload file " + fileInfo.getStorageFileName() + " : Storage with code = " + fileInfo.getStorageCode() + " not found!");
      }
    });
    fileService.save(sentFiles);
    return !sentFiles.isEmpty();
  }

  private boolean uploadImages(String adminId) {
    Collection<QSImageEntity> notSentImages = imageService.findNotUploaded();
    Collection<QSImageEntity> sentImages = new ArrayList<>();

    notSentImages.forEach(qsImage -> {
      FileInfo fileInfo = qsImage.getFileInfo();
      QSFileStorage storage = fsService.getStorage(fileInfo.getStorageCode());

      if (storage != null) {
        try (InputStream file = storage.read(fileInfo.getStorageFilePath(), fileInfo.getStorageFileName())) {
          uploadImage(file, qsImage.getFileName(), adminId)
            .ifPresent(imageId -> {
              qsImage.setTelegramId(imageId);
              sentImages.add(qsImage);
            });
        } catch (TelegramApiRequestException e) {
          LOG.error(e.getMessage() + ": " + e.getErrorCode() + " " + e.getApiResponse(), e);
        } catch (Exception e) {
          LOG.error(e.getMessage(), e);
        }
      } else {
        LOG.error("Can not upload image " + fileInfo.getStorageFileName() + " : Storage with code = " + fileInfo.getStorageCode() + " not found!");
      }
    });
    imageService.save(sentImages);
    return !sentImages.isEmpty();
  }

  private String uploadFile(InputStream stream, String fileName, String adminId) throws TelegramApiException {
    InputFile document = new InputFile(stream, fileName);
    Message sent = bot.execute(
      SendDocument.builder()
        .chatId(adminId)
        .document(document)
        .build()
    );
    return sent.getDocument().getFileId();
  }

  private Optional<String> uploadImage(InputStream stream, String imageName, String adminId) throws TelegramApiException {
    InputFile image = new InputFile(stream, imageName);
    Message sent = bot.execute(
      SendPhoto.builder()
        .chatId(adminId)
        .photo(image)
        .build()
    );
    Collection<PhotoSize> photos = sent.getPhoto();
    if (CollectionUtils.isNotEmpty(photos)) {
      return photos.stream().max(Comparator.comparing(PhotoSize::getFileSize)).map(PhotoSize::getFileId);
    }
    return Optional.empty();
  }

}
