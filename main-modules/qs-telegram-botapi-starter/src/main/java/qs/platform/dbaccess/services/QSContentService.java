package qs.platform.dbaccess.services;

import java.util.Collection;

public interface QSContentService<C> {

  Collection<C> getContent(String handlerCode);

  Collection<C> findCacheable();
}
