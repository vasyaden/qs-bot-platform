package qs.platform.dbaccess.beans;


import java.util.Map;

public interface QSImageView {

  String getTelegramId();

  String getHandlerCode();

  String getCaption();

  Map<String, Object> getParams();

  Integer getOrder();
}
