package qs.platform.dbaccess.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import qs.platform.botapi.content.beans.QSImage;
import qs.platform.dbaccess.beans.QSImageEntity;
import qs.platform.dbaccess.repositories.QSImageRepository;
import qs.platform.dbaccess.services.converters.QSImageConverter;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class QSImageService extends QSBaseContentService<QSImage, QSImageEntity> {
  private static final Logger LOG = LoggerFactory.getLogger(QSImageService.class);

  private QSImageConverter converter;

  @Autowired
  public void setConverter(QSImageConverter converter) {
    this.converter = converter;
  }

  public Collection<QSImageEntity> findNotUploaded() {
    return ((QSImageRepository) getRepository()).findByTelegramIdIsNull();
  }

  public void save(Collection<QSImageEntity> images) {
    getRepository().saveAll(images);
  }

  public Collection<QSImage> findCacheable() {
    return ((QSImageRepository) getRepository())
      .findByTelegramIdIsNotNullAndHandlerCodeIsNotNull()
      .stream()
      .map(converter::toContent)
      .filter(Objects::nonNull)
      .collect(Collectors.toList());
  }
}
