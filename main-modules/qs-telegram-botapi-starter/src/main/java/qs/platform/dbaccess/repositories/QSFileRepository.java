package qs.platform.dbaccess.repositories;

import qs.platform.dbaccess.beans.QSFileEntity;
import qs.platform.dbaccess.beans.QSFileView;

import java.util.Collection;

public interface QSFileRepository extends QSContentRepository<QSFileEntity, Long> {

  Collection<QSFileEntity> findByTelegramIdIsNull();

  Collection<QSFileView> findByTelegramIdIsNotNullAndHandlerCodeIsNotNull();
}
