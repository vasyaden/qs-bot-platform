package qs.platform.dbaccess.repositories;

import java.util.Collection;
import java.util.Map;

/**
 * Repository for getting data by SQL-query
 */
public interface QSQueryRepository {

  /**
   * Get message from database by given query
   *
   * @param query sql query to get message
   * @param args  args for the given query
   * @return result of the query
   */
  String getMessage(String query, Map<String, Object> args);

  /**
   * Get data from database by given query
   *
   * @param query sql query to get data
   * @param args  args for the given query
   * @return result of the query, where every map is a row of the resultSet
   */
  Collection<Map<String, Object>> getData(String query, Map<String, Object> args);
}
