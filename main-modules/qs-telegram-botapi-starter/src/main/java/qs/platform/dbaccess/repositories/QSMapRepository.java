package qs.platform.dbaccess.repositories;

import qs.platform.dbaccess.beans.QSMapEntity;
import qs.platform.dbaccess.beans.QSMapView;

import java.util.Collection;

public interface QSMapRepository extends QSContentRepository<QSMapEntity, Long> {

  Collection<QSMapView> findByHandlerCodeIsNotNull();
}
