package qs.platform.dbaccess.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.Collection;

@NoRepositoryBean
public interface QSContentRepository<E, ID extends Serializable> extends JpaRepository<E, ID> {

  Collection<E> findByHandlerCode(String handlerCode);
}
