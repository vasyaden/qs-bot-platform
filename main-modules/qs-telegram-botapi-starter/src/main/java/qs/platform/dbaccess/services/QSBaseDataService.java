package qs.platform.dbaccess.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import qs.platform.dbaccess.repositories.QSJdbcQueryRepository;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Service for getting data by SQL-query
 */
@Service
public class QSBaseDataService implements QSQueryService {
  private final QSJdbcQueryRepository queryRepository;

  @Autowired
  public QSBaseDataService(QSJdbcQueryRepository qsQueryRepository) {
    this.queryRepository = qsQueryRepository;
  }

  /**
   * Get data from database by given query
   *
   * @param query  sql query to get data
   * @param params request data
   * @return result of the query, where every map is a row of the resultSet
   */
  @Override
  public Collection<Map<String, Object>> getData(String query, Map<String, Object> params) {
    Map<String, Object> args = getArgs(query, params);
    return queryRepository.getData(query, args);
  }

  /**
   * Get message from database by given query
   *
   * @param query  sql query to get message
   * @param params request data
   * @return result of the query
   */
  @Override
  public String getMessage(String query, Map<String, Object> params) {
    Map<String, Object> args = getArgs(query, params);
    return queryRepository.getMessage(query, args);
  }

  private Map<String, Object> getArgs(String query, Map<String, Object> params) {
    return params.keySet().stream()
      .filter(param -> query.contains(":" + param))
      .collect(Collectors.toMap(k -> k, params::get));
  }
}
