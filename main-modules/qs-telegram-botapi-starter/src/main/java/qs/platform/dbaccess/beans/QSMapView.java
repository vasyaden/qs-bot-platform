package qs.platform.dbaccess.beans;

import java.util.Map;

public interface QSMapView {

  Double getLatitude();

  Double getLongitude();

  String getHandlerCode();

  Map<String, Object> getParams();

  Integer getOrder();
}
