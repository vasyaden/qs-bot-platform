package qs.platform.dbaccess.services;

public interface QSContentUploader {
  /**
   * Uploads content to telegram-bot
   */
  void uploadAll();
}
