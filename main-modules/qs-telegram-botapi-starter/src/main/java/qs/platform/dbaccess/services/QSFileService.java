package qs.platform.dbaccess.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import qs.platform.botapi.content.beans.QSFile;
import qs.platform.dbaccess.beans.QSFileEntity;
import qs.platform.dbaccess.repositories.QSFileRepository;
import qs.platform.dbaccess.services.converters.QSFileConverter;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class QSFileService extends QSBaseContentService<QSFile, QSFileEntity> {
  private static final Logger LOG = LoggerFactory.getLogger(QSFileService.class);

  private QSFileConverter converter;

  @Autowired
  public void setConverter(QSFileConverter converter) {
    this.converter = converter;
  }

  public Collection<QSFileEntity> findNotUploaded() {
    return ((QSFileRepository) getRepository()).findByTelegramIdIsNull();
  }

  public void save(Collection<QSFileEntity> files) {
    getRepository().saveAll(files);
  }

  @Override
  public Collection<QSFile> findCacheable() {
    return ((QSFileRepository) getRepository())
      .findByTelegramIdIsNotNullAndHandlerCodeIsNotNull()
      .stream()
      .map(converter::toContent)
      .filter(Objects::nonNull)
      .collect(Collectors.toList());
  }
}
