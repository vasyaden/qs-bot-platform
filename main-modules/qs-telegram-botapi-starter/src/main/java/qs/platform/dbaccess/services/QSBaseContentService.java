package qs.platform.dbaccess.services;

import org.springframework.beans.factory.annotation.Autowired;
import qs.platform.dbaccess.repositories.QSContentRepository;
import qs.platform.dbaccess.services.converters.QSContentConverter;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

public abstract class QSBaseContentService<C, E> implements QSContentService<C> {

  private QSContentRepository<E, Long> repository;

  private QSContentConverter<E, C> converter;

  @Autowired
  private void setRepository(QSContentRepository<E, Long> repository) {
    this.repository = repository;
  }

  @Autowired
  private void setConverter(QSContentConverter<E, C> converter) {
    this.converter = converter;
  }

  protected QSContentRepository<E, Long> getRepository() {
    return repository;
  }

  @Override
  public Collection<C> getContent(String handlerCode) {
    return repository.findByHandlerCode(handlerCode).stream()
      .map(converter::toContent)
      .filter(Objects::nonNull)
      .collect(Collectors.toList());
  }
}
