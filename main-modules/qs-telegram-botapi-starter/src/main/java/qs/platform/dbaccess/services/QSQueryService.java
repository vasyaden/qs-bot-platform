package qs.platform.dbaccess.services;

import java.util.Collection;
import java.util.Map;

/**
 * Service for getting data by SQL-query
 */
public interface QSQueryService {

  /**
   * Get data from database by given query
   *
   * @param query  sql query to get data
   * @param params request data
   * @return result of the query, where every map is a row of the resultSet
   */
  Collection<Map<String, Object>> getData(String query, Map<String, Object> params);

  /**
   * Get message from database by given query
   *
   * @param query  sql query to get message
   * @param params request data
   * @return result of the query
   */
  String getMessage(String query, Map<String, Object> params);
}
