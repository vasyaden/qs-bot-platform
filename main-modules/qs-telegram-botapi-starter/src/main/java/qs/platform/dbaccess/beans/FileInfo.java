package qs.platform.dbaccess.beans;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import java.time.LocalDateTime;

/**
 * Embedded class with information about the file
 */
@Embeddable
public class FileInfo {
  /**
   * file name for user
   */
  @Column(name = "file_title")
  private String fileTitle;
  /**
   * file extension
   */
  @Column(name = "file_ext")
  private String fileExt;
  /**
   * file size
   */
  @Column(name = "file_size")
  private Long fileSize;
  /**
   * date the file was saved/updated in our system
   */
  @Column(name = "file_date")
  private LocalDateTime fileDate;
  /**
   * storage code
   */
  @Column(name = "storage_code")
  private String storageCode;
  /**
   * path to file in storage
   */
  @Column(name = "storage_file_path")
  private String storageFilePath;
  /**
   * file name in storage
   */
  @Column(name = "storage_file_name")
  private String storageFileName;

  public FileInfo() {
  }

  public String getFileTitle() {
    return fileTitle;
  }

  public void setFileTitle(String fileTitle) {
    this.fileTitle = fileTitle;
  }

  public String getFileExt() {
    return fileExt;
  }

  public void setFileExt(String fileExt) {
    this.fileExt = fileExt;
  }

  public Long getFileSize() {
    return fileSize;
  }

  public void setFileSize(Long fileSize) {
    this.fileSize = fileSize;
  }

  public LocalDateTime getFileDate() {
    return fileDate;
  }

  public void setFileDate(LocalDateTime fileDate) {
    this.fileDate = fileDate;
  }

  public String getStorageCode() {
    return storageCode;
  }

  public void setStorageCode(String storageCode) {
    this.storageCode = storageCode;
  }

  public String getStorageFilePath() {
    return storageFilePath;
  }

  public void setStorageFilePath(String storageFilePath) {
    this.storageFilePath = storageFilePath;
  }

  public String getStorageFileName() {
    return storageFileName;
  }

  public void setStorageFileName(String storageFileName) {
    this.storageFileName = storageFileName;
  }
}
