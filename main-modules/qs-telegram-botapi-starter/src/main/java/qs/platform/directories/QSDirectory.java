package qs.platform.directories;

/**
 * Directory
 * <p/>
 * Contains a pair of code and title
 *
 * @param <C> type of code
 */
public interface QSDirectory<C> {
  /**
   * Code of the entry
   */
  C getCode();

  /**
   * Title of the entry
   */
  String getTitle();
}