package qs.platform.directories;

/**
 * Directory of week days
 */
public enum WeekdaysRus implements QSDirectory<Integer> {

  MONDAY(0, "ПН"),
  TUESDAY(1, "ВТ"),
  WEDNESDAY(2, "СР"),
  THURSDAY(3, "ЧТ"),
  FRIDAY(4, "ПТ"),
  SATURDAY(5, "СБ"),
  SUNDAY(6, "ВС");

  public final int code;
  public final String title;

  WeekdaysRus(int code, String title) {
    this.code = code;
    this.title = title;
  }

  @Override
  public Integer getCode() {
    return code;
  }

  @Override
  public String getTitle() {
    return title;
  }
}