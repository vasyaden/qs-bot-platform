package qs.platform.directories.services;

import qs.platform.directories.QSDirectory;
import qs.platform.directories.QSDirectoryService;

public interface MonthDirectoryService<D extends QSDirectory<Integer>> extends QSDirectoryService<Integer, D> {
}
