package qs.platform.directories;

/**
 * Service for working with directories
 *
 * @param <C> type of directory code
 * @param <D> type of directory entry
 */
public interface QSDirectoryService<C, D extends QSDirectory<C>> {
  /**
   * Get directory element by code
   *
   * @param code code of directory entry
   * @return directory element
   */
  D getElement(C code);

  /**
   * Get directory element title by code
   *
   * @param code code of directory entry
   * @return title of directory element
   */
  String getTitle(C code);

}
