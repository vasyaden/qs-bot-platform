package qs.platform.directories.services;

import org.springframework.stereotype.Service;
import qs.platform.directories.Month;

import jakarta.ws.rs.NotFoundException;
import java.util.HashMap;
import java.util.Map;

@Service
public class EnumMonthDirectoryService implements MonthDirectoryService<Month> {

  private static final Map<Integer, Month> MONTH = new HashMap<>();

  static {
    for (Month month : Month.values()) {
      MONTH.put(month.getCode(), month);
    }
  }

  @Override
  public Month getElement(Integer code){
    Month element = MONTH.get(code);
    if (element == null) {
        throw new NotFoundException("Month not found, wrong input parameter " + code);
    }
    return element;
  }

  @Override
  public String getTitle(Integer code) {
    return getElement(code).getTitle();
  }
}