package qs.platform.directories.services;

import org.springframework.stereotype.Service;
import qs.platform.directories.WeekdaysRus;

import jakarta.ws.rs.NotFoundException;
import java.util.HashMap;
import java.util.Map;

@Service
public class EnumWeekdaysDirectoryService implements WeekdaysDirectoryService<WeekdaysRus> {
  private static final Map<Integer, WeekdaysRus> WEEKDAYS = new HashMap<>();

  static {
    for (WeekdaysRus weekdays : WeekdaysRus.values()) {
      WEEKDAYS.put(weekdays.getCode(), weekdays);
    }
  }

  @Override
  public WeekdaysRus getElement(Integer code) {
    WeekdaysRus element = WEEKDAYS.get(code);
    if (element == null) {
      throw new NotFoundException("Weekday not found, wrong input parameter " + code);
    }
    return element;
  }

  @Override
  public String getTitle(Integer code){
    return getElement(code).getTitle();
  }
}
