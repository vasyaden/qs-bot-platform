package qs.platform.configs;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableJpaRepositories(basePackages = "qs")
@EntityScan(basePackages = "qs")
public class QSAppConfig {

  @Bean
  public RestTemplate restTemplate() {
    return new RestTemplate();
  }
}
