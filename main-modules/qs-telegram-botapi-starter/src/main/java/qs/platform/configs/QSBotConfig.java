package qs.platform.configs;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.Resource;
import org.telegram.telegrambots.starter.TelegramBotStarterConfiguration;
import qs.platform.botapi.QSHandlerContextChain;
import qs.platform.botapi.bots.QSLongPollingBot;
import qs.platform.botapi.bots.QSWebHookBot;
import qs.platform.exceptions.QSRuntimeException;

import java.io.File;
import java.util.Collection;

@Import(TelegramBotStarterConfiguration.class)
@Configuration
public class QSBotConfig {

  @Value("${qs.bot.longpoll.enabled:false}")
  private boolean longPollEnabled;

  @Value("${qs.bot.webhook.enabled:false}")
  private boolean webhookEnabled;

  @Value("${qs.bot.webhook.path:}")
  private String webhookPath;

  @Value("${qs.bot.token}")
  private String token;

  @Value("${qs.bot.username}")
  private String username;

  @Value("${qs.bot.maxThreads:1}")
  private int maxThreads;

  @Value("classpath:certificate/bot.ssl.cert")
  private Resource cert;

  private QSHandlerContextChain context;

  @Autowired
  private void setContext(QSHandlerContextChain context, Collection<QSHandlerContextChain> chains) {
    this.context = context;
    chains.stream()
      .filter(chain -> !context.equals(chain))
      .forEach(context::addLast);
  }

  @Bean
  @ConditionalOnProperty(value = "qs.bot.longpoll.enabled", havingValue = "true")
  public QSLongPollingBot longPollingBot() {
    checkCollision();
    return new QSLongPollingBot(context, token, username, maxThreads);
  }

  @Bean
  @ConditionalOnProperty(value = "qs.bot.webhook.enabled", havingValue = "true")
  public QSWebHookBot webHookBot() {
    checkCollision();
    if (StringUtils.isBlank(webhookPath)) {
      throw new QSRuntimeException("Webhook path was not specified");
    }
    return new QSWebHookBot(context, token, username, webhookPath, maxThreads, getCertFile());
  }

  private void checkCollision() {
    if (longPollEnabled == webhookEnabled) {
      throw new QSRuntimeException("The bot type was not specified");
    }
  }

  private File getCertFile() {
    File certFile = null;
    if (cert.exists()) {
      try {
        certFile = cert.getFile();
      } catch (Exception e) {
        throw new QSRuntimeException(e.getMessage(), e);
      }
    }
    return certFile;
  }

}
