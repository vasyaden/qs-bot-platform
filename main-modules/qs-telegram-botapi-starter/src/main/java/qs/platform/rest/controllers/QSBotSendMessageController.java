package qs.platform.rest.controllers;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import qs.platform.botapi.QSMessageSender;
import qs.platform.botapi.QSSendMessage;
import qs.platform.botapi.beans.QSSendMessageBean;
import qs.platform.exceptions.QSAppRuntimeException;
import qs.platform.rest.headers.QSHttpHeaders;
import qs.platform.rest.json.beans.QSJsonMessage;
import qs.platform.rest.json.beans.QSJsonSendMessage;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/bot/v1/${qs.bot.routing.service-name}")
public class QSBotSendMessageController {

  private QSMessageSender messageSender;

  @Value("${qs.bot.secret}")
  private String secret;

  @Autowired
  public void setMessageSender(QSMessageSender messageSender) {
    this.messageSender = messageSender;
  }

  @PostMapping(value = "/send", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Void> sendMessage(@RequestBody QSJsonSendMessage sendMessages,
                                          @RequestHeader(value = QSHttpHeaders.X_QS_SERVICE_SECRET) String secret) {
    if (!this.secret.equals(secret)) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
    Collection<Long> ids = sendMessages.getIds();
    Collection<QSJsonMessage> jsonMessages = sendMessages.getMessages();
    if (CollectionUtils.isEmpty(ids) || CollectionUtils.isEmpty(jsonMessages)) {
      throw new QSAppRuntimeException("invalid.sendMessage.request", "ids and messages should not be empty");
    }
    Collection<QSSendMessage> messages = ids.stream()
      .map(id -> {
          QSSendMessageBean.Builder builder = QSSendMessageBean.builder().id(id);
          return jsonMessages.stream()
            .map(QSJsonMessage::getText)
            .filter(StringUtils::isNotBlank)
            .map(builder::text)
            .map(QSSendMessageBean.Builder::build)
            .collect(Collectors.toList());
        }
      )
      .flatMap(Collection::stream)
      .collect(Collectors.toList());
    messageSender.send(messages);
    return ResponseEntity.ok().build();
  }


}
