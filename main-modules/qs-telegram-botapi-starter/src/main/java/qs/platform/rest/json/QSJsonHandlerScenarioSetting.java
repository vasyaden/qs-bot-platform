package qs.platform.rest.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import qs.platform.botapi.scenarios.annotations.QSHandlerScenarioSetting;

import java.util.Collection;

/**
 * {@link QSHandlerScenarioSetting}'s json bean
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QSJsonHandlerScenarioSetting {
  /**
   * @see QSHandlerScenarioSetting#code()
   */
  private String code;

  /**
   * @see QSHandlerScenarioSetting#title()
   */
  private String title;

  /**
   * @see QSHandlerScenarioSetting#description()
   */
  private String description;

  /**
   * @see QSHandlerScenarioSetting#params()
   */
  private Collection<QSJsonHandlerScenarioParam> params;

  public QSJsonHandlerScenarioSetting() {
  }

  public static Builder builder() {
    return new Builder();
  }

  private QSJsonHandlerScenarioSetting(String code, String title, String description, Collection<QSJsonHandlerScenarioParam> params) {
    this.code = code;
    this.title = title;
    this.description = description;
    this.params = params;
  }

  public String getCode() {
    return code;
  }

  public String getTitle() {
    return title;
  }

  public String getDescription() {
    return description;
  }

  public Collection<QSJsonHandlerScenarioParam> getParams() {
    return params;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setParams(Collection<QSJsonHandlerScenarioParam> params) {
    this.params = params;
  }

  public static final class Builder {
    private String code;
    private String title;
    private String description;
    private Collection<QSJsonHandlerScenarioParam> params;

    private Builder() {
    }

    public Builder code(String code) {
      this.code = code;
      return this;
    }

    public Builder title(String title) {
      this.title = title;
      return this;
    }

    public Builder description(String description) {
      this.description = description;
      return this;
    }

    public Builder params(Collection<QSJsonHandlerScenarioParam> params) {
      this.params = params;
      return this;
    }

    public QSJsonHandlerScenarioSetting build() {
      return new QSJsonHandlerScenarioSetting(code, title, description, params);
    }
  }
}
