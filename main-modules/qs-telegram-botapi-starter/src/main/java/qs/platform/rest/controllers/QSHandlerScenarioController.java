package qs.platform.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import qs.platform.botapi.scenarios.QSHandlerScenarioService;
import qs.platform.converters.QSConverterService;
import qs.platform.rest.headers.QSHttpHeaders;
import qs.platform.rest.json.QSJsonHandlerScenarioSetting;

import java.util.Collection;


@RestController
@RequestMapping("/bot/v1/${qs.bot.routing.service-name}/scenarios")
public class QSHandlerScenarioController {
  private final QSHandlerScenarioService handlerScenarioService;
  private final QSConverterService converterService;
  @Value("${qs.bot.secret}")
  private String secret;

  @Autowired
  public QSHandlerScenarioController(QSHandlerScenarioService handlerScenarioService, QSConverterService converterService) {
    this.handlerScenarioService = handlerScenarioService;
    this.converterService = converterService;
  }

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Collection<QSJsonHandlerScenarioSetting>> getAll(@RequestHeader(value = QSHttpHeaders.X_QS_SERVICE_SECRET) String secret) {
    if (!this.secret.equals(secret)) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
    Collection<QSJsonHandlerScenarioSetting> scenarios = handlerScenarioService.getAll().stream()
      .map(bean -> converterService.convertTo(QSJsonHandlerScenarioSetting.class, bean))
      .toList();

    return new ResponseEntity<>(scenarios, HttpStatus.OK);
  }

}
