package qs.platform.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.telegram.telegrambots.meta.api.objects.Update;
import qs.platform.botapi.bots.QSWebHookBot;

import java.util.concurrent.CompletableFuture;

@RestController
@ConditionalOnProperty(value = "qs.bot.webhook.enabled", havingValue = "true")
public class QSWebHookController {

  @Autowired
  private QSWebHookBot bot;

  @PostMapping(value = QSWebHookBot.WEBHOOK_ENDPOINT)
  public ResponseEntity<Void> onUpdateReceived(@RequestBody Update update) {
    CompletableFuture.runAsync(() -> bot.onWebhookUpdateReceived(update));
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
