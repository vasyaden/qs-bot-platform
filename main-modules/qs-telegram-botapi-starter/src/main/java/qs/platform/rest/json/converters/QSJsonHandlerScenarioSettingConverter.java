package qs.platform.rest.json.converters;

import org.springframework.stereotype.Component;
import qs.platform.botapi.scenarios.QSHandlerScenarioSetting;
import qs.platform.botapi.scenarios.QSScenarioParam;
import qs.platform.converters.QSConverter;
import qs.platform.rest.json.QSJsonHandlerScenarioParam;
import qs.platform.rest.json.QSJsonHandlerScenarioSetting;

import java.util.Objects;

/**
 * {@link QSHandlerScenarioSetting} to {@link QSJsonHandlerScenarioSetting} converter
 */
@Component
public class QSJsonHandlerScenarioSettingConverter implements QSConverter<QSHandlerScenarioSetting, QSJsonHandlerScenarioSetting> {

  /**
   * Converts {@link QSHandlerScenarioSetting} value to {@link QSJsonHandlerScenarioSetting}
   *
   * @param value input value
   * @return {@link QSJsonHandlerScenarioSetting converted value}
   */
  @Override
  public QSJsonHandlerScenarioSetting convert(QSHandlerScenarioSetting value) {
    if (value == null) {
      return null;
    }
    return QSJsonHandlerScenarioSetting.builder()
      .code(value.getCode())
      .description(value.getDescription())
      .title(value.getTitle())
      .params(value.getParams().stream()
        .filter(Objects::nonNull)
        .map(this::paramToJson)
        .toList())
      .build();
  }

  @Override
  public Class<QSHandlerScenarioSetting> getInputType() {
    return QSHandlerScenarioSetting.class;
  }

  @Override
  public Class<QSJsonHandlerScenarioSetting> getOutputType() {
    return QSJsonHandlerScenarioSetting.class;
  }

  private QSJsonHandlerScenarioParam paramToJson(QSScenarioParam bean) {
    return QSJsonHandlerScenarioParam.builder()
      .code(bean.getCode())
      .description(bean.getDescription())
      .title(bean.getTitle())
      .type(bean.getType() == null ? null : bean.getType().getType())
      .multiple(bean.isMultiple())
      .required(bean.isRequired())
      .build();
  }
}
