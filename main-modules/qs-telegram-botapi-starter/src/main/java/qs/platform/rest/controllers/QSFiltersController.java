package qs.platform.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import qs.platform.botapi.QSHandlerFilter;
import qs.platform.botapi.filters.QSBaseHandlerFilterService;
import qs.platform.rest.headers.QSHttpHeaders;
import qs.platform.rest.json.beans.QSJsonHandlerFilter;

import java.util.Collection;

@RestController
@RequestMapping("/bot/v1/${qs.bot.routing.service-name}/filters")
public class QSFiltersController {

  @Value("${qs.bot.secret}")
  private String secret;

  private QSBaseHandlerFilterService baseHandlerFilterService;

  @Autowired
  public void setBaseHandlerFilterService(QSBaseHandlerFilterService baseHandlerFilterService) {
    this.baseHandlerFilterService = baseHandlerFilterService;
  }

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Collection<QSJsonHandlerFilter>> retrieveHandlers(@RequestHeader(value = QSHttpHeaders.X_QS_SERVICE_SECRET) String secret) {
    if (!this.secret.equals(secret)) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
    Collection<QSJsonHandlerFilter> filters = toJson(baseHandlerFilterService.getAllFilters());
    return new ResponseEntity<>(filters, HttpStatus.OK);
  }

  private Collection<QSJsonHandlerFilter> toJson(Collection<QSHandlerFilter> filters) {
    return filters.stream()
      .map(filter -> QSJsonHandlerFilter.builder()
        .title(filter.getTitle())
        .code(filter.getCode())
        .description(filter.getDescription())
        .build())
      .toList();
  }
}