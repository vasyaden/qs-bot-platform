package qs.platform.rest.json.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import qs.platform.botapi.QSHandlerFilter;


/**
 * Class represents Json model describing HandlerFilter DTO for REST API
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QSJsonHandlerFilter {

  /**
   * @see QSHandlerFilter#getTitle()
   */
  private String title;

  /**
   * @see QSHandlerFilter#getCode()
   */
  private String code;

  /**
   * @see QSHandlerFilter#getDescription()
   */
  private String description;

  public QSJsonHandlerFilter(String title, String code, String description) {
    this.title = title;
    this.code = code;
    this.description = description;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public static Builder builder() {
    return new Builder();
  }

  public static final class Builder {

    private String title;
    private String code;
    private String description;

    public Builder title(String title) {
      this.title = title;
      return this;
    }

    public Builder code(String code) {
      this.code = code;
      return this;
    }

    public Builder description(String description) {
      this.description = description;
      return this;
    }

    public QSJsonHandlerFilter build() {
      return new QSJsonHandlerFilter(title, code, description);
    }
  }
}