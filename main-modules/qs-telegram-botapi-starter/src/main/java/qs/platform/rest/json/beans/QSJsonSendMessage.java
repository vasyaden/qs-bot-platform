package qs.platform.rest.json.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Collection;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QSJsonSendMessage {

  private Collection<Long> ids;
  private Collection<QSJsonMessage> messages;

  public Collection<Long> getIds() {
    return ids;
  }

  public void setIds(Collection<Long> ids) {
    this.ids = ids;
  }

  public Collection<QSJsonMessage> getMessages() {
    return messages;
  }

  public void setMessages(Collection<QSJsonMessage> messages) {
    this.messages = messages;
  }
}
