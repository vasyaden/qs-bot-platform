package qs.platform.rest.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import qs.platform.botapi.scenarios.annotations.QSHandlerScenarioParam;

/**
 * {@link QSHandlerScenarioParam}'s json bean
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QSJsonHandlerScenarioParam {
  /**
   * @see QSHandlerScenarioParam#code()
   */
  private String code;

  /**
   * @see QSHandlerScenarioParam#title()
   */
  private String title;

  /**
   * @see QSHandlerScenarioParam#description()
   */
  private String description;

  /**
   * @see QSHandlerScenarioParam#type()
   */
  private String type;

  /**
   * @see QSHandlerScenarioParam#required()
   */
  private boolean required;

  /**
   * @see QSHandlerScenarioParam#multiple()
   */
  private boolean multiple;

  public QSJsonHandlerScenarioParam() {
  }

  public static Builder builder() {
    return new Builder();
  }

  private QSJsonHandlerScenarioParam(String code, String title, String description, String type, boolean required, boolean multiple) {
    this.code = code;
    this.title = title;
    this.description = description;
    this.type = type;
    this.required = required;
    this.multiple = multiple;
  }

  public String getCode() {
    return code;
  }

  public String getTitle() {
    return title;
  }

  public String getDescription() {
    return description;
  }

  public String getType() {
    return type;
  }

  public boolean isRequired() {
    return required;
  }

  public boolean isMultiple() {
    return multiple;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setRequired(boolean required) {
    this.required = required;
  }

  public void setMultiple(boolean multiple) {
    this.multiple = multiple;
  }

  public static final class Builder {
    private String code;
    private String title;
    private String description;
    private String type;
    private boolean required;
    private boolean multiple;

    private Builder() {
    }

    public Builder code(String code) {
      this.code = code;
      return this;
    }

    public Builder title(String title) {
      this.title = title;
      return this;
    }

    public Builder description(String description) {
      this.description = description;
      return this;
    }

    public Builder type(String type) {
      this.type = type;
      return this;
    }

    public Builder required(boolean required) {
      this.required = required;
      return this;
    }

    public Builder multiple(boolean multiple) {
      this.multiple = multiple;
      return this;
    }

    public QSJsonHandlerScenarioParam build() {
      return new QSJsonHandlerScenarioParam(code, title, description, type, required, multiple);
    }
  }
}