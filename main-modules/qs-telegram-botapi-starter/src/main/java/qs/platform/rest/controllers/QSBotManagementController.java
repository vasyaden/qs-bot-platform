package qs.platform.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import qs.platform.botapi.QSCacheManagementService;
import qs.platform.botapi.QSHandlerCache;
import qs.platform.dbaccess.services.QSContentUploader;
import qs.platform.rest.headers.QSHttpHeaders;

@RestController
@RequestMapping("/bot/v1/${qs.bot.routing.service-name}")
public class QSBotManagementController {

  @Value("${qs.bot.secret}")
  private String secret;

  private QSHandlerCache handlerCache;
  private QSContentUploader uploadService;
  private QSCacheManagementService service;

  @Autowired
  private void setHandlerCache(QSHandlerCache handlerCache) {
    this.handlerCache = handlerCache;
  }

  @Autowired
  private void setUploadService(QSContentUploader uploadService) {
    this.uploadService = uploadService;
  }

  @Autowired
  private void setService(QSCacheManagementService service) {
    this.service = service;
  }

  @PostMapping(value = "/actualize", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Void> actualize(@RequestHeader(value = QSHttpHeaders.X_QS_SERVICE_SECRET) String secret) {
    if (!this.secret.equals(secret)) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
    handlerCache.refresh();
    return ResponseEntity.status(HttpStatus.OK).build();
  }

  @PostMapping(value = "/upload", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Void> uploadAll(@RequestHeader(value = QSHttpHeaders.X_QS_SERVICE_SECRET) String secret) {
    if (!this.secret.equals(secret)) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
    uploadService.uploadAll();
    return ResponseEntity.status(HttpStatus.OK).build();
  }

  @PostMapping(value = "/refresh-cache", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Void> refreshCache(@RequestHeader(value = QSHttpHeaders.X_QS_SERVICE_SECRET) String secret) {
    if (!this.secret.equals(secret)) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
    service.refresh();
    return ResponseEntity.status(HttpStatus.OK).build();
  }

  @PostMapping(value = "/refresh-cache/{code}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Void> refreshCacheByCode(@PathVariable("code") String code, @RequestHeader(value = QSHttpHeaders.X_QS_SERVICE_SECRET) String secret) {
    if (!this.secret.equals(secret)) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
    service.refresh(code);
    return ResponseEntity.status(HttpStatus.OK).build();
  }
}
