CREATE TABLE IF NOT EXISTS menu_settings (
    code                varchar NOT NULL,
    message             varchar NOT NULL,
    m_query             varchar,
    title               varchar NOT NULL,
    data_query          varchar,
    description         varchar,
    multiple            bool,
    parent_code         varchar,
    deleted             bool DEFAULT false,
    menu_order             integer NOT NULL,
    PRIMARY KEY (code)
);

CREATE TABLE IF NOT EXISTS request_history (
    id              bigint  NOT NULL GENERATED ALWAYS AS IDENTITY,
    user_id         bigint  NOT NULL,
    handler_code    varchar NOT NULL,
    json_data       varchar,
    previous        bigint,
    PRIMARY KEY (id)
);

CREATE INDEX IF NOT EXISTS request_history_id_user_id_idx ON request_history USING btree (id, user_id);

CREATE TABLE IF NOT EXISTS photo_content (
    id              bigint  NOT NULL GENERATED ALWAYS AS IDENTITY,
    telegram_id     varchar,
    handler_code    varchar NOT NULL,
    description     varchar,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS document_content (
    id              bigint  NOT NULL GENERATED ALWAYS AS IDENTITY,
    telegram_id     varchar,
    handler_code    varchar NOT NULL,
    description     varchar,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS map_content (
    id              bigint  NOT NULL GENERATED ALWAYS AS IDENTITY,
    latitude        float   NOT NULL,
    longitude       float   NOT NULL,
    handler_code    varchar NOT NULL,
    description     varchar,
    PRIMARY KEY (id)
);

CREATE INDEX IF NOT EXISTS document_content_handler_code_idx ON document_content USING btree (handler_code);
CREATE INDEX IF NOT EXISTS photo_content_handler_code_idx ON photo_content USING btree (handler_code);
CREATE INDEX IF NOT EXISTS map_content_handler_code_idx ON map_content USING btree (handler_code);

ALTER TABLE document_content ADD COLUMN IF NOT EXISTS caption   varchar;
ALTER TABLE document_content ADD COLUMN IF NOT EXISTS base64    varchar;
ALTER TABLE document_content ADD COLUMN IF NOT EXISTS file_name varchar;
ALTER TABLE photo_content    ADD COLUMN IF NOT EXISTS caption   varchar;
ALTER TABLE photo_content    ADD COLUMN IF NOT EXISTS base64    varchar;
ALTER TABLE photo_content    ADD COLUMN IF NOT EXISTS file_name varchar;
