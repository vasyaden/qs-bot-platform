DROP INDEX IF EXISTS request_history_id_user_id_idx;
ALTER TABLE request_history DROP COLUMN id;
ALTER TABLE request_history DROP COLUMN previous;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

ALTER TABLE request_history ADD COLUMN id uuid DEFAULT uuid_generate_v4 ();
ALTER TABLE request_history ADD COLUMN previous uuid;
ALTER TABLE request_history ADD PRIMARY KEY (id);

CREATE INDEX IF NOT EXISTS request_history_id_user_id_idx ON request_history USING btree (id, user_id);
