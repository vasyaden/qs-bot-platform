ALTER TABLE document_content ADD COLUMN IF NOT EXISTS content_order int NOT NULL DEFAULT 0;
ALTER TABLE photo_content    ADD COLUMN IF NOT EXISTS content_order int NOT NULL DEFAULT 0;
ALTER TABLE map_content      ADD COLUMN IF NOT EXISTS content_order int NOT NULL DEFAULT 0;