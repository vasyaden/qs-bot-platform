ALTER TABLE document_content ADD COLUMN IF NOT EXISTS file_title   varchar;
ALTER TABLE document_content ADD COLUMN IF NOT EXISTS file_ext    varchar;
ALTER TABLE document_content ADD COLUMN IF NOT EXISTS file_size bigint;
ALTER TABLE document_content ADD COLUMN IF NOT EXISTS file_date timestamptz NOT NULL DEFAULT now();
ALTER TABLE document_content ADD COLUMN IF NOT EXISTS storage_code    varchar;
ALTER TABLE document_content ADD COLUMN IF NOT EXISTS storage_file_path varchar;
ALTER TABLE document_content ADD COLUMN IF NOT EXISTS storage_file_name   varchar;

ALTER TABLE photo_content ADD COLUMN IF NOT EXISTS file_title   varchar;
ALTER TABLE photo_content ADD COLUMN IF NOT EXISTS file_ext    varchar;
ALTER TABLE photo_content ADD COLUMN IF NOT EXISTS file_size bigint;
ALTER TABLE photo_content ADD COLUMN IF NOT EXISTS file_date timestamptz NOT NULL DEFAULT now();
ALTER TABLE photo_content ADD COLUMN IF NOT EXISTS storage_code    varchar;
ALTER TABLE photo_content ADD COLUMN IF NOT EXISTS storage_file_path varchar;
ALTER TABLE photo_content ADD COLUMN IF NOT EXISTS storage_file_name   varchar;