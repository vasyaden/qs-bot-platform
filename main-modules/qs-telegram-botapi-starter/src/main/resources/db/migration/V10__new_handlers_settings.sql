CREATE TABLE IF NOT EXISTS qs_handlers_settings (
  code            varchar     NOT NULL,
  title           varchar     NOT NULL,
  description     varchar,
  parent_code     varchar,
  menu_order      integer     NOT NULL,
  filters         varchar,
  data_query      varchar,
  unique_handler  boolean     NOT NULL DEFAULT FALSE,
  scenario_code   varchar     NOT NULL,
  scenario_params varchar     NOT NULL,
  creation_date   timestamptz NOT NULL DEFAULT now(),
  PRIMARY KEY (code)
);
--TODO: change type of filters and params to jsonb
CREATE TABLE IF NOT EXISTS qs_handlers_settings_editor (
  code            varchar     NOT NULL,
  title           varchar     NOT NULL,
  description     varchar,
  parent_code     varchar,
  menu_order      integer     NOT NULL,
  filters         varchar,
  data_query      varchar,
  unique_handler  boolean     NOT NULL DEFAULT FALSE,
  scenario_code   varchar     NOT NULL,
  scenario_params varchar     NOT NULL,
  creation_date   timestamptz NOT NULL DEFAULT now(),
  change_date     timestamptz NOT NULL DEFAULT now(),
  version         integer     NOT NULL DEFAULT (0),
  deleted         bool        DEFAULT 'false' NOT NULL,
  PRIMARY KEY (code)
);

INSERT INTO qs_handlers_settings_editor (code, title, description, parent_code, menu_order, filters, data_query,
  unique_handler, scenario_code,scenario_params, deleted)
  SELECT code, title, description, parent_code,  menu_order, filters,data_query, unique_handler, 'qsSimpleMenuScenario',
    json_build_object('message', message, 'messageQuery', m_query, 'toState', to_state)::json, deleted
  FROM menu_settings;

INSERT INTO qs_handlers_settings (code, title, description, parent_code, menu_order, filters,
  data_query, unique_handler, scenario_code,scenario_params)
  SELECT code, title, description, parent_code, menu_order, filters, data_query, unique_handler, scenario_code,
    scenario_params
  FROM qs_handlers_settings_editor
  WHERE deleted = false;

DROP TABLE menu_settings;