ALTER TABLE request_history
  ALTER COLUMN json_data TYPE jsonb USING json_data::jsonb;

ALTER TABLE document_content
  ALTER COLUMN params TYPE jsonb USING params::jsonb;

ALTER TABLE photo_content
  ALTER COLUMN params TYPE jsonb USING params::jsonb;

ALTER TABLE map_content
  ALTER COLUMN params TYPE jsonb USING params::jsonb;

ALTER TABLE qs_handlers_settings
  ALTER COLUMN filters TYPE jsonb USING filters::jsonb;

ALTER TABLE qs_handlers_settings
  ALTER COLUMN scenario_params TYPE jsonb USING scenario_params::jsonb;

ALTER TABLE qs_handlers_settings_editor
  ALTER COLUMN filters TYPE jsonb USING filters::jsonb;

ALTER TABLE qs_handlers_settings_editor
  ALTER COLUMN scenario_params TYPE jsonb USING scenario_params::jsonb;
