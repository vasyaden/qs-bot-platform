ALTER TABLE document_content ADD COLUMN IF NOT EXISTS params varchar;
ALTER TABLE photo_content    ADD COLUMN IF NOT EXISTS params varchar;
ALTER TABLE map_content      ADD COLUMN IF NOT EXISTS params varchar;